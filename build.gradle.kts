import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    val kotlinVersion = "2.0.10"
    java
    application
    idea
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion
    alias(libs.plugins.org.somda.append.snapshot.id)
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
}

group = "org.somda.asciidoc"
version = "0.0.31"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))

    implementation(libs.ktor.server.core)
    implementation(libs.ktor.server.netty)

    implementation(libs.docx4j.core)
    implementation(libs.docx4j.jaxb.moxy)

    // asciidoc conversion
    implementation(libs.asciidoctorj.core)
    implementation(libs.asciidoctorj.diagram)
    implementation(libs.asciidoctorj.diagram.plantuml)
    implementation(libs.asciidoctorj.pdf)

    // logging
    implementation(libs.log4j.core)
    implementation(libs.log4j.api.kotlin)

    implementation(libs.apache.commons.text)
    implementation(libs.clikt)
    implementation(libs.google.guava)
    implementation(libs.kotlin.diff.utils)
    implementation(libs.kotlinx.serialization.json)

    testImplementation(libs.junit.jupiter.api)
    runtimeOnly(libs.junit.jupiter.engine)
}

application {
    mainClass = "org.somda.asciidoc.ieee.ConvertKt"
}

val jdkVersion: JvmTarget = JvmTarget.JVM_17

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(jdkVersion.target)
    }
    withSourcesJar()
}

kotlin {
    // explicitApi()
    compilerOptions {
        jvmTarget = jdkVersion
    }
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<Jar> {
    // Otherwise you'll get a "No main manifest attribute" error
    manifest {
        attributes["Main-Class"] = "org.somda.asciidoc.ieee.ConvertKt"
    }

    duplicatesStrategy = DuplicatesStrategy.EXCLUDE

    // To add all of the dependencies otherwise a "NoClassDefFoundError" error
    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
}