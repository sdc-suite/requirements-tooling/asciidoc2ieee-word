= P{ieee_metadata_designation}™/D{ieee_metadata_draft_number} -- Draft {ieee_metadata_trial_use} {ieee_metadata_gde_rec_prac_std} for {ieee_metadata_par_title}
:doctype: book
:xrefstyle: short
:toc-title: Contents
:toc: left
:toclevels: 2
:sectnums:
:icons: font
:imagesdir: images/
:docinfo: shared
// Choose from "Amendment", "Corrigenda" or leave empty if this document is a baseline / revision
:ieee_amendment_corrigenda: Amendment
// Amendment/Corrigenda number as specified in PAR
:ieee_amendment_corrigenda_number: 2
:ieee_keywords: designation, document development, draft, equation, figure, guide, IEEE 987.6TM, introduction, list, purpose, recommended practice, scope, standard
:ieee_metadata_designation: 987.6
:ieee_metadata_draft_number: 3
// Choose from "Trial-Use" or leave empty
:ieee_metadata_trial_use:
// Choose from "Standard", "Recommended Practice" or "Guide"
:ieee_metadata_gde_rec_prac_std: Recommended Practice
:ieee_metadata_par_title: Preparing an IEEE Standards Draft
:ieee_metadata_committee_name: Standards Staff Engineering Committee
:ieee_metadata_society_name: Template Society
:ieee_metadata_draft_date: 2021-01
:ieee_metadata_working_group_name: <Working Group Name>
:ieee_metadata_chair_name: Arthur C. Clarke
:ieee_metadata_subgroup_chair_name: Alessandro Volta

// Abstract

Key discussion points covered in the draft are stated here in a few complete sentences, using passive rather than active voice.
The more specific the better since the abstract often populates search engines and catalog databases.

[preface]
== Introduction

The introduction of the frontmatter is informative.
It serves to give readers context, including background, key themes, history, etc.

**Acknowledgments**

Permissions have been granted as follows:

[ieee_overview=1]
== Overview

=== Scope

The scope shall be within the technical boundaries, as determined by the balloting group, of the scope submitted on the PAR.

If the standard incorporated Open Source, this should be noted in the Scope along with a link to the Open Source of the URL.

=== Purpose

The purpose shall be within the technical boundaries, as determined by the balloting group, of the purpose submitted on the PAR.

[bibliography,ieee_bib_group=normative]
== Normative References

* [[[normref_ieee_astm_si_10, IEEE/ASTM SI 10]]] IEEE/ASTM SI 10™, American National Standard for Metric Practice.
* [[[normref_c2, C2-2012]]] Accredited Standards Committee C2-2012, National Electrical Safety Code® (NESC®).
* [[[normref_ieee_p802_21, IEEE P802.21]]] IEEE P802.21™ (Draft 14, November 2003), Draft Standard for Local and Metropolitan Area Networks — Media Independent Handover Services.
* [[[normref_ieee_std_91, IEEE Std 91]]] IEEE Std 91™, IEEE Standard Graphic Symbols for Logic Functions.
* [[[normref_ieee_std_260_1, IEEE Std 260.1-2004]]] IEEE Std 260.1™-2004, IEEE Standard Letter Symbols for Units of Measurement (SI Units, Customary Inch-Pound Units, and Certain Other Units).
* [[[normref_iso_iec_27002, ISO/IEC 27002-:2013]]] ISO/IEC 27002-:2013, Information technology—Security techniques—Code of practice for information security controls.
* [[[normref_nfpa_70, NFPA 70]]] NFPA 70, 2011 Edition, National Electrical Code® (NEC®).

[ieee_skip=1]
== Definitions & Abbreviations

[ieee_definitions=1]
=== Definitions

[[def_acceleration_insensitive_drift_rate, acceleration-insensitive drift rate]]
==== acceleration-insensitive drift rate

The component of ... See also: drift rate; systematic drift rate.

:var_def_code_set: code set
[[def_code_set, {var_def_code_set}]]
==== {var_def_code_set}

See: coded character set.

:var_def_coded_character_set: coded character set
[[def_coded_character_set, {var_def_coded_character_set}]]
==== {var_def_coded_character_set}

A set of characters ... Syn: code set.

:var_def_drift_rate: drift rate
[[def_drift_rate, {var_def_drift_rate}]]
==== {var_def_drift_rate}

The slope at a stated time of ... (adapted from ISO/IEC 9945-1:2003).

:var_def_input_reference_axis: input reference axis (IRA)
[[def_input_reference_axis, {var_def_input_reference_axis}]]
==== {var_def_input_reference_axis}

The direction of an axis ... Contrast: output reference axis.

NOTE: See 6.7.14

:var_def_output: output
[[def_output, {var_def_output}]]
==== {var_def_output}

(A) Data that ... (B) The process of ...

:var_def_systematic_drift_rate: systematic drift rate
[[def_systematic_drift_rate, {var_def_systematic_drift_rate}]]
==== {var_def_systematic_drift_rate}

That component of drift rate that ... (IEEE Std 260.1-2004).

[ieee_abbreviations=1]
=== Abbreviations

[[abbrev_der]] DER:: distributed emission regeneration
[[abbrev_dis]] DIS:: distributed interactive simulation
[[abbrev_isdn]] ISDN:: integrated services digital network
[[abbrev_lan]] LAN:: local area network
[[abbrev_pdu]] PDU:: protocol data unit

== Important elements of IEEE standards drafts

.Change
[ieee_change,ieee_change_type=change,ieee_change_where='note 2 to R0068']
--
[discrete]
====== Old

This procedure has to be explicitly offered by a SERVICE PROVIDER by letting the SERVICE CONSUMER first modify a set of presettings^9^ and then confirm all presettings by a separated SET SERVICE OPERATION invocation. The presetting confirmation will apply all changes to the real settings.

[discrete]
====== New

This procedure has to be explicitly offered by a SERVICE PROVIDER by letting the SERVICE CONSUMER first modify a set of presets^9^ and then confirm all presets by a separated SET SERVICE OPERATION invocation. The preset confirmation will apply all changes to the real settings.
--

// replace table
.Change
[ieee_change, ieee_change_type=replace_table, ieee_table_number=1.2.3]
--
.A table with various column spans and widths
[#tbl_example1,cols="1,3,1,1",ieee_orientation=portrait]
|===
|0,0 |0,1 |0,2 |0,3

|2.3
|1,2
|1,3
|1,4

|2.3
|1,2
|1,3
|1,4
|===
--

// insert table

.Insert
[ieee_change, ieee_change_type=insert_table, ieee_table_number=1.2.3, ieee_change_where=Clause 1.2]
--
.A table with various column spans and widths
[#tbl_example2,cols="1,3,1,1",ieee_orientation=portrait]
|===
|0,0 |0,1 |0,2 |0,3

|2.3
|1,2
|1,3
|1,4

|2.3
|1,2
|1,3
|1,4
|===
--

// insert table rows

.Insert
[ieee_change, ieee_change_type=insert_table_rows, ieee_table_number=1.2.3, ieee_row_number=5]
--
.A table with various column spans and widths
[#tbl_example3,cols="1,3,1,1",ieee_orientation=portrait]
|===
|0,0 |0,1 |0,2 |0,3

|2.3
|1,2
|1,3
|1,4

|2.3
|1,2
|1,3
|1,4
|===
--

// insert table rows at the end

.Insert
[ieee_change, ieee_change_type=insert_table_rows, ieee_table_number=1.2.3]
--
.A table with various column spans and widths
[#tbl_example4,cols="1,3,1,1",ieee_orientation=portrait]
|===
|0,0 |0,1 |0,2 |0,3

|2.3
|1,2
|1,3
|1,4

|2.3
|1,2
|1,3
|1,4
|===
--

// replace table rows

.Insert
[ieee_change, ieee_change_type=replace_table_rows, ieee_row_number=4,ieee_table_number=1.2.3]
--
.A table with various column spans and widths
[#tbl_example5,cols="1,3,1,1",ieee_orientation=portrait]
|===
|0,0 |0,1 |0,2 |0,3

|2.3
|1,2
|1,3
|1,4

|2.3
|1,2
|1,3
|1,4
|===
--


// delete table rows

.Insert
[ieee_change, ieee_change_type=delete_table_rows, ieee_table_number=1.2.3]
--
.A table with various column spans and widths
[#tbl_exampl6,cols="1,3,1,1",ieee_orientation=portrait]
|===
|0,0 |0,1 |0,2 |0,3

|2.3
|1,2
|1,3
|1,4

|2.3
|1,2
|1,3
|1,4
|===
--

// Insert figure

.Insert
[ieee_change, ieee_change_type=insert_figure, ieee_figure_number=1.2.3, ieee_change_where=Clause 80]
--
.A beautiful view of Lübeck's old city
[#img_example]
image::luebeck_sunset.jpg[]
--

.Insert
[ieee_change, ieee_change_type=insert, ieee_change_where=Clause 5]
--
Here is some new text.
--

.Delete
[ieee_change, ieee_change_type=delete, ieee_change_where=Clause 8]
--
This text is supposed to be removed
--

.Delete
[ieee_change, ieee_change_type=delete, ieee_change_where=Clause 12.23]
--
--

.Requirement
[ieee_change, ieee_change_type=replace_requirement, ieee_change_where=R9080]
--

[discrete]
== Old

The old requirement text

[discrete]
== New

Replace req text of R9080
--

.Requirement
[ieee_change, ieee_change_type=replace_figure, ieee_figure_number=9.0]
--
.A beautiful view of Lübeck's old city
[#img_example]
image::luebeck_sunset.jpg[]
--

=== General

// .R0010
// [ieee_requirement#r10,ieee_keyword=should]
// ****
//
// IEEE drafts SHOULD be created using one of the approved IEEE SA templates.
//
// . List 1
// . List 2
//
// Intermediate
//
// . List 1
// . List 2
// . List 3
// ****
//
// .R0001
// [ieee_requirement#r1,ieee_keyword=should]
// ****
//
// IEEE drafts SHOULD be created using one of the approved IEEE SA templates.
//
// . List 1
// . List 2
//
// Intermediate
//
// . List 1
// . List 2
// . List 3
//
// NOTE: The templates have built-in 15 macro features that allow for easy tagging of each of the draft elements.
// ****
//
// .R0002
// [ieee_requirement#r2,ieee_keyword=shall]
// ****
//
// Sources listed in the normative references clause SHALL also be cited in text.
//
// NOTE: Explain the role and significance of each normative reference.
//
// NOTE: Note that IEEE drafts may be included in the normative references clause as long as they are properly cited.
//
// NOTE: See reference to <<normref_ieee_p802_21>>.
// ****
//
// .R0003
// [ieee_requirement#r3,ieee_keyword=shall]
// ****
//
// All IEEE standards SHALL use metric units as the primary units of measure.
//
// NOTE: Customary equivalents may be included in the text after the metric units in parentheses.
//
// NOTE: In the case of tables, separate tables for metric and customary units may be included.
//
// NOTE: See <<normref_c2>> and <<normref_nfpa_70>>.
//
// NOTE: For more information on the use of metric in IEEE standards, see <<normref_ieee_astm_si_10>>.
//
// NOTE: For guidance on the use of letter symbols for units of measurement, refer to <<normref_ieee_std_260_1>>.
// ****

=== Lists

.Test
[plantuml#pm__NumericMetricDescriptor_mindmap,target=pm__NumericMetricDescriptor_mindmap,format=png]
....
@startmindmap

skinparam dpi 300

<style>
mindmapDiagram {
  .attribute {
    BackgroundColor #FFBBCC
    RoundCorner 0
    Padding 3
  }
  .attribute_group {
    BackgroundColor #FFBBCC
  }
  .element {
    BackgroundColor lightgreen
  }
  .complex_type {
    BackgroundColor lightblue
  }
}
</style>

+ pm:NumericMetricDescriptor <<complex_type>>
++ @
+++ Handle (pm:Handle) <<attribute>>
+++ DescriptorVersion (pm:DescriptorVersion) <<attribute>>
+++ SafetyClassification (pm:SafetyClassification) <<attribute>>
+++ MetricCategory (pm:MetricCategory) <<attribute>>
+++ DerivationMethod (pm:DerivationMethod) <<attribute>>
+++ MetricAvailability (pm:MetricAvailability) <<attribute>>
+++ MaxMeasurementTime (pm:MaxMeasurementTime) <<attribute>>
+++ MaxDelayTime (pm:MaxDelayTime) <<attribute>>
+++ DeterminationPeriod (pm:DeterminationPeriod) <<attribute>>
+++ LifeTimePeriod (pm:LifeTimePeriod) <<attribute>>
+++ ActivationDuration (pm:ActivationDuration) <<attribute>>
+++ Resolution (pm:Resolution) <<attribute>>
+++ AveragingPeriod (pm:AveragingPeriod) <<attribute>>
++ ...
+++ ext:Extension (reference) <<element>>
+++ pm:Type (pm:CodedValue) <<element>>
++ ...
+++ pm:Unit (pm:CodedValue) <<element>>
+++ pm:BodySite (pm:CodedValue) <<element>>
+++ pm:Relation (anonymous) <<element>>
++ ...
+++ pm:TechnicalRange (pm:Range) <<element>>

@endmindmap
....

Lists in a clause or subclause may be ordered or unordered.

The following is an example of a properly formatted ordered list:

. Name of the manufacturer
. Connection chart showing
.. Full winding development
.. Taps
. Self-impedance (for linear coupler transformers)
.. Reactance
.. Impedance
... For volts
... For amperes

The following is an example of a properly formatted unordered list:

- Begin with a capital letter.
- Include final punctuation for all items in the list if one item in the list is a complete sentence.

=== Tables

Tables should be cited in text and the significance of the tables explained.
Table titles are positioned above the tables.
// <<tbl_example>> shows the nomenclature of a properly formatted table.
//
// .A table with various column spans and widths
// [#tbl_example,cols="1,3,1,1,2,1",ieee_orientation=portrait]
// |===
// |0,0 |0,1 |0,2 |0,3 |0,4| 0,5
//
// 2.3+|2.3+
// |1,2
// |1,3
// |1,4
// 1.2+|1.2+
//
// |2,0
// 2.2+|2.2+
//
// |3,0
// |3,5
//
// 2+|2+
// |4,2
// |4,3
// |4,4
// |4,5
// |===

=== Figures

Figures should be cited in text and the significance of the figures explained.
Figure titles are positioned below the figures themselves.
Figures can be created using text or graphics software.
<<img_example>> shows a properly formatted figure.

.A beautiful view of Lübeck's old city
[#img_example]
image::luebeck_sunset.jpg[]

=== Equations

There is currently no support for math equations.

[appendix, ieee_normativity=normative, ieee_change_where=A]
== Example of a draft amendment/corrigendum.

=== Test

.Change
[ieee_change, ieee_change_type=replace_requirement, ieee_change_where=C.9]
--

[discrete]
== Old

R5025: Updated descriptors SHALL be ordered in the way that parent descriptors appear before child descriptors.

[discrete]
== New

R5025: A SERVICE PROVIDER SHALL order msg:DescriptionModificationReport/msg:ReportPart elements in a way the report parts containing parent descriptors appear before report parts containing their child descriptors.
--

This annex contains an example of a draft amendment that was prepared using the IEEE SA Word template for amendments/corrigenda.footnote:test[This is a __styled__ exemplary footnote]

.An annex table with various column spans and widths
[#tbl_annex_example,cols="1,3,1,1,2,1",ieee_orientation=portrait]
|===
|0,0 |0,1 |0,2 |0,3 |0,4| 0,5

2.3+a|
Here we have complex content.

Another P

. list
. list

|1,2
|1,3
|1,4
1.2+|1.2+

|2,0
2.2+|2.2+

|3,0
|3,5

2+|2+
|4,2
|4,3
|4,4
|4,5
|===

.A beautiful view of Lübeck's old city in annex
[#img_annex_example]
image::luebeck_sunset.jpg[]

[bibliography,ieee_bib_group=informative, ieee_change_where=B]
== Bibliography

* [[[bibref_xml,XML]]] Extensible Markup Language (XML) 1.0 (Fifth Edition), Tim Bray et al., W3C Recommendation 26 November 2008, available at link:https://www.w3.org/TR/REC-xml[]
