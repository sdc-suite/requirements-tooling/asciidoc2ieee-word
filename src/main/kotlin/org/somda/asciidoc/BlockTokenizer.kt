package org.somda.asciidoc

import org.apache.commons.text.StringEscapeUtils
import org.somda.asciidoc.BlockTokenizer.Chunk
import org.somda.asciidoc.ieee.NumberedReferenceItem

/**
 * Parsed asciidoc text as a list of chunks.
 */
data class TokenizedText(val chunks: List<Chunk> = listOf())

/**
 * Asciidoc source code tokenizer.
 *
 * Detects the following markup:
 *
 * - pre-processor markup enclosed by "{" and "}" (excluding variables)
 * - references enclosed by "<<" and ">>"
 * - bold text enclosed by "**"
 * - italic text enclosed by "__"
 * - superscript enclosed by "^"
 * - subscript enclosed by "~"
 * - monospace text enclosed by "`"
 * - anchors enclosed by "[[" and "]]"
 * - hyperlink macros initiated by "link:"
 * - footnote macros initiated by "footnote:"
 *
 * *Caution: there is no support for single underscore or asterisk markup.*
 *
 * The asciidoc source code is parsed on instance construction and the tokenized items can be retrieved via [get].
 *
 * @param asciidocMarkup string that contains asciidoc markup
 * @param numbered set of references that are supposed to be numbered (will be part of the [Chunk.Reference] struct).
 */
class BlockTokenizer(
    private val asciidocMarkup: String,
    private val numbered: Map<String, NumberedReferenceItem> = mapOf()
) {

    /**
     * Secondary constructor that accepts arrays of strings in lieu of a single string.
     */
    constructor(asciidocMarkup: List<String>, numbered: Map<String, NumberedReferenceItem> = mapOf()) : this(
        asciidocMarkup.joinToString("\n"),
        numbered
    )

    private var activeStyles = ActiveStyles()
    private var currentIndex = 0
    private val chunks = mutableListOf<Chunk>()

    init {
        transform()
    }

    /**
     * Retrieves the tokenized text.
     */
    fun get() = TokenizedText(chunks)

    private fun transform() {
        while (true) {
            val currentScope = asciidocMarkup.substring(currentIndex)
            val nextToken = findNextToken(currentScope)

            when (nextToken) {
                null -> {
                    appendTextWithCurrentStyle(currentScope)
                    break
                }

                else -> appendTextWithCurrentStyle(currentScope.substring(0, nextToken.index))
            }

            when (nextToken.token) {
                Token.MONO -> TODO()

                Token.ESCAPED -> {
                    require(nextToken.index + 1 < currentScope.length) {
                        "Escape token is not allowed to be the last character"
                    }

                    appendTextWithCurrentStyle(currentScope.substring(nextToken.index + 1, nextToken.index + 2))

                    currentIndex += nextToken.index + nextToken.token.startSymbol.length + 1
                }

                Token.PRE_PROCESSOR -> {
                    currentScope.indexOf(nextToken.token.endSymbol!!, nextToken.index + 1).let { index ->
                        check(index != -1 && index > nextToken.index + nextToken.token.startSymbol.length) {
                            throwNoClosingToken(nextToken.token)
                        }

                        currentScope.substring(nextToken.index + nextToken.token.startSymbol.length until index).also {
                            val nbspPattern = "nbsp"
                            currentIndex += nextToken.index + nextToken.token.startSymbol.length
                            if (it == nbspPattern) {
                                chunks.add(Chunk.NonBreakingSpace(activeStyles))
                                // +1 to include closing parenthesis
                                currentIndex += nbspPattern.length + 1
                            } else {
                                throw Exception(
                                    "Unknown control sequence '{$it}' found. Escape via \\{ if this " +
                                            "was not intended"
                                )
                            }

                        }
                    }
                }

                Token.BOLD -> {
                    activeStyles = activeStyles.copy(bold = !activeStyles.bold)
                    currentIndex += nextToken.index + nextToken.token.startSymbol.length
                }

                Token.ITALIC -> {
                    activeStyles = activeStyles.copy(italic = !activeStyles.italic)
                    currentIndex += nextToken.index + nextToken.token.startSymbol.length
                }

                Token.SUPERSCRIPT -> {
                    activeStyles = activeStyles.copy(superscript = !activeStyles.superscript)
                    currentIndex += nextToken.index + nextToken.token.startSymbol.length
                }

                Token.SUBSCRIPT -> {
                    activeStyles = activeStyles.copy(subscript = !activeStyles.subscript)
                    currentIndex += nextToken.index + nextToken.token.startSymbol.length
                }

                Token.REFERENCE -> {
                    currentScope.indexOf(nextToken.token.endSymbol!!).let {
                        check(it != -1) {
                            throwNoClosingToken(nextToken.token)
                        }
                        chunks.add(
                            referenceOf(
                                currentScope.substring(
                                    nextToken.index + nextToken.token.startSymbol.length,
                                    it
                                )
                            )
                        )
                        currentIndex += it + nextToken.token.endSymbol.length
                    }
                }

                Token.ANCHOR -> {
                    currentScope.indexOf(nextToken.token.endSymbol!!).let {
                        check(it != -1) {
                            throwNoClosingToken(nextToken.token)
                        }

                        chunks.add(
                            anchorOf(
                                currentScope.substring(
                                    nextToken.index + nextToken.token.startSymbol.length,
                                    it
                                )
                            )
                        )
                        currentIndex += it + nextToken.token.endSymbol.length
                    }
                }

                Token.LINK -> processMacro(currentScope, nextToken.token, nextToken.index)?.also { macro ->
                    require(macro.macroText.isNotBlank()) {
                        "Missing URL for link macro."
                    }

                    val label = macro.bracketText.split(",").map { it.trim() }.first()
                    when (label.isEmpty()) {
                        true -> chunks.add(Chunk.Link(macro.macroText, macro.macroText, activeStyles))
                        false -> chunks.add(Chunk.Link(macro.macroText, label, activeStyles))
                    }
                }

                Token.FOOTNOTE -> processMacro(currentScope, nextToken.token, nextToken.index)?.also { macro ->
                    chunks.add(Chunk.Footnote(macro.macroText, macro.bracketText))
                }
            }
        }

        val allTokensClosed = activeStyles.let {
            !it.bold && !it.italic && !it.monospace && !it.subscript && !it.superscript
        }
        check(allTokensClosed) {
            throw Exception("At least one start token misses an end token")
        }
    }

    private fun processMacro(scope: String, token: Token, tokenIndex: Int): Macro? {
        var offset = 0
        while (true) {
            val indexOfClosingBracket = scope.indexOf("]", offset)
            if (indexOfClosingBracket == -1) {
                appendTextWithCurrentStyle(
                    scope.substring(
                        tokenIndex,
                        tokenIndex + token.startSymbol.length
                    )
                )
                currentIndex += tokenIndex + token.startSymbol.length
                return null
            }

            val textPlusAttr = scope.substring(
                tokenIndex + token.startSymbol.length,
                indexOfClosingBracket + 1
            )

            val match = macroPattern.find(textPlusAttr)
            if (match != null) {
                val (macroText, macroAttributes) = match.destructured
                currentIndex += indexOfClosingBracket + 1
                return Macro(macroText, macroAttributes)
            } else {
                offset = indexOfClosingBracket + 1 // jump over former occurrence of closing bracket
            }
        }
    }

    private fun splitCommaSeparatedContent(content: String) = content
        .split(delimiters = arrayOf(","), limit = 2)
        .map { it.trim() }
        .let {
            it.toMutableList().apply {
                if (size == 1) {
                    add("")
                }
            }
        }
        .toList()

    private fun anchorOf(content: String) =
        splitCommaSeparatedContent(content).let { Chunk.Anchor(it[0], it[1], activeStyles) }

    private fun referenceOf(content: String) =
        splitCommaSeparatedContent(content).let {
            val numberedItem = numbered[it[0]]
            Chunk.Reference(
                it[0],
                it[1].ifEmpty { numberedItem?.label ?: it.first() },
                activeStyles.copy(),
                numberedItem != null,
                it[1].isNotEmpty() || numberedItem?.label != null
            )
        }

    private fun appendBlankWithCurrentStyle() {
        appendTextWithCurrentStyle(" ")
    }

    private fun appendTextWithCurrentStyle(text: String) {
        chunks.add(Chunk.Styled(StringEscapeUtils.unescapeHtml4(text), activeStyles.copy()))
    }

    private fun throwNoClosingToken(token: Token) {
        throw Exception("Missing closing token ${token.endSymbol} for ${token.startSymbol}")
    }

    private fun findNextToken(text: String): TokenMatch? {
        var startIndex = 0
        while (true) {
            val index = text.indexOfAny(startTokens, startIndex)

            // nothing found at all, exit
            if (index == -1) {
                return null
            }

            // skip symbol, next iteration (case: escaped escape symbol)
            if (index > 0 && text[index - 1] == '\\') {
                startIndex = index + 1
                continue
            }

            // symbol found
            val textFromToken = text.substring(index)
            when (val symbolString = startTokens.firstOrNull { token -> textFromToken.startsWith(token) }) {
                null -> {
                    startIndex = index + 1
                    continue
                }

                else -> {
                    return TokenMatch(Token.from(symbolString), index)
                }
            }
        }
    }

    /**
     * Chunk system.
     */
    sealed interface Chunk {

        /**
         * Designates a text chunk of a certain style.
         *
         * @property text the chunk's text.
         * @property style the chunk's style.
         */
        data class Styled(val text: String, val style: ActiveStyles = ActiveStyles()) : Chunk

        /**
         * Designates a reference.
         *
         * @property name the reference identifier.
         * @property label the reference label.
         * @property style the style of the label.
         * @property numbering specifies if the reference is supposed to be numbered (true) or not (false).
         *                     Note that numbering is only correct is the tokenizer received the set of numbered references.
         * @property customLabel true if this particular reference is supposed to be rendered with the given label, false otherwise.
         *                       By default, the label is overridden by the referenced block title/reftext.
         */
        data class Reference(
            val name: String,
            val label: String,
            val style: ActiveStyles,
            val numbering: Boolean,
            val customLabel: Boolean
        ) : Chunk

        /**
         * Designates an inline text anchor.
         *
         * @property name the anchor's identifier.
         * @property label the reference label.
         * @property style the style of the label.
         */
        data class Anchor(val name: String, val label: String, val style: ActiveStyles) : Chunk

        /**
         * Designates a link resolved by the link macro.
         *
         * @property url the URL of the link object.
         * @property label the link label.
         * @property style the style of the label.
         */
        data class Link(val url: String, val label: String, val style: ActiveStyles) : Chunk

        /**
         * Designates a footnote resolved by the footnote macro.
         *
         * @property name the footnote's identifier.
         * @property text the footnote text (which can contain unparsed asciidoc markup!).
         */
        data class Footnote(val name: String, val text: String) : Chunk

        data class NonBreakingSpace(val style: ActiveStyles = ActiveStyles()) : Chunk
    }

    /**
     * Text styles of a chunk.
     */
    data class ActiveStyles(
        val bold: Boolean = false,
        val italic: Boolean = false,
        val monospace: Boolean = false,
        val subscript: Boolean = false,
        val superscript: Boolean = false,
        val strikethrough: Boolean = false,
        val underlined: Boolean = false
    )

    private data class Macro(val macroText: String, val bracketText: String)

    private enum class Token(val startSymbol: String, val endSymbol: String? = null) {
        PRE_PROCESSOR("{", "}"),
        REFERENCE("<<", ">>"),
        BOLD("**", "**"),
        ITALIC("__", "__"),
        SUPERSCRIPT("^", "^"),
        SUBSCRIPT("~", "~"),
        MONO("`", "`"),
        ANCHOR("[[", "]]"),
        LINK("link:"),
        FOOTNOTE("footnote:"),
        ESCAPED("\\");

        companion object {
            fun from(symbolString: String) = entries
                .firstOrNull { it.startSymbol == symbolString }
                ?: throw Exception("Expected one of ${entries.map { it.startSymbol }}, but found: $symbolString")
        }
    }

    private data class TokenMatch(val token: Token, val index: Int)

    private companion object {
        val startTokens = Token.entries.map { it.startSymbol }.toList()
        val macroPattern = """^(.*)\[(.*)]$""".toRegex()
    }
}

interface StyleModifier {
    operator fun invoke(style: BlockTokenizer.ActiveStyles): BlockTokenizer.ActiveStyles
}

object KeepStyle : StyleModifier {
    override operator fun invoke(style: BlockTokenizer.ActiveStyles): BlockTokenizer.ActiveStyles {
        return style
    }
}

fun newStyleForChunk(chunk: Chunk, style: StyleModifier): Chunk {
    return when (chunk) {
        is Chunk.Anchor -> Chunk.Anchor(chunk.name, chunk.label, style(chunk.style))
        is Chunk.Footnote -> chunk
        is Chunk.Link -> Chunk.Link(chunk.url, chunk.label, style(chunk.style))
        is Chunk.NonBreakingSpace -> Chunk.NonBreakingSpace(style(chunk.style))
        is Chunk.Reference -> Chunk.Reference(
            chunk.name,
            chunk.label,
            style(chunk.style),
            chunk.numbering,
            chunk.customLabel
        )

        is Chunk.Styled -> Chunk.Styled(chunk.text, style(chunk.style))
    }
}

fun newStyleForTokenizedText(tokenizedText: TokenizedText, style: StyleModifier): TokenizedText {
    return TokenizedText(
        tokenizedText.chunks.map { newStyleForChunk(it, style) }
    )
}