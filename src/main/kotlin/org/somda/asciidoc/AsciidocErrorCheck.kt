package org.somda.asciidoc

import org.apache.logging.log4j.kotlin.Logging
import java.io.ByteArrayOutputStream
import java.io.Closeable
import java.io.PrintStream

/**
 * Scans error stream and checks for Asciidoc error messages.
 *
 * Starts capturing the process' error stream when initialized, seeks Asciidoc error messages and throws if at least
 * one is found on close.
 */
class AsciidocErrorChecker : Closeable {
    private val errorStream = ByteArrayOutputStream().also {
        System.setErr(PrintStream(it))
    }

    override fun close() {
        val errors = errorStream.toByteArray().decodeToString().split("\n").count { line ->
            when (val errorPrefix = errorHints.firstOrNull { line.startsWith(it, true) }) {
                null -> false
                else -> logger.error {
                    "Asciidoc issue detected: ${line.substring(errorPrefix.length)}"
                }.let { true }
            }
        }

        if (errors > 0) {
            throw Exception("Found $errors Asciidoc error(s) that need to be fixed (see previous output)")
        }
    }

    private companion object : Logging {
        val errorHints = listOf("information: ", "info: ")
    }
}
