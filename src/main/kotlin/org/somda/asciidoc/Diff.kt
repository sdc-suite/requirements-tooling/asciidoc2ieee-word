package org.somda.asciidoc

import dev.gitlive.difflib.DiffUtils
import dev.gitlive.difflib.patch.DeltaType

sealed interface DiffChunk {
    data class Plain(
        val text: String
    ) : DiffChunk

    data class Insert(
        val text: String
    ) : DiffChunk

    data class Delete(
        val text: String
    ) : DiffChunk

    data class Change(
        val deletedText: String,
        val insertedText: String
    ) : DiffChunk
}

data class DiffSequence(
    val chunks: List<DiffChunk>
)

object Diff {
    fun from(original: String, revised: String): DiffSequence {
        val originalWords = original.split("""\s""".toRegex())
        val revisedWords = revised.split("""\s""".toRegex())

        val deltas = DiffUtils.diff(originalWords, revisedWords).getDeltas()

        var lastDelta = 0
        var currentDelta: Int

        val result = mutableListOf<DiffChunk>()

        for (delta in deltas) {
            currentDelta = delta.source.position
            when (delta.type) {
                DeltaType.DELETE -> {
                    result.add(DiffChunk.Plain(originalWords.subList(lastDelta, currentDelta).joinToString(" ")))
                    result.add(DiffChunk.Delete(delta.source.lines!!.joinToString(" ")))
                    lastDelta = currentDelta + delta.source.lines!!.size
                }

                DeltaType.CHANGE -> {
                    result.add(DiffChunk.Plain(originalWords.subList(lastDelta, currentDelta).joinToString(" ")))
                    result.add(
                        DiffChunk.Change(
                            delta.source.lines!!.joinToString(" "),
                            delta.target.lines!!.joinToString(" ")
                        )
                    )
                    lastDelta = currentDelta + delta.source.lines!!.size
                }

                DeltaType.INSERT -> {
                    result.add(DiffChunk.Plain(originalWords.subList(lastDelta, currentDelta).joinToString(" ")))
                    result.add(DiffChunk.Insert(delta.target.lines!!.joinToString(" ")))
                    lastDelta = currentDelta
                }
                else -> Unit
            }
        }

        // copy remaining text
        if (lastDelta < originalWords.size) {
            originalWords.subList(lastDelta, originalWords.size).also {
                if (it.isNotEmpty()) {
                    result.add(DiffChunk.Plain(it.joinToString(" ")))
                }
            }
        }

        return DiffSequence(result)
    }
}