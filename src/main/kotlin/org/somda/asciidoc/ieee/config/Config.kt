package org.somda.asciidoc.ieee.config

import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class Config(
    val requirements: Requirements? = null
)
