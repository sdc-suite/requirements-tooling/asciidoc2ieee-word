package org.somda.asciidoc.ieee.config

import kotlinx.serialization.Serializable

@Serializable
data class Requirements (val numbers: List<Long>)