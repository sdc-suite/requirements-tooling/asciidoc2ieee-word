package org.somda.asciidoc.ieee

import org.asciidoctor.ast.*
import org.asciidoctor.ast.List
import org.asciidoctor.extension.Contexts
import org.somda.asciidoc.isAppendix

const val BLOCK_NAME_IEEE_REQUIREMENT = "ieee_requirement"
const val BLOCK_NAME_IEEE_CHANGE = "ieee_change"

private fun blockType(attributes: Map<String, Any>): String? =
    attributes.entries.firstOrNull { (t, _) -> t == "1" }?.value.toString()

/**
 * Creates an [AsciidocNode] from a structural node.
 *
 * Currently, the following blocks are recognized:
 *
 * - section
 * - preface
 * - table
 * - image
 * - unordered list
 * - ordered list
 * - definition list
 * - paragraph
 * - document
 * - preamble
 * - sidebar
 * - custom IEEE requirement block
 */
fun StructuralNode.toSealed(): AsciidocNode {
    return when (this.context) {
        "section" -> when (blockType(attributes)) {
            "preface" -> AsciidocNode.Preface(this as Section)
            else -> AsciidocNode.Section(this as Section)
        }

        "table" -> AsciidocNode.Table(this as Table)
        "image" -> AsciidocNode.Image(this as Block)
        "ulist" -> AsciidocNode.UnorderedList(this as List)
        "olist" -> AsciidocNode.OrderedList(this as List)
        "dlist" -> AsciidocNode.Description(this as DescriptionList)
        "list_item" -> AsciidocNode.ListItem(this as ListItem)
        "document" -> AsciidocNode.Document(this as Document)
        "paragraph" -> AsciidocNode.Paragraph(this as Block)
        "admonition" -> AsciidocNode.Admonition(this as Block)
        "preamble" -> AsciidocNode.Preamble(this as Block)
        "preface" -> AsciidocNode.Preface(this as Section)
        "floating_title" -> AsciidocNode.Discrete(this as Block)
        "listing" -> AsciidocNode.ComputerCode(this as Block)
        "sidebar" -> when (blockType(attributes)) {
            BLOCK_NAME_IEEE_REQUIREMENT -> AsciidocNode.Requirement(this as Block)
            else -> AsciidocNode.Sidebar(this as Block)
        }

        "open" -> when (blockType(attributes)) {
            BLOCK_NAME_IEEE_CHANGE -> AsciidocNode.Change(this as Block)
            else -> throw Exception("Open blocks are not supported by the converter")
        }

        else -> AsciidocNode.Unknown(this).also { println("UNKNOWN: $context") }
    }
}

/**
 * Wrapper class for improved functional dispatching.
 *
 * @property children all child blocks of a structural node as [AsciidocNode].
 * @property attributes attributes of this block with convenient access via [Attributes].
 */
sealed class AsciidocNode(structuralNode: StructuralNode? = null) {
    val children = structuralNode?.let { structuralNode.blocks.map { it.toSealed() } } ?: listOf()
    val attributes = Attributes(structuralNode?.attributes ?: mutableMapOf())

    /**
     * Represents an asciidoc section.
     */
    data class Section(val wrapped: org.asciidoctor.ast.Section) : AsciidocNode(wrapped) {
        private companion object {
            const val NORMATIVE = "normative"
            const val INFORMATIVE = "informative"
        }

        /**
         * Checks if the section is supposed to be skipped from the resulting Word document.
         *
         * @see [Attribute.IEEE_SKIP]
         */
        fun skipped() = attributes.has(Attribute.IEEE_SKIP)

        /**
         * Checks if the section represents the definitions clause.
         *
         * @see [Attribute.IEEE_DEFINITIONS]
         */
        fun containsDefinitions() = attributes.has(Attribute.IEEE_DEFINITIONS)

        /**
         * Checks if the section represents the abbreviations clause.
         *
         * @see [Attribute.IEEE_ABBREVIATIONS]
         */
        fun containsAbbreviations() = attributes.has(Attribute.IEEE_ABBREVIATIONS)

        /**
         * Checks if the section represents the normative references clause.
         *
         * @see [Attribute.IEEE_BIBLIOGRAPHY_GROUP]
         */
        fun containsNormativeReferences() =
            attributes[Attribute.IEEE_BIBLIOGRAPHY_GROUP] == NORMATIVE

        /**
         * Checks if the section represents the informative references clause.
         *
         * @see [Attribute.IEEE_BIBLIOGRAPHY_GROUP]
         */
        fun containsInformativeReferences() =
            attributes[Attribute.IEEE_BIBLIOGRAPHY_GROUP] == INFORMATIVE

        /**
         * Checks if the clause is the overview clause.
         *
         * @see [Attribute.IEEE_OVERVIEW]
         */
        fun isOverview() = attributes.has(Attribute.IEEE_OVERVIEW)

        /**
         * Checks is the clause is an asciidoc appendix.
         */
        fun isAppendix() = wrapped.isAppendix()

    }

    data class Document(val wrapped: org.asciidoctor.ast.Document) : AsciidocNode(wrapped)
    data class Sidebar(val wrapped: Block) : AsciidocNode(wrapped)
    data class Requirement(val wrapped: Block) : AsciidocNode(wrapped)
    data class Change(val wrapped: Block) : AsciidocNode(wrapped)
    data class Paragraph(val wrapped: Block) : AsciidocNode(wrapped)
    data class Admonition(val wrapped: Block) : AsciidocNode(wrapped)
    data class Preamble(val wrapped: Block) : AsciidocNode(wrapped)
    data class Preface(val wrapped: org.asciidoctor.ast.Section) : AsciidocNode(wrapped)
    data class UnorderedList(val wrapped: List) : AsciidocNode(wrapped)
    data class OrderedList(val wrapped: List) : AsciidocNode(wrapped)
    data class ListItem(val wrapped: org.asciidoctor.ast.ListItem) : AsciidocNode(wrapped)
    data class Description(val wrapped: DescriptionList) : AsciidocNode(wrapped)
    data class Unknown(val wrapped: StructuralNode) : AsciidocNode()
    data class Image(val wrapped: Block) : AsciidocNode(wrapped)
    data class Table(val wrapped: org.asciidoctor.ast.Table) : AsciidocNode(wrapped)

    data class ComputerCode(val wrapped: Block): AsciidocNode(wrapped)

    data class Discrete(val wrapped: Block): AsciidocNode(wrapped)
}

