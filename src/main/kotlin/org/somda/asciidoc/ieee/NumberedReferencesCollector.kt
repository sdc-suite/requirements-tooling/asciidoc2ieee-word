package org.somda.asciidoc.ieee

import org.asciidoctor.ast.Document
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.Treeprocessor
import org.somda.asciidoc.id
import org.somda.asciidoc.refText

data class NumberedReferenceItem(
    val name: String,
    val label: String?
)

/**
 * Asciidoc plugin to collect all references from the asciidoc input that are supposed to be numbered.
 *
 * Numbered references are references to:
 *
 * - Sections
 * - Images
 * - Tables
 */
class NumberedReferencesCollector : Treeprocessor() {
    private val numberedReferences = mutableMapOf<String, NumberedReferenceItem>()

    /**
     * After the plugin has run, this function returns a set of all collected references.
     */
    fun numberedReferences(): Map<String, NumberedReferenceItem> = numberedReferences

    override fun process(document: Document): Document {
        processBlock(document as StructuralNode)
        return document
    }

    private fun processBlock(block: StructuralNode) {
        block.toSealed().also { node ->
            when (node) {
                is AsciidocNode.Section -> {
                    node.attributes.id()?.also {
                        numberedReferences[it] = NumberedReferenceItem(
                            it, node.attributes.refText()
                        )
                    }
                }

                is AsciidocNode.Image -> {
                    node.attributes.id()?.also {
                        numberedReferences[it] = NumberedReferenceItem(
                            it, node.attributes.refText()
                        )
                    }
                }

                is AsciidocNode.Table -> {
                    node.attributes.id()?.also {
                        numberedReferences[it] = NumberedReferenceItem(
                            it, node.attributes.refText()
                        )
                    }
                }

                else -> Unit
            }
        }

        block.blocks.forEach { processBlock(it) }
    }
}