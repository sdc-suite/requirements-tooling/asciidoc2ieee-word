package org.somda.asciidoc.ieee

import org.apache.logging.log4j.kotlin.Logging
import org.asciidoctor.ast.Row
import org.somda.asciidoc.AttributedTerm
import org.somda.asciidoc.BlockTokenizer
import org.somda.asciidoc.KeepStyle
import org.somda.asciidoc.StyleModifier
import org.somda.asciidoc.TokenizedText
import org.somda.asciidoc.id
import org.somda.asciidoc.ieee.model.Abbreviation
import org.somda.asciidoc.ieee.model.Admonition
import org.somda.asciidoc.ieee.model.AnnexSection
import org.somda.asciidoc.ieee.model.AnnexSectionRoot
import org.somda.asciidoc.ieee.model.AnnexTitle
import org.somda.asciidoc.ieee.model.BibliographicItem
import org.somda.asciidoc.ieee.model.Bookmark
import org.somda.asciidoc.ieee.model.Change
import org.somda.asciidoc.ieee.model.ChangeType
import org.somda.asciidoc.ieee.model.ChangeType.CHANGE
import org.somda.asciidoc.ieee.model.ChangeType.CUSTOM
import org.somda.asciidoc.ieee.model.ChangeType.DELETE
import org.somda.asciidoc.ieee.model.ChangeType.DELETE_TABLE_ROWS
import org.somda.asciidoc.ieee.model.ChangeType.INSERT
import org.somda.asciidoc.ieee.model.ChangeType.INSERT_FIGURE
import org.somda.asciidoc.ieee.model.ChangeType.INSERT_TABLE
import org.somda.asciidoc.ieee.model.ChangeType.INSERT_TABLE_ROWS
import org.somda.asciidoc.ieee.model.ChangeType.REPLACE_FIGURE
import org.somda.asciidoc.ieee.model.ChangeType.REPLACE_TABLE
import org.somda.asciidoc.ieee.model.ChangeType.REPLACE_TABLE_ROWS
import org.somda.asciidoc.ieee.model.ChangeType.REPLACE_TEXT
import org.somda.asciidoc.ieee.model.ComputerCode
import org.somda.asciidoc.ieee.model.DecoratedText
import org.somda.asciidoc.ieee.model.Definition
import org.somda.asciidoc.ieee.model.GenericTitle
import org.somda.asciidoc.ieee.model.Image
import org.somda.asciidoc.ieee.model.Normativity
import org.somda.asciidoc.ieee.model.OrderedList
import org.somda.asciidoc.ieee.model.OrderedListItem
import org.somda.asciidoc.ieee.model.Paragraph
import org.somda.asciidoc.ieee.model.RegularSection
import org.somda.asciidoc.ieee.model.RegularTitle
import org.somda.asciidoc.ieee.model.Requirement
import org.somda.asciidoc.ieee.model.RequirementMetadata
import org.somda.asciidoc.ieee.model.Table
import org.somda.asciidoc.ieee.model.Title
import org.somda.asciidoc.ieee.model.Title.Type.FIGURE
import org.somda.asciidoc.ieee.model.Title.Type.TABLE
import org.somda.asciidoc.ieee.model.UnorderedList
import org.somda.asciidoc.newStyleForChunk
import org.somda.asciidoc.newStyleForTokenizedText
import org.somda.asciidoc.refText
import java.io.File
import java.util.*

/**
 * Maps from asciidoc blocks to internal data model.
 */
class AsciidocToModelMapping(
    private val requirementsMetadata: Map<Int, RequirementMetadata>,
    private val numberedReferences: Map<String, NumberedReferenceItem>,
    private val baseDir: File
) {
    private val tableReader = TableAsciidocReader(this)

    companion object : Logging

    inline fun <reified T : BibliographicItem> toReferences(
        node: AsciidocNode.UnorderedList,
        createBibItem: (bookmark: Bookmark, text: DecoratedText) -> T
    ): List<T> {
        return node.wrapped.items.map {
            it.toSealed()
        }.filterIsInstance<AsciidocNode.ListItem>().map {
            toReference(it, createBibItem)
        }.toList()
    }

    inline fun <reified T : BibliographicItem> toReference(
        item: AsciidocNode.ListItem,
        createBibItem: (bookmark: Bookmark, text: DecoratedText) -> T
    ): T {
        val (id, refText, text) = AttributedTerm(
            item.wrapped.source, AttributedTerm.TermStyle.BIB_REF
        ).get()
        checkNotNull(id) { "Bibliographic items require an identifier" }
        checkNotNull(refText) { "Bibliographic items require a reference text" }
        return createBibItem(Bookmark(id, refText), DecoratedText(blockTokenizerFor(text).get()))
    }

    fun toDefinitions(node: AsciidocNode.Section): List<Definition> {
        return node.wrapped.blocks.map { it.toSealed() }.filterIsInstance<AsciidocNode.Section>()
            .map { definitionItem ->
                toDefinition(definitionItem)
            }
    }

    fun toAnnexSection(node: AsciidocNode.Section): AnnexSection {
        val bookmark = bookmarkFrom(node.attributes)
        return AnnexSection(node.wrapped.title, bookmark, node.children.mapNotNull { childNode ->
            when (childNode) {
                is AsciidocNode.Section -> childNode.attributes[Attribute.IEEE_SKIP]?.let { null } ?: toAnnexSection(
                    childNode
                )

                is AsciidocNode.Paragraph -> toParagraph(childNode)
                is AsciidocNode.ComputerCode -> toParagraph(childNode)
                is AsciidocNode.OrderedList -> toParagraph(childNode)
                is AsciidocNode.UnorderedList -> toParagraph(childNode)
                is AsciidocNode.Requirement -> toRequirement(childNode)
                is AsciidocNode.Change -> toChange(childNode)
                is AsciidocNode.Image -> toImage(childNode) { t, bm -> RegularTitle(t, bm, Title.Type.FIGURE) }
                is AsciidocNode.Table -> toTable(childNode) { t, bm -> RegularTitle(t, bm, Title.Type.TABLE) }
                else -> null
            }
        })
    }

    fun toRegularSection(node: AsciidocNode.Section): RegularSection {
        val bookmark = bookmarkFrom(node.attributes)
        return RegularSection(node.wrapped.title, bookmark, node.children.mapNotNull { childNode ->
            when (childNode) {
                is AsciidocNode.Section -> childNode.attributes[Attribute.IEEE_SKIP]?.let { null } ?: toRegularSection(
                    childNode
                )

                is AsciidocNode.Paragraph -> toParagraph(childNode)
                is AsciidocNode.ComputerCode -> toParagraph(childNode)
                is AsciidocNode.OrderedList -> toParagraph(childNode)
                is AsciidocNode.UnorderedList -> toParagraph(childNode)
                is AsciidocNode.Requirement -> toRequirement(childNode)
                is AsciidocNode.Change -> toChange(childNode)
                is AsciidocNode.Image -> toImage(childNode) { t, bm -> RegularTitle(t, bm, Title.Type.FIGURE) }
                is AsciidocNode.Table -> toTable(childNode) { t, bm -> RegularTitle(t, bm, Title.Type.TABLE) }
                else -> null
            }
        })
    }

    private fun toTable(node: AsciidocNode.Table, getTitle: (text: DecoratedText, bookmark: Bookmark) -> Title): Table {
        val rowCount = node.attributes[Attribute.ROW_COUNT]?.toIntOrNull()
        val colCount = node.attributes[Attribute.COL_COUNT]?.toIntOrNull()
        checkNotNull(rowCount) {
            "Row count for table is missing"
        }
        checkNotNull(colCount) {
            "Column count for table is missing"
        }

        val rows = mutableListOf<Table.Row>().apply {
            node.wrapped.header.firstOrNull()?.also { row ->
                add(toTableRow(row) { cells -> Table.HeaderRow(cells) })
            }
        }
        rows.addAll(node.wrapped.body.map { row -> toTableRow(row) { cells -> Table.DataRow(cells) } })

        val orientation = node.attributes[Attribute.IEEE_ORIENTATION].let {
            when (it?.lowercase() == "landscape") {
                true -> Table.PageOrientation.LANDSCAPE
                false -> Table.PageOrientation.PORTRAIT
            }
        }

        return Table(
            getTitle(toDecoratedText(node.wrapped.title), bookmarkFrom(node.attributes)),
            rows,
            orientation,
            rowCount,
            colCount,
            node.attributes[Attribute.TABLE_WIDTH_PERCENTAGE]?.toInt() ?: 100,
            node.attributes[Attribute.COLS]?.split(",")?.map { it.trim().toInt() }
                ?: mutableListOf<Int>().apply {
                    repeat(colCount) {
                        add(1)
                    }
                }
        )
    }

    fun <T> toTableRow(row: Row, convert: (List<Table.Cell>) -> T): T = convert(row.cells.map { cell ->
        val cellAttributes = Attributes(cell.attributes)
        Table.Cell(
            when (cellAttributes[Attribute.STYLE]) {
                "asciidoc" -> tableReader.resolveBlocks(cell.innerDocument)
                else -> listOf(toDecoratedText(cell.text))
            },
            cell.rowspan,
            cell.colspan,
            cellAttributes[Attribute.HORIZONTAL_ALIGN].let {
                when (it) {
                    "left" -> Table.Cell.HorizontalAlign.LEFT
                    "center" -> Table.Cell.HorizontalAlign.CENTER
                    "right" -> Table.Cell.HorizontalAlign.RIGHT
                    else -> Table.Cell.HorizontalAlign.LEFT
                }
            },
            cellAttributes[Attribute.VERTICAL_ALIGN].let {
                when (it) {
                    "top" -> Table.Cell.VerticalAlign.TOP
                    "middle" -> Table.Cell.VerticalAlign.MIDDLE
                    "bottom" -> Table.Cell.VerticalAlign.BOTTOM
                    else -> Table.Cell.VerticalAlign.TOP
                }
            }
        )
    })

    fun bookmarkFrom(attributes: Attributes) = attributes.id()?.let {
        Bookmark(it, attributes.refText(), true)
    } ?: Bookmark(UUID.randomUUID().toString(), attributes.refText(), true)

    fun toAnnexSectionRoot(node: AsciidocNode.Section, isAmendmentCorrigenda: Boolean): AnnexSectionRoot {
        check(node.attributes.has(Attribute.IEEE_NORMATIVITY)) {
            "Annex requires ieee_normativity attribute (normative/informative)"
        }

        val normativity = when (node.attributes[Attribute.IEEE_NORMATIVITY]) {
            "normative" -> Normativity.NORMATIVE
            "informative" -> Normativity.INFORMATIVE
            else -> throw Exception("Annex requires ieee_normativity attribute being 'normative' or 'informative')")
        }

        val letterOverride = node.attributes[Attribute.IEEE_CHANGE_WHERE]?.first()
        require(!isAmendmentCorrigenda || letterOverride != null) {
            "Annex lacks ${Attribute.IEEE_CHANGE_WHERE.key} attribute"
        }
        require(letterOverride == null || (letterOverride in 'A'..'Z')) {
            "Annex requires ${Attribute.IEEE_CHANGE_WHERE.key} attribute in the range of A to Z"
        }

        val bookmark = bookmarkFrom(node.attributes)
        return AnnexSectionRoot(node.wrapped.title, bookmark, node.children.mapNotNull { childNode ->
            when (childNode) {
                is AsciidocNode.Section -> childNode.attributes[Attribute.IEEE_SKIP]?.let { null } ?: toAnnexSection(
                    childNode
                )

                is AsciidocNode.Paragraph -> toParagraph(childNode)
                is AsciidocNode.ComputerCode -> toParagraph(childNode)
                is AsciidocNode.OrderedList -> toParagraph(childNode)
                is AsciidocNode.UnorderedList -> toParagraph(childNode)
                is AsciidocNode.Requirement -> toRequirement(childNode)
                is AsciidocNode.Change -> toChange(childNode)
                is AsciidocNode.Image -> toImage(childNode) { titleText, titleBookmark ->
                    AnnexTitle(
                        titleText,
                        titleBookmark,
                        Title.Type.FIGURE,
                        node.wrapped.numeral
                    )
                }

                is AsciidocNode.Table -> toTable(childNode) { titleText, titleBookmark ->
                    AnnexTitle(
                        titleText,
                        titleBookmark,
                        Title.Type.TABLE,
                        node.wrapped.numeral
                    )
                }

                else -> null
            }
        }, normativity, letterOverride)
    }

    private fun toDefinition(node: AsciidocNode.Section): Definition {
        val attributes = Attributes(node.wrapped.attributes)
        val term = node.wrapped.title
        val refText =
            attributes.refText() ?: throw Exception("Definition $term has no reference text, but needed")
        val identifier = attributes.id() ?: throw Exception("Definition $term has no identifier, but needed")

        check(refText == term) {
            "Title of definition $term does not match reference text: $refText"
        }

        check(node.wrapped.blocks.isNotEmpty()) {
            "Definition for $term does not have a text, but is required"
        }

        val (text, notes) = node.wrapped.blocks.map { it.toSealed() }.let { children ->
            Pair(children.filter {
                it is AsciidocNode.Paragraph || it is AsciidocNode.UnorderedList || it is AsciidocNode.OrderedList
            }.map {
                toParagraph(it)
            }, children.filterIsInstance<AsciidocNode.Admonition>().map {
                toAdmonition(it)
            })
        }

        return Definition(
            term = term,
            bookmark = Bookmark(identifier, term),
            text = text, notes =
            notes
        )
    }

    fun toImage(node: AsciidocNode.Image, getTitle: (text: DecoratedText, bookmark: Bookmark) -> Title): Image {
        val folderStr = node.attributes[Attribute.IMAGE_FOLDER] ?: ""
        val imagesFolder = if (folderStr.isNotBlank()) File(baseDir, folderStr) else baseDir
        require(imagesFolder.isDirectory && imagesFolder.exists()) {
            "Images folder does not exist: ${imagesFolder.absolutePath}"
        }

        val id = node.attributes.id() ?: "<unknown-id>"
        val target = node.attributes[Attribute.TARGET] ?: ""
        require(target.isNotBlank()) {
            "Missing file path for image with id: $id"
        }

        val targetFile = File(target)
        // perform extra check since plantuml diagram targets are complete file paths
        val imageFile = when (targetFile.exists()) {
            true -> targetFile
            false -> File(imagesFolder, target)
        }

        require(imageFile.isFile && imageFile.exists()) {
            "Image not found: ${imageFile.absolutePath}"
        }

        return Image(
            getTitle(
                toDecoratedText(node.wrapped.title),
                bookmarkFrom(node.attributes),
            ),
            imageFile,
            node.attributes[Attribute.ALTERNATIVE_TEXT] ?: ""
        )
    }

    fun toAbbreviations(node: AsciidocNode.Description): List<Abbreviation> {
        return node.wrapped.items.map { descriptionItem ->
            val (id, refText, term) = AttributedTerm(
                descriptionItem.terms.first().source, AttributedTerm.TermStyle.DESCRIPTION
            ).get()

            check(refText == null || refText == term) {
                "Title of abbreviation $term does not match reference text: $refText"
            }

            Abbreviation(
                term = term,
                bookmark = id?.let { Bookmark(id, refText) },
                text = toDecoratedText(descriptionItem.description.source),
            )
        }
    }

    fun toBookmarkFromAttributes(attributeMap: MutableMap<String, Any>, numbered: Boolean = false): Bookmark? {
        return toBookmarkFromAttributes(Attributes(attributeMap), numbered)
    }

    fun toBookmarkFromAttributes(attributes: Attributes, numbered: Boolean = false): Bookmark? {
        return attributes.id()?.let {
            Bookmark(it, attributes.refText(), numbered)
        }
    }

    fun toOrderedList(
        node: AsciidocNode.OrderedList,
        depth: Int = 0,
        styleModifier: StyleModifier = KeepStyle
    ): OrderedList =
        node.wrapped.items.mapIndexed { index, item ->
            item.toSealed().let { asciidocNode ->
                check(asciidocNode is AsciidocNode.ListItem) { "Expected ListItem, but found: ${item.javaClass}" }
                val childList = asciidocNode.children.filterIsInstance<AsciidocNode.OrderedList>().firstOrNull()?.let {
                    toOrderedList(it, depth + 1, styleModifier)
                }
                OrderedListItem(
                    index + 1,
                    newStyleForTokenizedText(blockTokenizerFor(asciidocNode.wrapped.source).get(), styleModifier),
                    childList
                )
            }
        }.let {
            OrderedList(it, depth)
        }

    fun toUnorderedList(node: AsciidocNode.UnorderedList, styleModifier: StyleModifier = KeepStyle) =
        node.wrapped.items.map { item ->
            item.toSealed().let {
                check(it is AsciidocNode.ListItem) { "Expected ListItem, but found: ${item.javaClass}" }
                newStyleForTokenizedText(blockTokenizerFor(it.wrapped.source).get(), styleModifier)
            }
        }.let {
            UnorderedList(it)
        }

    fun toParagraph(node: AsciidocNode, styleModifier: StyleModifier = KeepStyle): Paragraph = when (node) {
        is AsciidocNode.Paragraph -> toDecoratedText(node.wrapped.lines, styleModifier)
        is AsciidocNode.OrderedList -> toOrderedList(node, 0, styleModifier)
        is AsciidocNode.UnorderedList -> toUnorderedList(node, styleModifier)
        is AsciidocNode.ComputerCode -> toComputerCode(node, styleModifier)
        else -> throw Exception("Could not process ${node.javaClass} as paragraph")
    }

    fun toComputerCode(node: AsciidocNode, styleModifier: StyleModifier = KeepStyle): ComputerCode = when (node) {
        is AsciidocNode.ComputerCode -> {
            ComputerCode(node.wrapped.lines.map {
                DecoratedText(
                    newStyleForTokenizedText(TokenizedText(listOf(BlockTokenizer.Chunk.Styled(it))), styleModifier)
                )
            })
        }

        else -> throw Exception("Could not process ${node.javaClass} as computer code")
    }

    private fun toRequirement(node: AsciidocNode.Requirement): Requirement {
        val reqId = node.attributes.id() ?: throw Exception("Requirement block is missing is")
        val reqNumber = RequirementsCollector.requirementNumberOfId(reqId)
        val requirementMetadata = requirementsMetadata[reqNumber]
            ?: throw Exception("No requirements metadata found for requirement #$reqNumber")
        val bookmark = Bookmark(reqId, requirementMetadata.blockTitle, false)

        val requirementText = node.children.filter {
            it is AsciidocNode.Paragraph || it is AsciidocNode.OrderedList || it is AsciidocNode.UnorderedList
        }.map { childNode ->
            toParagraph(childNode)
        }

        val notes = node.children.filterIsInstance<AsciidocNode.Admonition>().map { admonition ->
            toAdmonition(admonition)
        }

        return Requirement(
            requirementMetadata,
            bookmark,
            requirementText,
            notes
        )
    }

    fun toChange(node: AsciidocNode): Change {
        val changeType = ChangeType.fromName(node.attributes[Attribute.IEEE_CHANGE_TYPE] ?: "")
        val where = node.attributes[Attribute.IEEE_CHANGE_WHERE]
        val rowNumber = node.attributes[Attribute.IEEE_ROW_NUMBER]
        val tableNumber = node.attributes[Attribute.IEEE_TABLE_NUMBER]
        val figureNumber = node.attributes[Attribute.IEEE_FIGURE_NUMBER]

        validateAttributeInfoAvailability(node.attributes, changeType)

        val content = when (changeType) {
            INSERT, CUSTOM -> node.children.mapNotNull {
                when (it) {
                    is AsciidocNode.Paragraph -> toParagraph(it)
                    is AsciidocNode.OrderedList -> toParagraph(it)
                    is AsciidocNode.UnorderedList -> toParagraph(it)
                    is AsciidocNode.Requirement -> toRequirement(it)
                    is AsciidocNode.ComputerCode -> toComputerCode(it)
                    is AsciidocNode.Change -> null
                    is AsciidocNode.Image -> toImage(it) { t, bm -> RegularTitle(t, bm, FIGURE) }
                    is AsciidocNode.Table -> toTable(it) { t, bm -> RegularTitle(t, bm, TABLE) }
                    else -> null
                }
            }

            DELETE -> node.children.mapNotNull {
                when (it) {
                    is AsciidocNode.Paragraph -> toParagraph(it)
                    is AsciidocNode.OrderedList -> toParagraph(it)
                    is AsciidocNode.UnorderedList -> toParagraph(it)
                    is AsciidocNode.ComputerCode -> toComputerCode(it)
                    else -> null
                }
            }

            REPLACE_TEXT -> AsciidocChangeReader.toNodesFromOldSection(node.children).mapNotNull {
                when (it) {
                    is AsciidocNode.Paragraph -> toParagraph(it, Strikethrough)
                    is AsciidocNode.OrderedList -> toParagraph(it, Strikethrough)
                    is AsciidocNode.UnorderedList -> toParagraph(it, Strikethrough)
                    is AsciidocNode.ComputerCode -> toComputerCode(it, Strikethrough)
                    else -> null
                }
            } + AsciidocChangeReader.toNodesFromNewSection(node.children).mapNotNull {
                when (it) {
                    is AsciidocNode.Paragraph -> toParagraph(it, Underline)
                    is AsciidocNode.OrderedList -> toParagraph(it, Underline)
                    is AsciidocNode.UnorderedList -> toParagraph(it, Underline)
                    is AsciidocNode.ComputerCode -> toComputerCode(it, Underline)
                    else -> null
                }
            }

            CHANGE -> listOf(AsciidocChangeReader.toDecoratedDiffText(node.children))

            REPLACE_TABLE, INSERT_TABLE, INSERT_TABLE_ROWS, DELETE_TABLE_ROWS, REPLACE_TABLE_ROWS -> {
                node.children.filterIsInstance<AsciidocNode.Table>().map { childNode ->
                    toTable(childNode) { text, _ -> GenericTitle(tableNumber!!, text, TABLE) }
                }
            }

            REPLACE_FIGURE, INSERT_FIGURE -> {
                node.children.filterIsInstance<AsciidocNode.Image>().map { childNode ->
                    toImage(childNode) { text, _ -> GenericTitle(figureNumber!!, text, FIGURE) }
                }
            }
        }


        val headLineRaw = when (changeType) {
            INSERT -> "Insert the following text after $where:"
            DELETE -> when (content.isEmpty()) {
                true -> "Delete entire $where"
                false -> "Delete the following text from $where:"
            }

            CHANGE -> "Change the following text in $where:"
            REPLACE_TABLE -> "Replace Table $tableNumber with the following table:"
            INSERT_TABLE -> "Insert Table $tableNumber after $where:"
            INSERT_TABLE_ROWS -> "Insert the following rows into Table $tableNumber " +
                    (rowNumber?.let { "after row number $it:" } ?: "at the end:")

            DELETE_TABLE_ROWS -> "Delete the following rows from Table $tableNumber:"
            REPLACE_TABLE_ROWS -> "Replace row $rowNumber in Table $tableNumber:"
            REPLACE_FIGURE -> "Replace Figure $figureNumber with the following figure:"
            INSERT_FIGURE -> "Insert Figure $figureNumber after $where:"
            REPLACE_TEXT -> "Change the following text in $where:"
            CUSTOM -> "%s"
        }

        val headLine = DecoratedText(
            TokenizedText(
                listOf(
                    BlockTokenizer.Chunk.Styled(
                        text = headLineRaw,
                        style = BlockTokenizer.ActiveStyles(italic = true, bold = true)
                    )
                )
            )
        )

        return Change(headLine, content)
    }

    private fun validateAttributeInfoAvailability(attributes: Attributes, changeType: ChangeType) {
        val where = attributes[Attribute.IEEE_CHANGE_WHERE]
        val rowNumber = attributes[Attribute.IEEE_ROW_NUMBER]
        val tableNumber = attributes[Attribute.IEEE_TABLE_NUMBER]
        val figureNumber = attributes[Attribute.IEEE_FIGURE_NUMBER]

        val whereMissingMsg =
            "${Attribute.IEEE_CHANGE_WHERE.key} is missing from ${changeType.changeName} block declaration"
        val tableNumberMissingMsg =
            "${Attribute.IEEE_TABLE_NUMBER.key} is missing from ${changeType.changeName} block declaration"
        val figureNumberMissingMsg =
            "${Attribute.IEEE_FIGURE_NUMBER.key} is missing from ${changeType.changeName} block declaration"
        when (changeType) {
            INSERT, DELETE, CHANGE, REPLACE_TEXT -> requireNotNull(where) {
                whereMissingMsg
            }

            INSERT_TABLE -> {
                requireNotNull(where) {
                    whereMissingMsg
                }
                requireNotNull(tableNumber) {
                    tableNumberMissingMsg
                }
            }

            REPLACE_TABLE -> requireNotNull(tableNumber) {
                tableNumberMissingMsg
            }

            INSERT_TABLE_ROWS -> {
                requireNotNull(tableNumber) {
                    tableNumberMissingMsg
                }
            }

            DELETE_TABLE_ROWS -> requireNotNull(tableNumber) {
                tableNumberMissingMsg
            }

            REPLACE_TABLE_ROWS -> {
                requireNotNull(tableNumber) {
                    tableNumberMissingMsg
                }

                requireNotNull(rowNumber) {
                    "${Attribute.IEEE_ROW_NUMBER.key} is missing from ${changeType.changeName} block declaration"
                }
            }

            INSERT_FIGURE -> {
                requireNotNull(where) {
                    whereMissingMsg
                }
                requireNotNull(figureNumber) {
                    figureNumberMissingMsg
                }
            }

            REPLACE_FIGURE -> requireNotNull(figureNumber) {
                figureNumberMissingMsg
            }

            CUSTOM -> Unit
        }
    }

    fun toAdmonition(node: AsciidocNode): Admonition = when (node) {
        is AsciidocNode.Admonition -> Admonition(when (node.wrapped.blocks.isEmpty()) {
            true -> listOf(toDecoratedText(node.wrapped.lines))
            false -> node.wrapped.blocks.map { it.toSealed() }.map { toParagraph(it) }
        }, toBookmarkFromAttributes(node.wrapped.attributes))

        else -> throw Exception("Could not process ${node.javaClass} as admonition")
    }

    fun toDecoratedText(lines: List<String>, styleModifier: StyleModifier = KeepStyle) =
        DecoratedText(blockTokenizerFor(lines).get().let { tt ->
            tt.copy(chunks = tt.chunks.map { newStyleForChunk(it, styleModifier) })
        })

    fun toDecoratedText(lines: String, styleModifier: StyleModifier = KeepStyle) =
        DecoratedText(blockTokenizerFor(lines).get().let { tt ->
            tt.copy(chunks = tt.chunks.map { newStyleForChunk(it, styleModifier) })
        })

    fun blockTokenizerFor(text: String) = BlockTokenizer(text, numberedReferences)

    fun blockTokenizerFor(text: List<String>) = BlockTokenizer(text, numberedReferences)

    private object Strikethrough : StyleModifier {
        override fun invoke(style: BlockTokenizer.ActiveStyles): BlockTokenizer.ActiveStyles {
            return style.copy(strikethrough = true)
        }
    }

    private object Underline : StyleModifier {
        override fun invoke(style: BlockTokenizer.ActiveStyles): BlockTokenizer.ActiveStyles {
            return style.copy(underlined = true)
        }
    }
}