package org.somda.asciidoc.ieee

import org.somda.asciidoc.BlockTokenizer
import org.somda.asciidoc.Diff
import org.somda.asciidoc.DiffChunk
import org.somda.asciidoc.TokenizedText
import org.somda.asciidoc.ieee.model.DecoratedText

object AsciidocChangeReader {
    fun toDecoratedDiffText(nodes: List<AsciidocNode>): DecoratedText {
        val sectionIndices = nodes.withIndex().filter { (_, node) ->
            node is AsciidocNode.Discrete
        }.map { (index, _) -> index }

        validateSectionIndices(sectionIndices, nodes)

        val original = nodes[sectionIndices[0] + 1]
        val revised = nodes[sectionIndices[1] + 1]

        val concatenate: (AsciidocNode) -> String = { node ->
            require(node is AsciidocNode.Paragraph) {
                "Change blocks must contain exactly two discrete sections, each followed by exactly one paragraph node."
            }
            node.wrapped.lines.joinToString(" ")
        }

        val originalConcatenated = concatenate(original)
        val revisedConcatenated = concatenate(revised)
        val diff = Diff.from(originalConcatenated, revisedConcatenated)

        val appendWhitespace: (String, Boolean) -> String = { text, isLast ->
            if (!isLast) {
                "$text "
            } else {
                text
            }
        }

        val chunks = diff.chunks.withIndex().map { (index, chunk) ->
            val isLast = index == diff.chunks.lastIndex
            when (chunk) {
                is DiffChunk.Plain ->
                    chunksOf(appendWhitespace(chunk.text, isLast))

                is DiffChunk.Change ->
                    applyStrikethrough(chunksOf(appendWhitespace(chunk.deletedText, false))) +
                            applyUnderline(chunksOf(appendWhitespace(chunk.insertedText, isLast)))


                is DiffChunk.Insert ->
                    applyUnderline(chunksOf(appendWhitespace(chunk.text, isLast)))

                is DiffChunk.Delete ->
                    applyStrikethrough(chunksOf(appendWhitespace(chunk.text, false)))
            }
        }.flatten()

        return DecoratedText(TokenizedText(chunks))
    }

    private fun chunksOf(text: String) = BlockTokenizer(text).get().chunks

    private fun applyStrikethrough(chunks: List<BlockTokenizer.Chunk>): List<BlockTokenizer.Chunk> {
        return chunks.filterIsInstance<BlockTokenizer.Chunk.Styled>().map {
            BlockTokenizer.Chunk.Styled(it.text, it.style.copy(strikethrough = true))
        }
    }

    private fun applyUnderline(chunks: List<BlockTokenizer.Chunk>): List<BlockTokenizer.Chunk> {
        return chunks.filterIsInstance<BlockTokenizer.Chunk.Styled>().map {
            BlockTokenizer.Chunk.Styled(it.text, it.style.copy(underlined = true))
        }
    }

    fun toNodesFromNewSection(nodes: List<AsciidocNode>): List<AsciidocNode> {
        val sectionIndices = nodes.withIndex().filter { (_, node) ->
            node is AsciidocNode.Discrete
        }.map { (index, _) -> index }

        validateSectionIndices(sectionIndices, nodes)

        require(sectionIndices[1] < nodes.lastIndex) {
            "A New section of a change requires to contain at least one paragraph."
        }

        return nodes.subList(sectionIndices[1] + 1, nodes.size)
    }

    fun toNodesFromOldSection(nodes: List<AsciidocNode>): List<AsciidocNode> {
        val sectionIndices = nodes.withIndex().filter { (_, node) ->
            node is AsciidocNode.Discrete
        }.map { (index, _) -> index }

        validateSectionIndices(sectionIndices, nodes)

        require(sectionIndices[0] < sectionIndices[1]) {
            "A Old section of a change requires to contain at least one paragraph."
        }

        return nodes.subList(sectionIndices[0] + 1, sectionIndices[1])
    }

    private fun validateSectionIndices(sectionIndices: List<Int>, nodes: List<AsciidocNode>) {
        require(sectionIndices.size == 2) {
            "Changes and replaced requirements must contain exactly two discrete sections named '$OLD' and '$NEW'."
        }
        require((nodes[sectionIndices[0]] as AsciidocNode.Discrete).wrapped.title == OLD) {
            "First discrete section must be named $OLD."
        }
        require((nodes[sectionIndices[1]] as AsciidocNode.Discrete).wrapped.title == NEW) {
            "Second discrete section must be named $NEW."
        }
    }

    private const val OLD = "Old"
    private const val NEW = "New"
}