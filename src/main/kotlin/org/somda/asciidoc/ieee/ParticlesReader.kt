package org.somda.asciidoc.ieee

import org.apache.logging.log4j.kotlin.Logging
import org.asciidoctor.ast.Block
import org.asciidoctor.ast.Document
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.Treeprocessor
import org.somda.asciidoc.BlockTokenizer
import org.somda.asciidoc.ieee.model.*
import java.io.File
import java.text.SimpleDateFormat

/**
 * Asciidoc plugin to collect all required document particles for the Word conversion.
 *
 * @param documentParticles the document particles struct to be populated.
 */
class ParticlesReader(
    private val documentParticles: DocumentParticles,
    baseDir: File,
    requirementsCollector: RequirementsCollector,
    numberedReferencesCollector: NumberedReferencesCollector
) : Treeprocessor() {
    private val mapper = AsciidocToModelMapping(
        requirementsCollector.detectedRequirements(),
        numberedReferencesCollector.numberedReferences(),
        baseDir
    )

    override fun process(document: Document): Document {
        logger.info { "Start Word code generator" }

        processKeywords(document)
        processMetadata(document)

        document.blocks.forEach {
            processBlock(it)
        }
        return document
    }

    // collect all ieee keywords
    private fun processKeywords(document: Document) {
        documentParticles.keywords = document.attributes["ieee_keywords"]?.let { keywordsString ->
            check(keywordsString is String)
            logger.info { "Found keywords: $keywordsString" }
            keywordsString.split(',').map { it.trim() }.toList()
        } ?: listOf()
    }

    // collect all ieee document metadata
    private fun processMetadata(document: Document) {
        document.toSealed().attributes.also { attr ->
            require(attr.has(Attribute.IEEE_METADATA_DRAFT_NUMBER)) {
                "Document requires a draft number: :${Attribute.IEEE_METADATA_DRAFT_NUMBER.key}:"
            }
            require(attr.has(Attribute.IEEE_METADATA_DESIGNATION)) {
                "Document requires a designation: :${Attribute.IEEE_METADATA_DESIGNATION.key}:"
            }
            require(attr.has(Attribute.IEEE_METADATA_PAR_TITLE)) {
                "Document requires a PAR title: :${Attribute.IEEE_METADATA_PAR_TITLE.key}:"
            }

            val standardTypes = StandardType.values().map { it.typeName }
            check(attr[Attribute.IEEE_METADATA_GDE_REC_PRAC_STD]?.let { standardTypes.contains(it) }
                ?: true) {
                "Standard type needs to be one of { ${standardTypes.joinToString(", ")} }"
            }

            val trialUseValue = "Trial-Use"
            check(attr[Attribute.IEEE_METADATA_TRIAL_USE]?.let { it == trialUseValue || it.isEmpty() } ?: true) {
                "Trial-use needs to be empty or '$trialUseValue'"
            }

            check(attr[Attribute.IEEE_METADATA_COMMITTEE_NAME]?.isNotBlank() ?: false) {
                "Document requires a committee name: :${Attribute.IEEE_METADATA_COMMITTEE_NAME.key}:"
            }

            check(attr[Attribute.IEEE_METADATA_COMMITTEE_NAME]?.isNotBlank() ?: false) {
                "Document requires a committee name: :${Attribute.IEEE_METADATA_COMMITTEE_NAME.key}:"
            }

            check(attr[Attribute.IEEE_METADATA_SOCIETY_NAME]?.isNotBlank() ?: false) {
                "Document requires a society name: :${Attribute.IEEE_METADATA_SOCIETY_NAME.key}:"
            }

            check(attr[Attribute.IEEE_METADATA_DRAFT_DATE]?.isNotBlank() ?: false) {
                "Document requires a draft date: :${Attribute.IEEE_METADATA_DRAFT_DATE.key}:"
            }

            val dateFormat = "yyyy-MM"
            val localDate = try {
                SimpleDateFormat(dateFormat).parse(attr[Attribute.IEEE_METADATA_DRAFT_DATE])
            } catch (e: Exception) {
                throw Exception("Draft date needs to be in the format of $dateFormat but was '${attr[Attribute.IEEE_METADATA_DRAFT_DATE]}")
            }

            check(attr[Attribute.IEEE_METADATA_WORKING_GROUP_NAME]?.isNotBlank() ?: false) {
                "Document requires a working group name: :${Attribute.IEEE_METADATA_WORKING_GROUP_NAME.key}:"
            }

            val nomenNescio = "n.n."
            documentParticles.metadata = DocumentMetadata(
                attr[Attribute.IEEE_METADATA_DESIGNATION]!!,
                attr[Attribute.IEEE_METADATA_DRAFT_NUMBER]!!,
                attr[Attribute.IEEE_METADATA_TRIAL_USE]?.let { it == trialUseValue } ?: false,
                attr[Attribute.IEEE_METADATA_GDE_REC_PRAC_STD]?.let { StandardType.from(it) } ?: StandardType.STANDARD,
                attr[Attribute.IEEE_METADATA_PAR_TITLE]!!,
                attr[Attribute.IEEE_METADATA_COMMITTEE_NAME]!!,
                attr[Attribute.IEEE_METADATA_SOCIETY_NAME]!!,
                localDate,
                attr[Attribute.IEEE_METADATA_WORKING_GROUP_NAME]!!,
                attr[Attribute.IEEE_METADATA_CHAIR_NAME] ?: nomenNescio,
                attr[Attribute.IEEE_METADATA_SUBGROUP_CHAIR_NAME] ?: nomenNescio,
                attr[Attribute.IEEE_AMENDMENT_CORRIGENDA] ?: "",
                attr[Attribute.IEEE_AMENDMENT_CORRIGENDA_NUMBER] ?: "",
                attr[Attribute.IEEE_AMENDMENT_CORRIGENDA_YEAR] ?: ""
            )
        }
    }

    private fun processBlock(structuralNode: StructuralNode) {
        structuralNode.toSealed().let { node ->
            when (node) {
                is AsciidocNode.Document -> {
                    logger.info { "Found document, process children" }
                    structuralNode.blocks.forEach {
                        processBlock(it)
                    }
                }

                is AsciidocNode.Section -> dispatchAndProcessSection(node)

                is AsciidocNode.Preamble -> processPreamble(node)

                is AsciidocNode.Preface -> processPreface(node)

                else -> logger.info { "Found ${node.javaClass.simpleName}: ignore on root level" }
            }
        }
    }

    private fun dispatchAndProcessSection(node: AsciidocNode.Section) {
        if (node.containsDefinitions()) {
            documentParticles.definitions = mapper.toDefinitions(node)
            return
        }

        if (node.containsAbbreviations()) {
            node.children.filterIsInstance<AsciidocNode.Description>().firstOrNull()
                ?.also {
                    documentParticles.abbreviations = mapper.toAbbreviations(it)
                }
            return
        }

        if (node.containsNormativeReferences()) {
            node.children.filterIsInstance<AsciidocNode.UnorderedList>().firstOrNull()?.also {
                documentParticles.normativeReferences = mapper.toReferences(it) { bookmark, text ->
                    NormativeReference(bookmark, text)
                }
            }
            return
        }

        if (node.containsInformativeReferences()) {
            node.children.filterIsInstance<AsciidocNode.UnorderedList>().firstOrNull()?.also {
                documentParticles.informativeReferences = mapper.toReferences(it) { bookmark, text ->
                    InformativeReference(bookmark, text)
                }
            }
            return
        }

        if (node.isOverview()) {
            documentParticles.overview = mapper.toRegularSection(node).regularBlocks
            return
        }

        if (node.isAppendix()) {
            documentParticles.annexes = documentParticles.annexes.toMutableList().apply {
                if (!node.skipped()) {
                    add(mapper.toAnnexSectionRoot(node, documentParticles.metadata.amendmentCorrigenda.isNotEmpty()))
                }
            }
            return
        }

        if (node.skipped()) {
            logger.info { "Skip section: ${node.wrapped.title}" }
            node.children.filterIsInstance<AsciidocNode.Section>().forEach { dispatchAndProcessSection(it) }
            return
        }

        logger.info { "Process section: ${node.wrapped.title}" }

        // recursively process children of this child block
        processSection(node)
    }

    private fun processSection(node: AsciidocNode.Section) {
        documentParticles.sections = documentParticles.sections.toMutableList().apply {
            if (!node.skipped()) {
                add(mapper.toRegularSection(node))
            }
        }
    }

    private fun processPreamble(node: AsciidocNode.Preamble) {
        logger.info { "Found Preamble; convert to Abstract" }
        node.wrapped.blocks.map {
            check(it is Block)
            BlockTokenizer(it.lines, mapOf()).get()
        }.also {
            documentParticles.abstract = it
        }
    }

    private fun processPreface(node: AsciidocNode.Preface) {
        logger.info { "Found Preface; convert to Introduction" }
        node.wrapped.blocks.map {
            mapper.toParagraph(it.toSealed())
        }.also {
            documentParticles.introduction = it
        }
    }

    private companion object : Logging
}