package org.somda.asciidoc.ieee

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.somda.asciidoc.ieee.config.Config
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.WordConfig
import org.somda.asciidoc.ieee.word.WordTemplate
import org.somda.asciidoc.ieee.word.hook.*
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import java.io.File
import java.io.IOException

/**
 * Constants and java class agnostic functions that are shared between different operation modes (server or single file
 * conversion).
 */
object Globals {
    private const val RESOURCE_PATH_STANDARDS_TEMPLATE = "/IEEESTD-WORDTEMPLATE_v1_2022.docx"
    private const val RESOURCE_PATH_AMENDMENT_CORRIGENDA_TEMPLATE =
        "/IEEESTD-WORDTEMPLATE_Amendments-Corri-v7-2024.docx"

    /**
     * Creates a standards template stream (baseline/revision document).
     *
     * - The caller is responsible to close the stream
     * - Generates a new stream every time the function is called
     */
    fun createStandardsTemplateStream() = javaClass.getResourceAsStream(RESOURCE_PATH_STANDARDS_TEMPLATE)
        ?: throw IOException("Standards template missing from resources: $RESOURCE_PATH_STANDARDS_TEMPLATE")

    /**
     * Creates a amendment/corrigenda template stream.
     *
     * - The caller is responsible to close the stream
     * - Generates a new stream every time the function is called
     */
    fun createCorrigendaAmendmentTemplateStream() =
        javaClass.getResourceAsStream(RESOURCE_PATH_AMENDMENT_CORRIGENDA_TEMPLATE)
            ?: throw IOException("Standards template missing from resources: $RESOURCE_PATH_AMENDMENT_CORRIGENDA_TEMPLATE")
}

/**
 * Creates bookmark hooks for the Word file mapping.
 *
 * @param documentParticles the document contents to map into the Word file.
 * @param modelToWordMapper the mapping instance passed to the hooks.
 * @return list of hooks required by [WordTemplate].
 */
fun bookmarkHooks(documentParticles: DocumentParticles, modelToWordMapper: ModelToWordMapping) = listOf(
    KeywordHook(documentParticles),
    AbstractHook(documentParticles, modelToWordMapper),
    IntroductionHook(documentParticles, modelToWordMapper),
    DefinitionHook(documentParticles, modelToWordMapper),
    AbbreviationHook(documentParticles, modelToWordMapper),
    NormativeReferenceHook(documentParticles, modelToWordMapper),
    InformativeReferenceHook(documentParticles, modelToWordMapper),
    MainContentHook(documentParticles, modelToWordMapper),
    OverviewHook(documentParticles, modelToWordMapper),
    AnnexHook(documentParticles, modelToWordMapper),
    AmendmentCorrigendaHook1(documentParticles),
    AmendmentCorrigendaHook2(documentParticles),
)

/**
 * Creates document hooks for the Word file mapping.
 *
 * @param documentParticles the document contents to map into the Word file.
 * @return list of hooks required by [WordTemplate].
 */
fun documentHooks(documentParticles: DocumentParticles) = listOf(MetadataHook(documentParticles))

/**
 * Creates global hooks for the Word file mapping.
 *
 * @return list of hooks required by [WordTemplate].
 */
fun globalHooks() = listOf(TocHook())

/**
 * Creates a Word [File] from the asciidoc input file.
 *
 * @param adocInputFile the input file.
 */
fun wordFileFrom(adocInputFile: File) = File(
    adocInputFile.absoluteFile.parentFile.absolutePath
            + File.separator
            + adocInputFile.nameWithoutExtension
            + ".docx"
)

/**
 * Creates a config [File] from the asciidoc input file.
 *
 * @param adocInputFile the input file.
 */
fun configFileFrom(adocInputFile: File) = File(
    adocInputFile.absoluteFile.parentFile.absolutePath
            + File.separator
            + adocInputFile.nameWithoutExtension
            + ".json"
)

/**
 * Creates an asciidoc output file from the asciidoc input file.
 *
 * @param adocInputFile the input file.
 * @param backend the backend (pdf/html).
 */
fun asciidocOutputFileFrom(adocInputFile: File, backend: String) = File(
    adocInputFile.absoluteFile.parentFile.absolutePath
            + File.separator
            + adocInputFile.nameWithoutExtension
            + ".$backend"
)

/**
 * Creates a [Config] object from a config JSON [File].
 *
 * @param configFile the file of the config JSON or null if no config is given.
 * @return a populated [Config] if `configFile` exists and could be unmarshalled, otherwise a default instance.
 */
fun createConfig(configFile: File?) = if (configFile == null || !configFile.exists()) {
    Config()
} else {
    Json.decodeFromString(configFile.readText(Charsets.UTF_8))
}

/**
 * Creates a [WordTemplate] based on the general standards document DOCX.
 *
 * @param isAmendmentCorrigenda true to create a template for an amendment/corrigenda, false for baseline/revision
 */
fun createWordTemplate(isAmendmentCorrigenda: Boolean) = if (isAmendmentCorrigenda) {
    Globals.createCorrigendaAmendmentTemplateStream().use {
        WordTemplate(it, createWordConfig(true))
    }
} else {
    Globals.createStandardsTemplateStream().use {
        WordTemplate(it, createWordConfig(false))
    }
}

/**
 * Creates a [org.somda.asciidoc.ieee.word.WordConfig] based on the general standards document DOCX.
 *
 * @param isAmendmentCorrigenda true to create a template for an amendment/corrigenda, false for baseline/revision
 */
fun createWordConfig(isAmendmentCorrigenda: Boolean) = if (isAmendmentCorrigenda) {
    WordConfig(deactivateNumbering = true)
} else {
    WordConfig()
}
