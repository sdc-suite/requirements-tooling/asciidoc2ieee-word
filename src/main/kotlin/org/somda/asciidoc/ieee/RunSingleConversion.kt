package org.somda.asciidoc.ieee

import org.apache.logging.log4j.kotlin.Logging
import org.somda.asciidoc.AsciidocErrorChecker
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import java.io.File
import java.io.FileOutputStream
import kotlin.system.exitProcess

/**
 * Class to perform single conversions.
 *
 */
class RunSingleConversion {

    /**
     * Performs single conversion of the given file.
     *
     * @param adocInputFile the asciidoc input to convert.
     *                      JSON config and output files are derived from the input file name.
     */
    fun run(adocInputFile: File) {
        val backend = "pdf"
        try {
            logger.info { "Start conversion of '${adocInputFile.canonicalPath}'" }

            val config = configFileFrom(adocInputFile).let { configFile ->
                logger.info("Validate config file")
                createConfig(configFile)
            }

            val adocOutputFile = asciidocOutputFileFrom(adocInputFile, backend)
            val wordOutputFile = wordFileFrom(adocInputFile)

            logger.info { "Write ${backend.uppercase()} output to '${adocOutputFile.canonicalPath}'" }
            logger.info { "Write Word output to '${wordOutputFile.canonicalPath}'" }

            val documentParticles = DocumentParticles()

            AsciidocErrorChecker().use {
                AsciidocConverter(
                    AsciidocConverter.Input.FileInput(adocInputFile),
                    adocInputFile.absoluteFile.parentFile,
                    adocOutputFile,
                    backend,
                    documentParticles,
                    config.requirements?.numbers
                ).run()
            }

            val amendmentCorrigenda = documentParticles.metadata.isAmendmentCorrigenda()
            if (amendmentCorrigenda) {
                logger.info {
                    "Create amendment/corrigenda document (${Attribute.IEEE_AMENDMENT_CORRIGENDA.key} non-empty)"
                }
            } else {
                logger.info {
                    "Create baseline/revision document (${Attribute.IEEE_AMENDMENT_CORRIGENDA.key} empty)"
                }
            }

            val wordTemplate = createWordTemplate(amendmentCorrigenda)
            val modelToWordMapper = ModelToWordMapping(wordTemplate.wordPackage)
            wordTemplate.transform(
                FileOutputStream(wordOutputFile),
                bookmarkHooks(documentParticles, modelToWordMapper),
                documentHooks(documentParticles),
                globalHooks()
            )

            logger.info { "File(s) successfully written" }
        } catch (e: NullPointerException) {
            logger.error(e) { "A null pointer exception was thrown. Please look for details in the stack trace." }
            exitProcess(1)
        } catch (e: Exception) {
            logCauses(e)
            logger.error(e) { e.message }
            exitProcess(1)
        }
    }

    private companion object : Logging {
        fun logCauses(throwable: Throwable?) {
            throwable?.also {
                logCauses(it.cause)
                logger.error { it.message }
            }
        }
    }
}