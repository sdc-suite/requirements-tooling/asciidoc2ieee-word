package org.somda.asciidoc.ieee

import org.apache.logging.log4j.kotlin.Logging
import org.asciidoctor.Options
import org.asciidoctor.ast.ContentModel
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.BlockProcessor
import org.asciidoctor.extension.Contexts
import org.asciidoctor.extension.Name
import org.asciidoctor.extension.Reader
import org.somda.asciidoc.*

/**
 * Plugin that searches for [BLOCK_NAME_IEEE_CHANGE] blocks.
 *
 * Checks for requirement number duplicates
 *
 * @param expectedRequirementNumbers an optional list of numbers to verify if all expected requirements exist in the document.
 */
@Name(BLOCK_NAME_IEEE_CHANGE)
@Contexts(Contexts.OPEN)
@ContentModel(ContentModel.COMPOUND)
class AmendmentCorrigendaRequirementsCollector(private val expectedRequirementNumbers: Set<Long>? = null) :
    BlockProcessor(BLOCK_NAME_IEEE_CHANGE) {
    companion object : Logging {
        private val REQUIREMENT_NUMBER_FORMAT = "^r(\\d+)$".toRegex()

        fun requirementNumberOfId(id: String): Long {
            val matchResults = REQUIREMENT_NUMBER_FORMAT.findAll(id)
            return matchResults.map { it.groupValues[1] }.toList().first().toLong()
        }
    }

    val detectedIds = mutableSetOf<Long>()

    override fun process(
        parent: StructuralNode, reader: Reader,
        attributes: MutableMap<String, Any>
    ): Any {
        val passThrough: () -> Any = {
            createBlock(
                parent, plainContext(Contexts.OPEN), mapOf(
                    Options.ATTRIBUTES to attributes, // copy attributes for further processing
                    ContentModel.KEY to ContentModel.COMPOUND // signify construction of a compound object
                )
            )

            // parseContent(it, requirement.asciiDocLines) required?
        }

        val id = Attributes(attributes).id() ?: return passThrough()
        val number = runCatching {
            requirementNumberOfId(id)
        }.onFailure {
            return passThrough()
        }.getOrThrow()

        validateRequirement(number)

        detectedIds.add(number)

        return passThrough()
    }

    private fun validateRequirement(number: Long) {
        check(!detectedIds.contains(number)) {
            "IEEE requirement #'$number' already exists"
        }

        check(expectedRequirementNumbers?.contains(number) ?: true) {
            "Requirement number $number could not be found in requirement number set"
        }
    }
}