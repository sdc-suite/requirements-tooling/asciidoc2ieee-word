package org.somda.asciidoc.ieee.model

import org.somda.asciidoc.TokenizedText
import kotlin.collections.List

class DocumentParticles {
    var metadata: DocumentMetadata = DocumentMetadata()
    var keywords: List<String> = listOf()
    var abstract: List<TokenizedText> = listOf()
    var introduction: List<Paragraph> = listOf()
    var definitions: List<Definition> = listOf()
    var abbreviations: List<Abbreviation> = listOf()
    var normativeReferences: List<NormativeReference> = listOf()
    var informativeReferences: List<InformativeReference> = listOf()
    var sections: List<RegularSection> = listOf()
    var overview: List<RegularSectionChild> = listOf()
    var annexes: List<AnnexSectionRoot> = listOf()
}