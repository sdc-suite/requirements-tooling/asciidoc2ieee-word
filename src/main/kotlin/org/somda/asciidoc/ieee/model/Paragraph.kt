package org.somda.asciidoc.ieee.model

import org.somda.asciidoc.TokenizedText
import java.io.File
import kotlin.collections.List

sealed interface Paragraph : RegularSectionChild, AnnexSectionChild

data class ComputerCode(val content: List<DecoratedText>): Paragraph

data class DecoratedText(val content: TokenizedText) : Paragraph

sealed interface Title {
    enum class Type {
        FIGURE,
        TABLE
    }
}

data class RegularTitle(
    val text: DecoratedText,
    val bookmark: Bookmark,
    val type: Title.Type
): Title

data class AnnexTitle(
    val text: DecoratedText,
    val bookmark: Bookmark,
    val type: Title.Type,
    val annexLetter: String
): Title

data class GenericTitle(
    val reference: String,
    val text: DecoratedText,
    val type: Title.Type
): Title

data class Image(
    val title: Title,
    val file: File,
    val alternativeText: String
) : Paragraph

data class Table(
    val title: Title,
    val rows: List<Row>,
    val pageOrientation: PageOrientation,
    val rowCount: Int,
    val colCount: Int,
    val widthPercentage: Int,
    val colWidthsParts: List<Int>
) : Paragraph {

    sealed class Row(val cells: List<Cell>)
    data class HeaderRow(val headerCells: List<Cell>): Row(headerCells)
    data class DataRow(val dataCells: List<Cell>): Row(dataCells)

    data class Cell(
        val content: List<Block>,
        val rowSpan: Int,
        val columnSpan: Int,
        val horizontalAlign: HorizontalAlign,
        val verticalAlign: VerticalAlign
    ) {
        enum class HorizontalAlign {
            LEFT,
            CENTER,
            RIGHT
        }

        enum class VerticalAlign {
            TOP,
            MIDDLE,
            BOTTOM
        }
    }

    enum class PageOrientation() {
        PORTRAIT,
        LANDSCAPE;
    }
}