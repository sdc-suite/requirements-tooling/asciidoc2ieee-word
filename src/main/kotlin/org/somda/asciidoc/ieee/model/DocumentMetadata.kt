package org.somda.asciidoc.ieee.model

import org.joda.time.DateTime
import java.util.*

enum class StandardType(val typeName: String) {
    STANDARD("Standard"),
    RECOMMENDED_PRACTICE("Recommended Practice"),
    GUIDE("Guide");

    companion object {
        fun from(typeName: String) = entries
            .firstOrNull { it.typeName == typeName }
            ?: throw Exception("Expected one of ${entries.map { it.typeName }}, but found: $typeName")
    }
}

data class DocumentMetadata(
    val designation: String = "",
    val draftNumber: String = "",
    val trialUse: Boolean = false,
    val standardType: StandardType = StandardType.STANDARD,
    val parTitle: String = "",
    val committeeName: String = "",
    val societyName: String = "",
    val draftDate: Date = DateTime.now().toDate(),
    val workingGroupName: String = "",
    val chairName: String = "",
    val subgroupChairName: String = "",
    val amendmentCorrigenda: String = "",
    val amendmentCorrigendaNumber: String = "",
    val amendmentCorrigendaYear: String = "",
) {
    fun isAmendmentCorrigenda() = this.amendmentCorrigenda.isNotEmpty()

    fun documentType() = when (this.amendmentCorrigenda) {
        DocumentType.AMENDMENT.typeName -> DocumentType.AMENDMENT
        DocumentType.CORRIGENDA.typeName -> DocumentType.CORRIGENDA
        else -> DocumentType.REVISION
    }
}

enum class DocumentType(val typeName: String, val shortName: String) {
    REVISION("Revision", "Rev"),
    AMENDMENT("Amendment", "Amd"),
    CORRIGENDA("Corrigenda", "Cor")
}