package org.somda.asciidoc.ieee.model

import org.somda.asciidoc.*
import org.somda.asciidoc.ieee.Attributes
import kotlin.collections.List

/**
 * Takes a string and converts it to a [RequirementLevel] enum.
 *
 * @param raw Raw text being shall, should or may.
 *
 * @return the [RequirementLevel] enum or null if the conversion failed (raw was not shall, should or may).
 */
fun resolveRequirementLevel(raw: String) = RequirementLevel.values().firstOrNull { it.keyword == raw }

data class Requirement(
    val metadata: RequirementMetadata,
    val bookmark: Bookmark,
    val text: List<Paragraph>,
    val notes: List<Admonition>
) : RegularSectionChild, AnnexSectionChild


/**
 * Definition of requirement levels.
 *
 * @property keyword The keyword as it is supposed to appear as a value to the attribute key.
 */
enum class RequirementLevel(val keyword: String) {
    MAY("may"),
    SHOULD("should"),
    SHALL("shall")
}

enum class RequirementRole(val roleName: String) {
    PARTICIPANT("participant"),
    PROVIDER("provider"),
    CONSUMER("consumer")
}

/**
 * Structure that represents a requirement.
 *
 * @property number The requirement number as an integer (no leading R or zero-padding).
 * @property level The requirement level as specified by the attribute [BlockAttribute.REQUIREMENT_KEYWORD].
 * @property role Requirement applicability.
 * @property icsClusters Set of ICS identifiers for ICS table generation.
 * @property asciiDocAttributes All attributes captured by the block that represents this requirement.
 * @property asciiDocLines The actual ASCIIdoc source.
 */
data class RequirementMetadata(
    val number: Int,
    val level: RequirementLevel,
    val role: RequirementRole,
    val icsClusters: Set<String>,
    val asciiDocAttributes: Attributes,
    val asciiDocLines: List<String>
) {
    val effectiveIcsClusters = icsClusters + setOf(role.roleName)
    val blockId =
        asciiDocAttributes.id() ?: throw IllegalArgumentException("Requirements require a block id, but none found")
    val blockTitle = asciiDocAttributes.title()
        ?: throw IllegalArgumentException("Requirements require a block title, but none found for #$blockId ")
}