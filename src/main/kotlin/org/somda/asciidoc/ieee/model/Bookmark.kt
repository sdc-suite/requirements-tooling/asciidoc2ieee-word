package org.somda.asciidoc.ieee.model

data class Bookmark(val name: String, val label: String? = null, val numbered: Boolean = false) {
    fun label() = label ?: name
}