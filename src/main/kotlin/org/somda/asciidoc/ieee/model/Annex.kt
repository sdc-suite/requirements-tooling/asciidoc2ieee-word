package org.somda.asciidoc.ieee.model

import kotlin.collections.List

enum class Normativity {
    NORMATIVE,
    INFORMATIVE
}
data class AnnexSectionRoot(
    val title: String,
    val bookmark: Bookmark,
    val annexBlocks: List<AnnexSectionChild>,
    val normativity: Normativity,
    val letterOverride: Char?
) : Section(annexBlocks)

data class AnnexSection(
    val title: String,
    val bookmark: Bookmark,
    val annexBlocks: List<AnnexSectionChild>
) : Section(annexBlocks), Block, AnnexSectionChild

sealed interface AnnexSectionChild: SectionChild