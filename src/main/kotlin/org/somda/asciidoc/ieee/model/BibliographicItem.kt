package org.somda.asciidoc.ieee.model

sealed class BibliographicItem(val bibItemBookmark: Bookmark, val bibItemText: DecoratedText)

data class NormativeReference(val bookmark: Bookmark, val text: DecoratedText) : Block,
    BibliographicItem(bookmark, text)

data class InformativeReference(val bookmark: Bookmark, val text: DecoratedText) : Block,
    BibliographicItem(bookmark, text)