package org.somda.asciidoc.ieee.model

import kotlin.collections.List

sealed class Section(val sectionBlocks: List<SectionChild>): Block
sealed interface SectionChild: Block

sealed interface RegularSectionChild: SectionChild

data class RegularSection(
    val title: String,
    val bookmark: Bookmark,
    val regularBlocks: List<RegularSectionChild>
) : Section(regularBlocks), Block, RegularSectionChild