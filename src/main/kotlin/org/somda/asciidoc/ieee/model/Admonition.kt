package org.somda.asciidoc.ieee.model

import kotlin.collections.List

data class Admonition(
    val text: List<Paragraph>,
    val bookmark: Bookmark? = null
): Block