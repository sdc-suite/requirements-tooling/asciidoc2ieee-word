package org.somda.asciidoc.ieee.model

import org.somda.asciidoc.TokenizedText

sealed interface List

data class UnorderedList(val list: kotlin.collections.List<TokenizedText>) : Block,
    List, Paragraph, SectionChild

data class OrderedList(
    val list: kotlin.collections.List<OrderedListItem>,
    val level: Int
) : Block, List, Paragraph, SectionChild

data class OrderedListItem(val index: Int, val text: TokenizedText, val subList: OrderedList?)