package org.somda.asciidoc.ieee.model

import kotlin.collections.List

data class Definition(
    val term: String,
    val bookmark: Bookmark,
    val text: List<Paragraph>,
    val notes: List<Admonition> = listOf()
)

data class Abbreviation(
    val term: String,
    val text: DecoratedText,
    val bookmark: Bookmark? = null
)

