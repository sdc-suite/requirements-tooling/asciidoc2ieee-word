package org.somda.asciidoc.ieee.model

import kotlin.collections.List

enum class ChangeType(val changeName: String) {
    INSERT("insert"),
    DELETE("delete"),
    CHANGE("change"),
    REPLACE_TEXT("replace_text"),
    INSERT_TABLE("insert_table"),
    REPLACE_TABLE("replace_table"),
    INSERT_TABLE_ROWS("insert_table_rows"),
    DELETE_TABLE_ROWS("delete_table_rows"),
    REPLACE_TABLE_ROWS("replace_table_rows"),
    INSERT_FIGURE("insert_figure"),
    REPLACE_FIGURE("replace_figure"),
    CUSTOM("custom");

    companion object {
        fun fromName(changeName: String) = entries.firstOrNull { it.changeName == changeName }
            ?: throw Exception("'$changeName' is not a valid change type name; expected one of ${entries.joinToString(",") { it.changeName }}")
    }
}

data class Change(
    val headLine: Paragraph,
    val content: List<RegularSectionChild>
) : AnnexSectionChild, RegularSectionChild

