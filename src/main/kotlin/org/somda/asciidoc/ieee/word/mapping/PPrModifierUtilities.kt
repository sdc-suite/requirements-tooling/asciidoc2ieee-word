package org.somda.asciidoc.ieee.word.mapping

import org.docx4j.wml.JcEnumeration
import org.docx4j.wml.PPrBase
import org.docx4j.wml.STLineSpacingRule
import org.somda.asciidoc.ieee.word.WordGlobals

/**
 * Modifier transformer.
 *
 * @param modifier the modifier to transform.
 * @return `modifier` if `modifier` is [PPrModifier.Append], otherwise [PPrModifier.Void]
 */
fun appendOrVoid(modifier: PPrModifier) = when (modifier) {
    is PPrModifier.Append -> modifier
    else -> PPrModifier.Void
}

/**
 * Modifier baseline for an ordered list style.
 *
 * @param level the list level.
 * @return a modifier that reflects the numbered list style.
 */
fun numberedListStyle(level: Int) = PPrModifier.Assign(
    createPPr(
        WordGlobals.STYLE_TEMPLATE_NUMBERED_LIST_LEVEL.format(level + 1),
        JcEnumeration.LEFT,
        true
    ).also {
        it.spacing = PPrBase.Spacing()
        it.spacing.lineRule = STLineSpacingRule.AUTO
        it.spacing.line = 240.toBigInteger()
    }
)

/**
 * Modifier baseline for an unordered list style.
 *
 * @return a modifier that reflects the dashed list style.
 */
fun dashListStyle() = PPrModifier.Assign(
    createPPr(
        WordGlobals.STYLE_UNORDERED_LIST,
        JcEnumeration.LEFT
    ).also {
        it.spacing = PPrBase.Spacing()
        it.spacing.lineRule = STLineSpacingRule.AUTO
        it.spacing.line = 240.toBigInteger()
    }
)

/**
 * Appends spaces to a paragraph.
 *
 * @param beforeInPt space before the paragraph - null to not touch the before space.
 * @param afterInPt space after the paragraph - null to not touch the after space.
 * @return a modifier that adds space to a paragraph.
 */
fun spacingAppender(beforeInPt: Int?, afterInPt: Int?) = PPrModifier.Append { pPr ->
    pPr.spacing = (pPr.spacing ?: PPrBase.Spacing()).apply {
        beforeInPt?.let { WordGlobals.space(it) }?.also { before = it }
        afterInPt?.let { WordGlobals.space(it) }?.also { after = it }
    }
}