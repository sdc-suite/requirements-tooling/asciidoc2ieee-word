package org.somda.asciidoc.ieee.word

/**
 * Constants and functions that support Word document generation.
 */
object WordGlobals {
    const val STYLE_ABSTRACT_HEADER = "IEEEStdsAbstractHeader"
    const val STYLE_ABSTRACT_BODY = "IEEEStdsAbstractBody"
    const val STYLE_TEMPLATE_HEADING_LEVEL_PREFIX = "IEEEStdsLevel"
    const val STYLE_TEMPLATE_HEADING_LEVEL = "$STYLE_TEMPLATE_HEADING_LEVEL_PREFIX%sHeader"
    const val STYLE_TEMPLATE_ANNEX_HEADING_LEVEL_PREFIX = "Heading"
    const val STYLE_TEMPLATE_ANNEX_HEADING_LEVEL = "$STYLE_TEMPLATE_ANNEX_HEADING_LEVEL_PREFIX%s"
    const val STYLE_FOOTNOTE = "IEEEStdsFootnote"
    const val STYLE_DEFINITIONS = "IEEEStdsDefinitions"
    const val STYLE_DEFINITION_TERM_NUMBERS = "IEEEStdsDefTermsNumbers"
    const val STYLE_TEMPLATE_NUMBERED_LIST_LEVEL = "IEEEStdsNumberedListLevel%s"
    const val STYLE_UNORDERED_LIST = "IEEEStdsUnorderedList"
    const val STYLE_PARAGRAPH = "IEEEStdsParagraph"
    const val STYLE_COMPUTER_CODE = "IEEEStdsComputerCode"
    const val LABEL_NOTE_PREFIX = "NOTE"
    const val LABEL_NOTE_SUFFIX = "—"
    const val LABEL_SINGLE_NOTE = LABEL_NOTE_PREFIX + LABEL_NOTE_SUFFIX
    const val STYLE_SINGLE_NOTE = "IEEEStdsSingleNote"
    const val STYLE_MULTI_NODE = "IEEEStdsMultipleNotes"
    const val STYLE_REGULAR_FIGURE_CAPTION = "IEEEStdsRegularFigureCaption"
    const val STYLE_REGULAR_TABLE_CAPTION = "IEEEStdsRegularTableCaption"
    const val STYLE_CAPTION = "Caption"
    const val STYLE_IMAGE = "IEEEStdsImage"
    const val STYLE_TABLE_HEADER = "IEEEStdsTableColumnHead"
    const val STYLE_TABLE_LEFT = "IEEEStdsTableData-Left"
    const val STYLE_TABLE_CENTER = "IEEEStdsTableData-Center"
    const val STYLE_REQUIREMENT = "Requirement"
    const val STYLE_REQUIREMENT_UNORDERED_LIST = "RequirementUnorderedList"
    const val STYLE_TEMPLATE_REQUIREMENT_NUMBERED_LIST_LEVEL = "RequirementOrderedListLevel%s"
    const val STYLE_TABLE_UNORDERED_LIST = "TableUnorderedList"
    const val STYLE_TEMPLATE_TABLE_NUMBERED_LIST_LEVEL = "TableOrderedListLevel%s"

    const val PARAGRAPH_SPACE_NONE = 0
    const val PARAGRAPH_SPACE_SHORT = 6
    const val PARAGRAPH_SPACE_LARGE = 24

    private const val SPACE_PT_TO_TWIPS_FACTOR = 20
    private const val FONT_PT_FACTOR = 2

    /**
     * Creates space based on a number in Pt.
     *
     * @param spaceInPt the space to generate.
     * @return space in twips.
     */
    fun space(spaceInPt: Int) = spaceInPt.times(SPACE_PT_TO_TWIPS_FACTOR).toBigInteger()

    /**
     * Creates a font size based on a number in Pt.
     *
     * @param sizeInPt the font size to generate.
     * @return space in twips.
     */
    fun fontSize(sizeInPt: Double) = sizeInPt.times(FONT_PT_FACTOR).toLong().toBigInteger()
}