package org.somda.asciidoc.ieee.word.mapping

import org.docx4j.jaxb.Context
import org.docx4j.model.table.TblFactory
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.*
import org.somda.asciidoc.BlockTokenizer
import org.somda.asciidoc.TokenizedText
import org.somda.asciidoc.ieee.model.*
import org.somda.asciidoc.ieee.word.WordGlobals
import java.math.BigInteger
import kotlin.math.roundToLong

/**
 * Maps from internal data model to Word paragraphs.
 */
class ModelToWordMapping(private val wordPackage: WordprocessingMLPackage) {
    private val mainDocumentPart = wordPackage.mainDocumentPart.also {
        it.propertyResolver.activateStyle("FootnoteReference")
    }

    // todo add more styles
    private fun toRPr(style: BlockTokenizer.ActiveStyles, baseRPr: RPr? = null) =
        baseRPr?.deepCopy()?.apply {
            if (style.italic) {
                i = createTrue()
            }
            if (style.bold) {
                b = createTrue()
            }
            if (style.underlined) {
                u = U().apply { `val` = UnderlineEnumeration.SINGLE }
            }
            if (style.strikethrough) {
                strike = createTrue()
            }
            if (style.superscript) {
                vertAlign = CTVerticalAlignRun().apply {
                    `val` = STVerticalAlignRun.SUPERSCRIPT
                }
            }
            if (style.subscript) {
                vertAlign = CTVerticalAlignRun().apply {
                    `val` = STVerticalAlignRun.SUBSCRIPT
                }
            }
        } ?: RPr().apply {
            i = createBoolean(style.italic)
            b = createBoolean(style.bold)
        }

    fun toRuns(decoratedText: DecoratedText, baseRPr: RPr? = null) = toRuns(decoratedText.content, baseRPr)

    fun toRuns(decoratedText: TokenizedText, baseRPr: RPr? = null): List<Any> = mutableListOf<Any>().apply {
        decoratedText.chunks.forEach { chunk ->
            when (chunk) {
                is BlockTokenizer.Chunk.Styled -> add(createWordRun(chunk.text, toRPr(chunk.style, baseRPr)))

                is BlockTokenizer.Chunk.Reference -> if (chunk.customLabel) {
                    add(
                        createInternalHyperlink(
                            Bookmark(chunk.name, chunk.label, chunk.numbering),
                            toRPr(chunk.style, baseRPr)
                        )
                    )
                } else {
                    addAll(
                        createBookmarkReferenceRun(
                            Bookmark(chunk.name, chunk.label, chunk.numbering),
                            toRPr(chunk.style, baseRPr)
                        )
                    )
                }

                // there is no feasible way to render anchors, ignore for the moment
                is BlockTokenizer.Chunk.Anchor -> Unit

                is BlockTokenizer.Chunk.Link -> add(
                    createExternalHyperlink(mainDocumentPart, chunk.url, chunk.label)
                )

                is BlockTokenizer.Chunk.Footnote -> add(
                    createFootnoteAndReturnReference(
                        mainDocumentPart.footnotesPart,
                        toRuns(BlockTokenizer(chunk.text).get())
                    )
                )

                is BlockTokenizer.Chunk.NonBreakingSpace -> add(createWordRun(" ", toRPr(chunk.style, baseRPr)))
            }
        }
    }.toList()

    fun toParagraphFromDecoratedText(
        decoratedText: DecoratedText,
        basePPr: PPrModifier = PPrModifier.Void,
        baseRPr: RPr? = null
    ) = P().apply {
        if (basePPr is PPrModifier.Void) {
            PPrModifier.Default
        } else {
            basePPr
        }.applyOn(this)
        basePPr.applyOn(this)
        content.addAll(toRuns(decoratedText.content, baseRPr ?: RPr()))
    }

    private fun toParagraph(
        paragraph: Paragraph,
        decoratedTextPPr: PPrModifier = PPrModifier.Default,
        orderedListPPr: PPrModifier = PPrModifier.Void,
        unorderedListPPr: PPrModifier = PPrModifier.Void,
        baseRPr: RPr? = null
    ): List<ContentAccessor> = when (paragraph) {
        is DecoratedText -> listOf(
            toParagraphFromDecoratedText(
                paragraph,
                decoratedTextPPr,
                baseRPr ?: RPr()
            )
        )

        is ComputerCode -> paragraph.content.map { text ->
            toParagraphFromDecoratedText(
                text,
                PPrModifier.Append { it.pStyle.`val` = WordGlobals.STYLE_COMPUTER_CODE },
                baseRPr ?: RPr()
            )
        }

        is OrderedList -> toOrderedList(paragraph, orderedListPPr, baseRPr)
        is UnorderedList -> toUnorderedList(paragraph, unorderedListPPr, baseRPr)
        is Image -> toImage(paragraph)
        is Table -> toTable(paragraph)
    }

    private fun toRequirement(requirement: Requirement): List<ContentAccessor> =
        mutableListOf<ContentAccessor>().apply {
            val reqStyle = PPrModifier.Assign(createPPr(WordGlobals.STYLE_REQUIREMENT, JcEnumeration.LEFT))
            val reqUnorderedListStyle =
                PPrModifier.Append {
                    it.pStyle = PPrBase.PStyle().apply { `val` = WordGlobals.STYLE_REQUIREMENT_UNORDERED_LIST }
                }

            requirement.text.map {
                when (it) {
                    is UnorderedList -> toParagraph(
                        paragraph = it,
                        decoratedTextPPr = reqStyle,
                        unorderedListPPr = reqUnorderedListStyle
                    )

                    is OrderedList -> toParagraph(
                        paragraph = it,
                        decoratedTextPPr = reqStyle,
                        orderedListPPr = PPrModifier.Append { pPr ->
                            pPr.pStyle = PPrBase.PStyle().apply {
                                `val` =
                                    WordGlobals.STYLE_TEMPLATE_REQUIREMENT_NUMBERED_LIST_LEVEL.format(it.level + 1)
                            }
                        },
                        unorderedListPPr = reqUnorderedListStyle
                    )

                    else -> toParagraph(it, reqStyle)
                }
            }.fold(mutableListOf<ContentAccessor>()) { acc, paragraphs ->
                acc.apply { addAll(paragraphs) }
            }.also {
                it[0] = P().apply {
                    reqStyle.applyOn(this)
                    appendBookmark(requirement.bookmark, createWordRun(requirement.metadata.blockTitle))
                    append(createWordRun(": "))
                    content.addAll(it.first().content)
                }

                if (requirement.notes.isEmpty()) {
                    // append more space if there are no notes
                    spacingAppender(
                        beforeInPt = null,
                        afterInPt = WordGlobals.PARAGRAPH_SPACE_LARGE
                    ).applyOn(it.last() as P)
                }
                addAll(it)
            }

            addAll(toNotes(requirement.notes))
        }

    private fun toChange(change: Change): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        addAll(toParagraph(change.headLine))
        addAll(change.content.map { toRegularSectionChild(it) }.flatten())
    }

    fun toUnorderedList(
        enumeration: UnorderedList,
        baseStyle: PPrModifier = PPrModifier.Void,
        baseRPr: RPr? = null
    ): List<P> {
        return enumeration.list.map {
            toRuns(it, baseRPr ?: RPr())
        }.map {
            P().apply {
                dashListStyle().applyOn(this)
                appendOrVoid(baseStyle).applyOn(this)
                content.addAll(it)
            }
        }
    }

    fun toOrderedList(
        list: OrderedList,
        baseStyle: PPrModifier = PPrModifier.Void,
        baseRPr: RPr? = null
    ): List<P> {
        // grab list style from modifiers by simulating instantiation of a list item paragraph
        val simulatedP = P().apply {
            numberedListStyle(list.level).applyOn(this)
            appendOrVoid(baseStyle).applyOn(this)
        }

        // find abstract definition number to find the last numbering instance
        val abstractDefEntries = wordPackage.mainDocumentPart.numberingDefinitionsPart.abstractListDefinitions.entries
        val abstractNumId = abstractDefEntries.firstOrNull { listDef ->
            listDef.value.listLevels.any { listLevel ->
                listLevel.value.jaxbAbstractLvl.pStyle?.`val`?.equals(
                    simulatedP.pPr.pStyle.`val`
                ) ?: false
            }
        }?.value?.abstractNumNode?.abstractNumId

        // find last numbering instance based on abstractDefEntries
        val newNum = wordPackage.mainDocumentPart.numberingDefinitionsPart.instanceListDefinitions.values
            .reversed()
            .firstOrNull { listDef ->
                listDef.abstractListDefinition.abstractNumNode.abstractNumId == abstractNumId
            }?.let {
                // create a restart item and return the number of that restart item
                wordPackage.mainDocumentPart.numberingDefinitionsPart.restart(
                    it.numNode.numId.longValueExact(),
                    list.level.toLong(),
                    1L
                )
            }

        checkNotNull(newNum) {
            "Could not restart a list since no corresponding abstract numbering was found for ${simulatedP.pPr.pStyle.`val`}"
        }

        return list.list.mapIndexed { listIndex, listItem ->
            val textRuns = toRuns(listItem.text, baseRPr ?: RPr())
            val text = P().apply {
                numberedListStyle(list.level).applyOn(this)
                appendOrVoid(baseStyle).applyOn(this)

                // on the first list item attach new numbering item
                if (listIndex == 0) {
                    pPr.numPr = PPrBase.NumPr().apply {
                        numId = PPrBase.NumPr.NumId().apply { `val` = newNum.toBigInteger() }
                        ilvl = PPrBase.NumPr.Ilvl().apply { `val` = list.level.toBigInteger() }
                    }
                }

                content.addAll(textRuns)
            }

            mutableListOf<P>().apply {
                add(text)
                addAll(listItem.subList?.let { toOrderedList(it, baseStyle, baseRPr) } ?: listOf())
            }
        }.fold(mutableListOf()) { acc, paragraphs ->
            acc.apply { addAll(paragraphs) }
        }
    }

    fun toBibItem(bibItem: BibliographicItem) = P().apply {
        pPr = createPPr(WordGlobals.STYLE_PARAGRAPH, JcEnumeration.LEFT)

        append(createWordRun("["))
        appendBookmark(bibItem.bibItemBookmark)
        append(createWordRun("]"))
        append(createWordRun(" "))
        content.addAll(toRuns(bibItem.bibItemText.content))
    }

    fun toDefinition(definition: Definition): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        val initialP = P()
        add(initialP)
        initialP.pPr = createPPr(WordGlobals.STYLE_DEFINITIONS)

        val rPrStyle = RPr().apply {
            rStyle = RStyle()
            rStyle.`val` = WordGlobals.STYLE_DEFINITION_TERM_NUMBERS
        }

        initialP.appendBookmark(definition.bookmark, createWordRun(definition.term, rPrStyle))
        initialP.append(createWordRun(":", rPrStyle))
        initialP.append(createWordRun(" "))

        definition.text.first().also { paragraph ->
            check(paragraph is DecoratedText) { "First paragraph of a definition needs to be text only" }
            toRuns(paragraph.content).forEach {
                initialP.append(it)
            }
        }

        definition.text.drop(1).map { paragraph ->
            toParagraph(paragraph)
        }.fold(mutableListOf<ContentAccessor>()) { acc, paragraphs ->
            acc.apply { addAll(paragraphs) }
        }.also {
            addAll(it)
        }

        addAll(toNotes(definition.notes, WordGlobals.PARAGRAPH_SPACE_SHORT))
    }

    fun toAbbreviation(abbreviation: Abbreviation): P = P().apply {
        pPr = createPPr(WordGlobals.STYLE_DEFINITIONS)

        val rPrStyle = RPr().apply {
            rStyle = RStyle()
            rStyle.`val` = WordGlobals.STYLE_DEFINITION_TERM_NUMBERS
        }

        abbreviation.bookmark?.let {
            appendBookmark(it, createWordRun(abbreviation.term, rPrStyle))
        } ?: append(createWordRun(abbreviation.term, rPrStyle))
        append(createWordRun(":", rPrStyle))
        append(createWordRun(" "))

        toRuns(abbreviation.text.content).forEach {
            append(it)
        }
    }

    private fun toNotes(
        admonitions: List<Admonition>,
        finalSpaceInPt: Int = WordGlobals.PARAGRAPH_SPACE_LARGE
    ): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        val factory: ObjectFactory = Context.getWmlObjectFactory()
        val shortSpacing = spacingAppender(
            beforeInPt = WordGlobals.PARAGRAPH_SPACE_NONE,
            afterInPt = WordGlobals.PARAGRAPH_SPACE_SHORT
        )

        val largeSpacing = spacingAppender(
            beforeInPt = WordGlobals.PARAGRAPH_SPACE_NONE,
            afterInPt = finalSpaceInPt
        )

        for ((index, admonition) in admonitions.withIndex()) {
            val initialP = P()
            add(initialP)
            initialP.pPr = createPPr(
                when (admonitions.size) {
                    1 -> WordGlobals.STYLE_SINGLE_NOTE
                    else -> WordGlobals.STYLE_SINGLE_NOTE
                },
                JcEnumeration.LEFT
            )

            shortSpacing.applyOn(initialP)

            when (admonitions.size) {
                1 -> initialP.append(createWordRun(WordGlobals.LABEL_SINGLE_NOTE))
                else -> initialP.apply {
                    append(createWordRun(WordGlobals.LABEL_NOTE_PREFIX + " "))
                    admonition.bookmark?.let {
                        appendBookmark(it, createWordRun("${index + 1}"))
                    } ?: append(createWordRun("${index + 1}"))
                    content.add(createWordRun(WordGlobals.LABEL_NOTE_SUFFIX))
                }
            }

            admonition.text.firstOrNull()?.also { paragraph ->
                check(paragraph is DecoratedText) { "First paragraph of a note needs to be text only" }
                toRuns(paragraph.content).forEach {
                    initialP.append(it)
                }
            } ?: throw Exception("Admonition needs at least one paragraph")

            val rPr = factory.createRPr().apply {
                sz = factory.createHpsMeasure().apply {
                    `val` = BigInteger.valueOf(18) // for some reason, 18 is 9pt...
                }
            }

            admonition.text.drop(1).map { paragraph ->
                toParagraph(
                    paragraph,
                    shortSpacing.append { pPr ->
                        createPPr(WordGlobals.STYLE_SINGLE_NOTE, JcEnumeration.LEFT).also { pPrMask ->
                            pPr.jc = pPrMask.jc
                            pPr.pStyle = pPrMask.pStyle
                        }
                    },
                    shortSpacing,
                    shortSpacing,
                    rPr
                )
            }.fold(mutableListOf<ContentAccessor>()) { acc, paragraphs ->
                acc.apply { addAll(paragraphs) }
            }.also {
                addAll(it)
            }
        }

        (lastOrNull() as? P)?.also {
            largeSpacing.applyOn(it)
        }
    }

    fun toSections(section: List<RegularSection>): List<ContentAccessor> = section.map {
        toRegularSection(it)
    }.fold(mutableListOf()) { acc, paragraphs ->
        acc.apply { addAll(paragraphs) }
    }

    fun toAnnexSections(section: List<AnnexSectionRoot>): List<ContentAccessor> = section.map {
        toAnnexSectionRoot(it)
    }.fold(mutableListOf()) { acc, paragraphs ->
        acc.apply { addAll(paragraphs) }
    }

    private fun toAnnexSectionRoot(
        section: AnnexSectionRoot,
        depth: Int = 0
    ): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        val annexInfo = when (section.normativity) {
            Normativity.INFORMATIVE -> "(informative)"
            Normativity.NORMATIVE -> "(normative)"
        }

        add(P().apply {
            pPr = createPPr(WordGlobals.STYLE_TEMPLATE_ANNEX_HEADING_LEVEL.format(depth + 1))
            when (val letter = section.letterOverride) {
                null -> Unit
                else -> append(createWordRun("Annex $letter"))
            }
            append(createLinebreak())
            append(createWordRun(annexInfo, RPr().apply { b = createFalse() }))
            append(createLinebreak())
            appendBookmark(section.bookmark) {
                it.append(createWordRun(section.title))
            }
        })

        addAll(section.annexBlocks.map {
            toAnnexSectionChild(it, depth)
        }.fold(mutableListOf<P>()) { acc, paragraphs ->
            acc.apply { addAll(paragraphs) }
        })
    }

    private fun toAnnexSection(
        section: AnnexSection,
        depth: Int = 0,
    ): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        add(P().apply {
            pPr = createPPr(WordGlobals.STYLE_TEMPLATE_ANNEX_HEADING_LEVEL.format(depth + 1))
            appendBookmark(section.bookmark) {
                it.append(createWordRun(section.title))
            }
        })

        addAll(section.annexBlocks.map {
            toAnnexSectionChild(it, depth)
        }.fold(mutableListOf()) { acc, paragraphs ->
            acc.apply { addAll(paragraphs) }
        })
    }

    private fun toAnnexSectionChild(sectionChild: AnnexSectionChild, depth: Int = 0): List<ContentAccessor> =
        mutableListOf<ContentAccessor>().apply {
            addAll(
                when (sectionChild) {
                    is DecoratedText -> toParagraph(sectionChild)
                    is OrderedList -> toParagraph(sectionChild)
                    is UnorderedList -> toParagraph(sectionChild)
                    is ComputerCode -> toParagraph(sectionChild)
                    is Requirement -> toRequirement(sectionChild)
                    is Change -> toChange(sectionChild)
                    is Image -> toImage(sectionChild)
                    is Table -> toTable(sectionChild)
                    is AnnexSection -> toAnnexSection(sectionChild, depth + 1)
                }
            )
        }

    // WordConstants.STYLE_REGULAR_FIGURE_CAPTION
    // WordConstants.STYLE_TEMPLATE_HEADING_LEVEL
    private fun toRegularSection(
        section: RegularSection,
        depth: Int = 0,
    ): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        add(P().apply {
            pPr = createPPr(WordGlobals.STYLE_TEMPLATE_HEADING_LEVEL.format(depth + 1))
            appendBookmark(section.bookmark) {
                it.append(createWordRun(section.title))
            }
        })

        addAll(section.regularBlocks.map {
            toRegularSectionChild(it, depth)
        }.fold(mutableListOf()) { acc, paragraphs ->
            acc.apply { addAll(paragraphs) }
        })
    }

    private fun toRegularSectionChild(
        sectionChild: RegularSectionChild,
        depth: Int = 0,
    ): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        addAll(
            when (sectionChild) {
                is DecoratedText -> toParagraph(sectionChild)
                is ComputerCode -> toParagraph(sectionChild)
                is OrderedList -> toParagraph(sectionChild)
                is UnorderedList -> toParagraph(sectionChild)
                is Requirement -> toRequirement(sectionChild)
                is Change -> toChange(sectionChild)
                is Image -> toImage(sectionChild)
                is Table -> toTable(sectionChild)
                is RegularSection -> toRegularSection(sectionChild, depth + 1)
            }
        )
    }

    fun toSectionChildren(
        sectionChildren: List<RegularSectionChild>,
        depth: Int = 0
    ): List<ContentAccessor> = sectionChildren.map {
        toRegularSectionChild(it, depth)
    }.fold(mutableListOf()) { acc, paragraphs ->
        acc.apply { addAll(paragraphs) }
    }

    private fun toImage(image: Image): List<P> =
        mutableListOf<P>().apply {
            listOf("png", "jpg", "jpeg").also { fileTypes ->
                require(fileTypes.contains(image.file.extension.lowercase())) {
                    "Image type ${image.file.extension} not supported. Supported file types: ${fileTypes.joinToString(",")}"
                }
            }

            add(createImageParagraph(wordPackage, image.file.readBytes(), image.file.name, image.alternativeText))
            add(toTitle(image.title))
        }

    private fun toTitle(title: Title): P = when (title) {
        is RegularTitle -> {
            P().apply {
                val style = when (title.type) {
                    Title.Type.FIGURE -> WordGlobals.STYLE_REGULAR_FIGURE_CAPTION
                    Title.Type.TABLE -> WordGlobals.STYLE_REGULAR_TABLE_CAPTION
                }
                pPr = createPPr(style, JcEnumeration.CENTER)
                appendBookmark(title.bookmark, createWordRun("—"))
                content.addAll(toRuns(title.text))
            }
        }

        is AnnexTitle -> {
            val label = when (title.type) {
                Title.Type.FIGURE -> "Figure"
                Title.Type.TABLE -> "Table"
            }
            createAnnexCaption(label, title.bookmark, title.annexLetter, title.text)
        }

        is GenericTitle -> P().apply {
            val label = when (title.type) {
                Title.Type.FIGURE -> "Figure"
                Title.Type.TABLE -> "Table"
            }
            pPr = createPPr(WordGlobals.STYLE_CAPTION)
            content.add(createWordRun("$label ${title.reference}—"))
            content.addAll(toRuns(title.text))
        }
    }

    private fun createTableMask(rowCount: Int, columnCount: Int) = TableMask().apply {
        repeat(rowCount) {
            rows.add(TableMask.Row().also { row ->
                repeat(columnCount) {
                    row.cells.add(TableMask.Cell())
                }
            })
        }
    }

    private fun toTable(
        table: Table
    ): List<ContentAccessor> = mutableListOf<ContentAccessor>().apply {
        if (table.pageOrientation == Table.PageOrientation.LANDSCAPE) {
            add(createPortraitP())
        }

        val tableMask = createTableMask(table.rowCount, table.colCount)

        table.rows.forEachIndexed { i, row ->
            row.cells.forEachIndexed { j, cell ->
                val absPos = tableMask.absolutePosition(TableMask.Position(i, j))
                tableMask.spanFrom(absPos, cell.rowSpan, cell.columnSpan)
                tableMask.rows[absPos.row].cells[absPos.column].content = cell
            }
        }

        add(toTitle(table.title))

        add(createTable(table, tableMask).also {
            addBorders(it)
        })

        if (table.pageOrientation == Table.PageOrientation.LANDSCAPE) {
            add(createLandscapeP())
            //
        } else {
            add(P().apply { pPr = createPPr(WordGlobals.STYLE_PARAGRAPH) })
        }
    }

    private fun createTable(table: Table, tableMask: TableMask): Tbl =
        TblFactory.createTable(table.rowCount, table.colCount, 1).apply {

            val maxWidth = writableTwipsWidth(table.pageOrientation)

            val partSum = table.colWidthsParts.sum()
            val widths = table.colWidthsParts.map {
                val part = (it.toDouble() / partSum.toDouble())
                (part * maxWidth).roundToLong().toBigInteger()
            }

            for (i in 0 until table.colCount) {
                tblGrid.gridCol[i].apply {
                    w = widths[i]
                }
            }

            tableMask.rows.forEachIndexed { rowIndex, row ->
                val tr = content[rowIndex] as Tr
                val isHeaderRow = table.rows[rowIndex] is Table.HeaderRow
                val pPr = if (isHeaderRow) {
                    tr.trPr = TrPr().apply {
                        cnfStyleOrDivIdOrGridBefore.add(ObjectFactory().createCTTrPrBaseTblHeader(BooleanDefaultTrue()))
                    }
                    createPPr(WordGlobals.STYLE_TABLE_HEADER)
                } else {
                    createPPr(WordGlobals.STYLE_TABLE_LEFT)
                }

                row.cells.indices.reversed().forEach { colIndex ->
                    val cell = row.cells[colIndex]

                    val tc = tr.content[colIndex] as Tc

                    if (cell.content == null) {
                        if (!cell.verticalMerge || cell.colSpan == null) {
                            tr.content.remove(tc)
                            return@forEach
                        }
                    }

                    tc.also {
                        tc.tcPr.tcW.w = widths[colIndex]
                        cell.content?.also { cellContent ->
                            if (cellContent.rowSpan > 0) {
                                tc.tcPr.vMerge = TcPrInner.VMerge().apply {
                                    `val` = "restart"
                                }
                            }
                        }
                        cell.colSpan?.also { colSpan ->
                            tc.tcPr.gridSpan = TcPrInner.GridSpan().apply {
                                `val` = colSpan.toBigInteger()
                            }
                        }
                        if (cell.verticalMerge) {
                            tc.tcPr.vMerge = TcPrInner.VMerge()
                        }
                    }


                    cell.content?.also { cellWithContent ->
                        tc.content.clear()
                        cellWithContent.content.forEach { block ->
                            when (block) {
                                is DecoratedText ->
                                    tc.content.add(
                                        toParagraphFromDecoratedText(
                                            block,
                                            PPrModifier.Assign(pPr)
                                        )
                                    )

                                is Paragraph ->
                                    tc.content.addAll(
                                        toParagraph(
                                            block,
                                            PPrModifier.Assign(pPr),
                                            PPrModifier.Append {
                                                if (block is OrderedList) {
                                                    it.pStyle.`val` =
                                                        WordGlobals.STYLE_TEMPLATE_TABLE_NUMBERED_LIST_LEVEL.format(
                                                            block.level + 1
                                                        )
                                                }
                                            },
                                            PPrModifier.Append {
                                                it.pStyle.`val` = WordGlobals.STYLE_TABLE_UNORDERED_LIST
                                            }
                                        )
                                    )

                                else -> Unit
                            }
                        }
                    }
                }
            }
        }

    @Suppress("DEPRECATION")
    private fun writableTwipsWidth(orientation: Table.PageOrientation) = when (orientation) {
        Table.PageOrientation.LANDSCAPE -> wordPackage.documentModel.sections[0].pageDimensions.writableHeightTwips
        Table.PageOrientation.PORTRAIT -> wordPackage.documentModel.sections[0].pageDimensions.writableWidthTwips
    }

    // todo add support for custom borders
    private fun addBorders(table: Tbl) {
        // if no border is given, set 1 as default
        var width = 1L
        try {
            width = 1L
        } catch (e: NumberFormatException) {
            // ignore as default is set
        }

        // do not add a border if width is 0
//        if (width == 0L) {
//            return
//        }

        val factory = Context.getWmlObjectFactory()
        table.tblPr = factory.createTblPr()
        val border = factory.createCTBorder()
        border.color = "auto"
        border.sz = BigInteger.valueOf(width * 4L) // 4 appears to be 1/2pt which is Word's default line width
        border.space = BigInteger("0")
        border.setVal(STBorder.SINGLE)
        val borders = factory.createTblBorders()
        borders.bottom = border
        borders.left = border
        borders.right = border
        borders.top = border
        borders.insideH = border
        borders.insideV = border
        table.tblPr.tblBorders = borders

        // Layouting such that table caption sticks to the actual table
//        table.tblPr.tblpPr = factory.createCTTblPPr()

        // The numbers that Word generated - seem to be pointless
        // table.tblPr.tblpPr.leftFromText = BigInteger.valueOf(141L)
        // table.tblPr.tblpPr.rightFromText = BigInteger.valueOf(141L)
//        table.tblPr.tblpPr.vertAnchor = STVAnchor.TEXT
//        table.tblPr.tblpPr.tblpY = BigInteger.ONE

//        table.tblPr.tblOverlap = factory.createCTTblOverlap()
//        table.tblPr.tblOverlap.`val` = STTblOverlap.NEVER

        table.tblPr.tblW = factory.createTblWidth()
        table.tblPr.tblW.type = "auto"
        table.tblPr.tblW.w = BigInteger.ZERO
    }

    private fun createAnnexCaption(
        label: String,
        bookmark: Bookmark,
        annexLetter: String,
        captionText: DecoratedText
    ): P = P().apply {
        pPr = createPPr(WordGlobals.STYLE_CAPTION)

        appendBookmark(bookmark) {
            it.content.add(createWordRun("$label "))

            it.content.add(createFieldCharBeginRun())
            it.content.add(createInstructionText(" STYLEREF 1 \\s "))
            it.content.add(createFieldCharSeparateRun())
            it.content.add(createWordRun(annexLetter))
            it.content.add(createFieldCharEndRun())

            it.content.add(createWordRun("."))

            it.content.add(createFieldCharBeginRun())
            it.content.add(createInstructionText(" SEQ Figure \\* ARABIC \\s 1 "))
            it.content.add(createFieldCharSeparateRun())
            it.content.add(createWordRun("?"))
            it.content.add(createFieldCharEndRun())
        }

        content.add(createWordRun("—"))
        content.addAll(toRuns(captionText))
    }
}