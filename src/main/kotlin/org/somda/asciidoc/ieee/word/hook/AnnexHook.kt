package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import org.somda.asciidoc.ieee.word.mapping.insertParagraphBefore
import org.somda.asciidoc.ieee.word.mapping.replaceBy

/**
 * Adds annex clause before [BookmarkLabel.ANNEX] bookmark.
 */
class AnnexHook(
    private val document: DocumentParticles,
    private val mapper: ModelToWordMapping
) : BookmarkHook {
    override fun label() = BookmarkLabel.ANNEX

    override fun callback(bookmark: CTBookmark) {
        bookmark.parent.also { bookmarkP ->
            check(bookmarkP is P) {
                "Bookmark for AnnexHook is required to be a Paragraph but was: ${bookmark.parent.javaClass}"
            }

            if (document.annexes.isNotEmpty()) {
                bookmarkP.insertParagraphBefore {
                    it.replaceBy(
                        mapper.toAnnexSections(document.annexes)
                    )
                }
            }
        }
    }
}