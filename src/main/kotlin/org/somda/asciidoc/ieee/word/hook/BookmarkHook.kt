package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark

/**
 * Interface for bookmark hooks.
 *
 * Bookmark hooks are scanned by a Word template.
 * The Word template fires the callback when an available hook is found.
 */
interface BookmarkHook {
    /**
     * Returns the hook this bookmark is triggered by.
     *
     * This function is used by the Word template to detect the bookmark.
     */
    fun label(): BookmarkLabel

    /**
     * The callback that is fired for the corresponding bookmark.
     */
    fun callback(bookmark: CTBookmark)
}