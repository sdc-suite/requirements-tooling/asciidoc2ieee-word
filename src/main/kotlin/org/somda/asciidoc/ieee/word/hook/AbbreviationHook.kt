package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import org.somda.asciidoc.ieee.word.mapping.replaceBy


/**
 * Replaces paragraph at [BookmarkLabel.ABBREVIATION] bookmark by list of abbreviations.
 */
class AbbreviationHook(
    private val document: DocumentParticles,
    private val mapper: ModelToWordMapping
) : BookmarkHook {
    override fun label() = BookmarkLabel.ABBREVIATION

    override fun callback(bookmark: CTBookmark) {
        bookmark.parent.also { bookmarkP ->
            check(bookmarkP is P) {
                "Bookmark for AbbreviationHook is required to be a Paragraph but was: ${bookmark.parent.javaClass}"
            }
            bookmarkP.replaceBy(
                document.abbreviations.map {
                    mapper.toAbbreviation(it)
                }
            )
        }
    }
}