package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import org.somda.asciidoc.ieee.word.mapping.replaceBy

/**
 * Replaces paragraph at [BookmarkLabel.MAIN_CONTENT] bookmark by normative clauses.
 */
class MainContentHook(
    private val document: DocumentParticles,
    private val mapper: ModelToWordMapping
) : BookmarkHook {
    override fun label() = BookmarkLabel.MAIN_CONTENT

    override fun callback(bookmark: CTBookmark) {
        bookmark.parent.also { bookmarkP ->
            check(bookmarkP is P) {
                "Bookmark for MainContentHook is required to be a Paragraph but was: ${bookmark.parent.javaClass}"
            }
            bookmarkP.replaceBy(
                mapper.toSections(document.sections).toMutableList().apply {
                    //add(createPagebreak())
                }
            )
        }
    }
}