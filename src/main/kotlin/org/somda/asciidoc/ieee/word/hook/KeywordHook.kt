package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.createWordRun

/**
 * Appends paragraph at [BookmarkLabel.KEYWORD] bookmark with list of keywords.
 */
class KeywordHook(private val document: DocumentParticles) : BookmarkHook {
    override fun label() = BookmarkLabel.KEYWORD
    override fun callback(bookmark: CTBookmark) {
        if (document.keywords.isEmpty()) {
            throw Exception("List of keywords is empty")
        }

        (bookmark.parent as ContentAccessor).content.add(createWordRun(document.keywords.joinToString(", ")))
    }
}