package org.somda.asciidoc.ieee.word.mapping

import org.docx4j.XmlUtils
import org.docx4j.jaxb.Context
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage
import org.docx4j.openpackaging.parts.WordprocessingML.FootnotesPart
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.docx4j.openpackaging.parts.relationships.Namespaces
import org.docx4j.relationships.ObjectFactory
import org.docx4j.relationships.Relationship
import org.docx4j.wml.*
import org.somda.asciidoc.ieee.model.Bookmark
import org.somda.asciidoc.ieee.word.WordGlobals
import java.math.BigInteger

/**
 * Deep-copies a Word run style.
 */
fun RPr.deepCopy(): RPr = XmlUtils.deepCopy(this)

/**
 * Deep-copies a Word AbstractNum object.
 */
fun Numbering.AbstractNum.deepCopy(): Numbering.AbstractNum = XmlUtils.deepCopy(this)

/**
 * Creates a Word run.
 *
 * @param content the text content of the run.
 * @param runPr the style of the run.
 * @return Word run with preserved space.
 */
fun createWordRun(content: String = "", runPr: RPr? = null): R {
    Context.getWmlObjectFactory().apply {
        val text = createText().apply {
            value = content
            space = "preserve"
        }

        val run = createR()
        runPr?.let { run.rPr = runPr }
        run.content.add(text)
        return run
    }
}

/**
 * Creates a Word run that will be rendered as a line-break.
 */
fun createLinebreak(): R {
    Context.getWmlObjectFactory().apply {
        return createR().also {
            it.content.add(createBr())
        }
    }
}

/**
 * Creates a Word paragraph that will be rendered as a page-break.
 */
fun createPagebreakParagraph(): P {
    Context.getWmlObjectFactory().apply {
        val pPageBreak = createP()
        pPageBreak.content.add(createWordRun())
        pPageBreak.content.add(createPagebreakRun())
        return pPageBreak
    }
}

fun createPagebreakRun(): R {
    Context.getWmlObjectFactory().apply {
        val pageBreak = createBr()
        pageBreak.type = STBrType.PAGE
        val rPageBreak = createR()
        rPageBreak.content.add(pageBreak)
        return rPageBreak
    }
}

/**
 * Creates a Word paragraph style based on an existing style.
 *
 * @param styleId an existing style id to be used as a base for the style.
 * @param jc indicator for horizontal alignment. Set to null for default.
 * @param skipOutline true to omit this paragraph from the table of contents, false to include it.
 *                    Typically used to exclude numbered list items from the table of contents.
 * @return the paragraph style.
 */
fun createPPr(styleId: String, jc: JcEnumeration? = null, skipOutline: Boolean = false): PPr {
    Context.getWmlObjectFactory().apply {
        val pPr = createPPr()
        val pStyle = createPPrBasePStyle()
        pPr.pStyle = pStyle
        pStyle.setVal(styleId)

        jc?.let {
            pPr.jc = createJc()
            pPr.jc.`val` = jc
        }

        if (skipOutline) {
            pPr.apply {
                outlineLvl = createPPrBaseOutlineLvl()
                outlineLvl.`val` = BigInteger.valueOf(9)
            }
        }

        return pPr
    }
}

private fun createFootnoteReference(footnoteId: BigInteger): R {
    Context.getWmlObjectFactory().apply {
        val ctFtnEdnRef = createCTFtnEdnRef().apply { id = footnoteId }
        return createR().apply {
            rPr = createRPr().apply {
                rStyle = createRStyle().apply { `val` = "FootnoteReference" }
            }
            content.add(createRFootnoteReference(ctFtnEdnRef))
        }
    }
}

private fun nextFreeFootnoteId(footNotesPart: FootnotesPart) =
    footNotesPart.contents.footnote.maxOfOrNull { it.id.toLong() }?.let { (it + 1).toBigInteger() }
        ?: BigInteger.ONE

/**
 * Creates a footnote and returns a reference to it as a Word run.
 *
 * @param footNotesPart footnotes part of the Word document.
 * @param text the footnote text as a list of Word runs (is [Any] since the list can contain control sequences).
 * @return the Word run that refers to the footnote.
 */
fun createFootnoteAndReturnReference(footNotesPart: FootnotesPart, text: List<Any>): R {
    val nextId = nextFreeFootnoteId(footNotesPart)

    Context.getWmlObjectFactory().apply {
        val footNoteP = createP().apply {
            pPr = createPPr().apply {
                pStyle = createPPrBasePStyle().apply { `val` = WordGlobals.STYLE_FOOTNOTE }
            }
        }

        val footnoteRefNumber = createR().apply {
            rPr = createRPr().apply {
                rStyle = createRStyle().apply { `val` = "FootnoteReference" }
            }
            content.add(createRFootnoteRef())
        }

        footNoteP.content.add(footnoteRefNumber)
        footNoteP.content.addAll(text)

        footNotesPart.contents.footnote.add(createCTFtnEdn().apply {
            id = nextId
            content.add(footNoteP)
        })
    }

    return createFootnoteReference(nextId)
}

/**
 * Creates a paragraph off of a ContentAccessor the points back to the ContentAccessor instance.
 *
 * Default style applied.
 */
fun ContentAccessor.createParagraph(): P {
    val p = P()
    p.parent = this
    p.pPr = createPPr(WordGlobals.STYLE_PARAGRAPH)
    return p
}

/**
 * Helper function to create a control sequence begin run.
 */
fun createFieldCharBeginRun(): R {
    val factory = Context.getWmlObjectFactory()
    val fieldCharBeginRun = factory.createR()
    val fldCharBegin = factory.createFldChar()
    val fldCharBeginWrapped = factory.createRFldChar(fldCharBegin)
    fldCharBegin.fldCharType = STFldCharType.BEGIN
    fieldCharBeginRun.content.add(fldCharBeginWrapped)
    return fieldCharBeginRun
}

/**
 * Helper function to create a control sequence separator run.
 */
fun createFieldCharSeparateRun(): R {
    val factory = Context.getWmlObjectFactory()
    val fieldCharSeparateRun = factory.createR()
    val fldCharSeparate = factory.createFldChar()
    val fldCharSeparateWrapped = factory.createRFldChar(fldCharSeparate)
    fldCharSeparate.fldCharType = STFldCharType.SEPARATE
    fieldCharSeparateRun.content.add(fldCharSeparateWrapped)
    return fieldCharSeparateRun
}

/**
 * Helper function to create a control sequence end run.
 */
fun createFieldCharEndRun(): R {
    val factory = Context.getWmlObjectFactory()
    val fieldCharEndRun = factory.createR()
    val fldCharEnd = factory.createFldChar()
    val fldCharEndWrapped = factory.createRFldChar(fldCharEnd)
    fldCharEnd.fldCharType = STFldCharType.END
    fieldCharEndRun.content.add(fldCharEndWrapped)
    return fieldCharEndRun
}

/**
 * Creates an instruction run.
 *
 * @param instruction the instruction text - no validation will be performed!
 */
fun createInstructionText(instruction: String): R {
    val factory = Context.getWmlObjectFactory()
    val referenceRun = factory.createR()
    val instrText = factory.createText()
    instrText.space = "preserve"
    instrText.value = instruction
    referenceRun.content.add(factory.createRInstrText(instrText))
    return referenceRun
}

/**
 * Creates bookmark reference run.
 *
 * @param bookmark a bookmark to resolve bookmark identifier and label from.
 * @param style style of the bookmark label. Set to null for default.
 * @return list of Word runs that form a bookmark reference.
 */
fun createBookmarkReferenceRun(bookmark: Bookmark, style: RPr?) = mutableListOf<R>().apply {
    val numbered = when (bookmark.numbered) {
        true -> "\\r "
        false -> ""
    }
    val label = when (bookmark.label) {
        null -> bookmark.name
        else -> bookmark.label
    }

    add(createFieldCharBeginRun())
    add(createInstructionText(" REF  ${bookmark.name} $numbered\\h \\* MERGEFORMAT "))
    add(createFieldCharSeparateRun())
    add(createWordRun(label, style))
    add(createFieldCharEndRun())
}.toList()

/**
 * Creates a docx4j boolean value.
 *
 * @param value the boolean to create.
 * @return docx4j boolean that defaults to true.
 */
fun createBoolean(value: Boolean = true): BooleanDefaultTrue {
    val boolVal = BooleanDefaultTrue()
    if (!value) {
        boolVal.isVal = false
    }
    return boolVal
}

/**
 * Creates a docx4j true value.
 */
fun createTrue() = createBoolean(true)

/**
 * Creates a docx4j false value.
 */
fun createFalse() = createBoolean(false)

/**
 * Creates a hyperlink to an internal bookmark.
 *
 * @param bookmark bookmark info.
 * @param style the style.
 * @return the hyperlink.
 */
fun createInternalHyperlink(bookmark: Bookmark, style: RPr?): P.Hyperlink {
    return P.Hyperlink().apply {
        anchor = bookmark.name
        content.apply {
            add(createWordRun(bookmark.label(), style))
        }
    }
}

/**
 * Creates a hyperlink to an external resource.
 *
 * @param document main document part of a Word package required to add an external relationship.
 * @param url the URL to link to.
 * @param name the text of the link.
 * @return the hyperlink.
 */
fun createExternalHyperlink(document: MainDocumentPart, url: String, name: String): P.Hyperlink {
    // We need to add a relationship to word/_rels/document.xml.rels
    // but since its external, we don't use the
    // usual wordMLPackage.getMainDocumentPart().addTargetPart
    // mechanism
    val factory = ObjectFactory()
    val rel: Relationship = factory.createRelationship()
    rel.type = Namespaces.HYPERLINK
    rel.target = url
    rel.targetMode = "External"
    document.relationshipsPart.addRelationship(rel)

    // addRelationship sets the rel's @Id
    val hpl = """
            <w:hyperlink r:id="${rel.id}"
              xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
              xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">
                <w:r>
                    <w:rPr/>
                    <w:t>$name</w:t>
                </w:r>
            </w:hyperlink>
""".trimIndent()

    return XmlUtils.unmarshalString(hpl) as P.Hyperlink
}

/**
 * Creates an image paragraph and stores the image to the Word package.
 *
 * @param wordMLPackage the Word package where the image binary data will be stored.
 * @param bytes the image data.
 * @param filenameHint the image filename.
 * @param alternativeText an alternative text for the image.
 * @return Word paragraph that will be rendered to an image.
 */
fun createImageParagraph(
    wordMLPackage: WordprocessingMLPackage,
    bytes: ByteArray,
    filenameHint: String,
    alternativeText: String,
): P {
    val imagePart = BinaryPartAbstractImage.createImagePart(wordMLPackage, bytes)
    val inline = imagePart.createImageInline(
        filenameHint,
        alternativeText,
        IdCounter.next().toLong(),
        IdCounter.next(),
        false
    )
    return P().apply {
        pPr = createPPr(WordGlobals.STYLE_IMAGE)
        content.add(R().apply {
            content.add(Drawing().apply { anchorOrInline.add(inline) })
        })
    }
}

/**
 * Creates a paragraph to initiate a portrait view.
 */
fun createPortraitP(): P {
    val pAsXml = """
            <w:p ${Namespaces.W_NAMESPACE_DECLARATION}>
                <w:pPr>
                    <w:pStyle w:val="${WordGlobals.STYLE_PARAGRAPH}"/>
                    <w:sectPr ${Namespaces.W_NAMESPACE_DECLARATION}>
                        <w:footnotePr>
                            <w:numRestart w:val="eachSect"/>
                        </w:footnotePr>
                        <w:type w:val="continuous"/>
                        <w:pgSz w:w="12240" w:h="15840" w:code="1"/>
                        <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="720" w:footer="720" w:gutter="0"/>
                        <w:lnNumType w:countBy="1"/>
                        <w:cols w:space="720"/>
                        <w:docGrid w:linePitch="360"/>
                    </w:sectPr>
                </w:pPr>
            </w:p>
""".trimIndent()
    return XmlUtils.unmarshalString(pAsXml) as P
}

/**
 * Creates a paragraph to initiate a landscape view.
 */
fun createLandscapeP(): P {
    val pAsXml = """
            <w:p ${Namespaces.W_NAMESPACE_DECLARATION}>
                <w:pPr>
                    <w:pStyle w:val="${WordGlobals.STYLE_PARAGRAPH}"/>
                    <w:sectPr>
                        <w:footnotePr>
                            <w:numRestart w:val="eachSect"/>
                        </w:footnotePr>
                        <w:pgSz w:w="15840" w:h="12240" w:orient="landscape" w:code="1"/>
                        <w:pgMar w:top="1800" w:right="1440" w:bottom="1800" w:left="1440" w:header="720" w:footer="720" w:gutter="0"/>
                        <w:lnNumType w:countBy="1"/>
                        <w:cols w:space="720"/>
                        <w:docGrid w:linePitch="360"/>
                    </w:sectPr>
                </w:pPr>
            </w:p>
""".trimIndent()
    return XmlUtils.unmarshalString(pAsXml) as P
}
