package org.somda.asciidoc.ieee.word.hook

import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping

/**
 * Replaces paragraph at [BookmarkLabel.BIBLIOGRAPHIC_ITEM] bookmark by list of informative bibliographic items.
 */
class InformativeReferenceHook(
    document: DocumentParticles,
    mapper: ModelToWordMapping
) : BibliographyHook(document.informativeReferences, mapper) {
    override fun label() = BookmarkLabel.BIBLIOGRAPHIC_ITEM
}