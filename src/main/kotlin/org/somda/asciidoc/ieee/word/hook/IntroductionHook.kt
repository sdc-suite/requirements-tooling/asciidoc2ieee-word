package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.asciidoc.ieee.model.DecoratedText
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.model.OrderedList
import org.somda.asciidoc.ieee.model.UnorderedList
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import org.somda.asciidoc.ieee.word.mapping.replaceBy

/**
 * Replaces paragraph at [BookmarkLabel.INTRODUCTION] bookmark by introductory text.
 */
class IntroductionHook(
    private val document: DocumentParticles,
    private val mapper: ModelToWordMapping
) : BookmarkHook {
    override fun label() = BookmarkLabel.INTRODUCTION

    override fun callback(bookmark: CTBookmark) {
        bookmark.parent.also { bookmarkP ->
            check(bookmarkP is P) {
                "Bookmark for IntroductionHook is required to be a Paragraph but was: ${bookmark.parent.javaClass}"
            }
            bookmarkP.replaceBy(
                document.introduction.mapNotNull {
                    when (it) {
                        is DecoratedText -> listOf(mapper.toParagraphFromDecoratedText(it))
                        is OrderedList -> mapper.toOrderedList(it)
                        is UnorderedList -> mapper.toUnorderedList(it)
                        else -> null
                    }
                }.fold(mutableListOf()) { acc, paragraphs ->
                    acc.apply { addAll(paragraphs) }
                }
            )
        }
    }
}