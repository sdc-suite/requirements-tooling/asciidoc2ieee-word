package org.somda.asciidoc.ieee.word.mapping

import org.somda.asciidoc.ieee.model.Table

/**
 * Struct that reflects table information to map between asciidoc and Word tables.
 *
 * The table mask contains information regarding
 *
 * - cell content
 * - row count
 * - column count
 * - row spans
 * - column spans
 *
 * @property rows table rows.
 */
data class TableMask(val rows: MutableList<Row> = mutableListOf()) {
    /**
     * Models a position in a table.
     *
     * @property row the row index.
     * @property column the column index.
     */
    data class Position(val row: Int, val column: Int)

    /**
     * Models a table row that contains of multiple column cells.
     *
     * @property cells the cells of that row.
     */
    data class Row(val cells: MutableList<Cell> = mutableListOf())

    /**
     * Models a cell in the table.
     *
     * @property content cell content based on the internal model.
     * @property spanned indicates if this cell is part of a cell span column-wise.
     * @property verticalMerge indicates if this cell is part of a cell span row-wise.
     * @property colSpan if the cell is the first cell of a cell span (!= null), this value defines the number of cells to span.
     */
    data class Cell(
        var content: Table.Cell? = null,
        var spanned: Boolean = false,
        var verticalMerge: Boolean = false,
        var colSpan: Int? = null
    )

    /**
     * Spans a cell at a specific position.
     *
     * @param position the position from which the cell span begins.
     * @param rowSpan the number of rows included in that span.
     * @param columnSpan the number of columns included in that span.
     */
    fun spanFrom(position: Position, rowSpan: Int, columnSpan: Int) {
        val actualRowSpan = if (rowSpan == 0) 1 else rowSpan
        val actualColumnSpan = if (columnSpan == 0) 1 else columnSpan
        repeat(actualRowSpan) { rowIndex ->
            rows[position.row + rowIndex].cells[position.column].colSpan = columnSpan
            repeat(columnSpan) { colIndex ->
                rows[position.row + rowIndex].cells[position.column + colIndex].spanned = true
            }
        }
        rows[position.row].cells[position.column].spanned = false

        if (rowSpan > 0) {
            repeat(rowSpan - 1) { rowIndex ->
                repeat(actualColumnSpan) { colIndex ->
                    rows[position.row + rowIndex + 1].cells[position.column + colIndex].verticalMerge = true
                }
            }
        }
    }

    /**
     * Gets the absolute position of a cell in the table mask based on a relative position.
     *
     * @param relativePosition position that does not take into consideration row and col spans.
     * @return position that adds up row and col spans.
     */
    fun absolutePosition(relativePosition: Position): Position {
        var nextFree = relativePosition.column
        var occupied: Boolean
        do {
            occupied = rows[relativePosition.row].cells[nextFree].spanned
                    || rows[relativePosition.row].cells[nextFree].content != null
            if (occupied) {
                nextFree++
            }
        } while (occupied)

        val occupiedCount = occupiedCountInRow(relativePosition.copy(column = nextFree))
        return relativePosition.copy(column = relativePosition.column + occupiedCount)
    }

    private fun occupiedCountInRow(position: Position) =
        rows[position.row].cells.subList(0, position.column + 1).count { it.spanned }
}