package org.somda.asciidoc.ieee.word.mapping

import org.docx4j.jaxb.Context
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.docx4j.wml.R
import org.jvnet.jaxb2_commons.ppp.Child
import org.somda.asciidoc.ieee.model.Bookmark

/**
 * Appends a bookmark to a Word paragraph.
 *
 * @param bookmark the bookmark to add.
 * @param callback allows for the caller to render the content runs between the bookmark runs.
 */
fun P.appendBookmark(bookmark: Bookmark, renderLabel: (P) -> Unit) {
    val bookmarkId = (IdCounter.next()).toBigInteger()
    Context.getWmlObjectFactory().let { factory ->
        val bm = factory.createCTBookmark().apply {
            id = bookmarkId
            name = bookmark.name
        }
        val bmStart = factory.createBodyBookmarkStart(bm)

        val mr = factory.createCTMarkupRange().apply { id = bookmarkId }
        val bmEnd = factory.createBodyBookmarkEnd(mr)

        content.add(bmStart)
        renderLabel(this)
        content.add(bmEnd)
    }
}

/**
 * Appends a bookmark to a Word paragraph.
 *
 * @param bookmark the bookmark to add. The label of the bookmark is used as the content runs between the bookmark runs.
 */
fun P.appendBookmark(bookmark: Bookmark) = appendBookmark(bookmark, createWordRun(bookmark.label()))

fun P.appendBookmark(bookmark: Bookmark, labelRun: R) {
    appendBookmark(bookmark) {
        it.content.add(labelRun)
    }
}

/**
 * Appends content to a Word paragraph.
 *
 * Shortcut of `P.content.add()`.
 */
fun P.append(run: Any) {
    content.add(run)
}

/**
 * Inserts a paragraph after this paragraph.
 *
 * Make sure the paragraph knows its parent as otherwise this method will fail.
 *
 * @param action callback to modify the inserted paragraph.
 */
fun P.insertParagraphAfter(action: (paragraphToInsert: P) -> Unit): P = this.createParagraph().also {
    it.parent = parent
    it.pPr = pPr
    val contentAccessor = parent as ContentAccessor
    val thisIndex = contentAccessor.content.indexOf(this)
    contentAccessor.content.add(thisIndex + 1, it)
    action(it)
}

/**
 * Inserts a paragraph before this paragraph.
 *
 * Make sure the paragraph knows its parent as otherwise this method will fail.
 *
 * @param action callback to modify the inserted paragraph.
 */
fun P.insertParagraphBefore(action: (paragraphToInsert: P) -> Unit): P = this.createParagraph().also {
    it.parent = parent
    it.pPr = pPr
    val contentAccessor = parent as ContentAccessor
    val thisIndex = contentAccessor.content.indexOf(this)
    contentAccessor.content.add(thisIndex, it)
    action(it)
}

/**
 * Replaces this paragraph with a list of other paragraphs.
 *
 * Make sure the paragraph knows its parent as otherwise this method will fail.
 *
 * @param paragraphsToReplace the paragraphs to be replaced with this one.
 */
fun P.replaceBy(paragraphsToReplace: List<ContentAccessor>) {
    val theParent = parent
    checkNotNull(theParent) { "Paragraph's parent is not set" }
    check(theParent is ContentAccessor) { "Paragraph's parent is no content accessor" }

    val insertPosition = theParent.content.indexOf(this)
    theParent.content.removeAt(insertPosition)
    paragraphsToReplace.forEach {
        if (it is Child) it.parent = theParent
    }
    theParent.content.addAll(insertPosition, paragraphsToReplace)
}