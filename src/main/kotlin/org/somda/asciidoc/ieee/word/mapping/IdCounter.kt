package org.somda.asciidoc.ieee.word.mapping

/**
 * Creates a row of numbers starting at 1 000 000.
 */
object IdCounter {
    private const val idOffset = 1_000_000
    private var value = idOffset - 1

    /**
     * Calculates the next number.
     */
    fun next() = ++value
}