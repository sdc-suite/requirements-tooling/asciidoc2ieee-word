package org.somda.asciidoc.ieee.word.hook

/**
 * Enumeration of all available bookmarks of the IEEE template.
 *
 * Available bookmarks:
 *
 * - [KEYWORD]: keywords list
 * - [ABSTRACT]: abstract text
 * - [INTRODUCTION]: informative introductory text
 * - [DEFINITION]: definitions clause
 * - [ABBREVIATION]: abbreviation clause
 * - [NORMATIVE_REFERENCE]: normative references clause
 * - [BIBLIOGRAPHIC_ITEM]: bibliography annex
 * - [MAIN_CONTENT]: main content clauses
 * - [ANNEX]: annexes
 *
 * @property label the bookmark label under which content can be inserted.
 */
enum class BookmarkLabel(val label: String) {
    KEYWORD("Keyword"),
    ABSTRACT("Abstract"),
    INTRODUCTION("Introduction"),
    DEFINITION("Definition"),
    ABBREVIATION("Abbreviation"),
    NORMATIVE_REFERENCE("NormativeReference"),
    BIBLIOGRAPHIC_ITEM("BibliographicItem"),
    MAIN_CONTENT("Heading"),
    OVERVIEW("Overview"),
    ANNEX("Annex"),
    AMENDMENT_CORRIGENDA_1("AmendmentCorrigenda1"),
    AMENDMENT_CORRIGENDA_2("AmendmentCorrigenda2")
}