package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.asciidoc.ieee.model.BibliographicItem
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import org.somda.asciidoc.ieee.word.mapping.replaceBy

/**
 * Abstract implementation to replace a bookmark by a list of bibliographic items (normative/informative).
 */
abstract class BibliographyHook(
    bibliographicItems: List<BibliographicItem>,
    private val mapper: ModelToWordMapping
) : BookmarkHook {
    private val sortedBibliographicItems = bibliographicItems.sortedBy { it.bibItemBookmark.label }
    override fun callback(bookmark: CTBookmark) {
        bookmark.parent.also { bookmarkP ->
            check(bookmarkP is P) {
                "Bookmark for bibliographic items is required to be a Paragraph but was: ${bookmark.parent.javaClass}"
            }

            bookmarkP.replaceBy(sortedBibliographicItems.map {
                mapper.toBibItem(it)
            })
        }
    }
}