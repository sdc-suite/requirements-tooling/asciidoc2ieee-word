package org.somda.asciidoc.ieee.word.hook

import org.apache.commons.text.StringEscapeUtils
import org.docx4j.model.structure.DocumentModel
import org.docx4j.model.structure.HeaderFooterPolicy
import org.docx4j.openpackaging.parts.WordprocessingML.HeaderPart
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.docx4j.wml.P
import org.somda.asciidoc.ieee.model.DocumentMetadata
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.model.DocumentType
import org.somda.asciidoc.ieee.word.mapping.createWordRun
import java.text.SimpleDateFormat
import java.util.*


/**
 * Populates document variables (metadata) and header/footer of the document.
 */
class MetadataHook(private val documentParticles: DocumentParticles) : DocumentHook {
    private lateinit var mainDocument: MainDocumentPart
    private lateinit var documentModel: DocumentModel

    override fun callback(
        mainDocument: MainDocumentPart,
        documentModel: DocumentModel
    ) {

        this.mainDocument = mainDocument
        this.documentModel = documentModel

        //TocGenerator
        assignDocVars()
        updateHeader()
    }

    private fun updateHeader() {
        for (sw in documentModel.sections) {
            val hfp = sw.headerFooterPolicy
            if (hfp.firstHeader != null) {
                replaceHeaderText(hfp.firstHeader)
            }
            if (hfp.defaultHeader != null) {
                replaceHeaderText(hfp.defaultHeader)
            }
            if (hfp.evenHeader != null) {
                replaceHeaderText(hfp.evenHeader)
            }
        }
    }

    private fun replaceHeaderText(headerPart: HeaderPart) {
        headerPart.content.filterIsInstance<P>().let { content ->
            // 1 line: amendment/corrigenda
            // 2 lines: baseline/revision
            check(content.size == 1 || content.size == 2) {
                "Header is required to contain exactly 1 or 2 paragraph(s) but found ${content.size}"
            }

            if (content.size == 1) {
                content[0].content.clear()
                content[0].content.add(
                    createWordRun(
                        HEADER_LINE_AMD_COR
                            .replace(DESIGNATION, designationOf(documentParticles.metadata))
                            .replace(DRAFT_NUMBER, decode(documentParticles.metadata.draftNumber))
                            .replace(DRAFT_MONTH, monthOf(documentParticles.metadata.draftDate))
                            .replace(DRAFT_YEAR, yearOf(documentParticles.metadata.draftDate))
                            .replace(AMD_COR_NUMBER, amdCorOf(documentParticles.metadata))
                    )
                )
            }

            if (content.size == 2) {
                content[0].content.clear()
                content[0].content.add(
                    createWordRun(
                        HEADER_LINE_1_FORMAT
                            .replace(DESIGNATION, designationOf(documentParticles.metadata))
                            .replace(DRAFT_NUMBER, decode(documentParticles.metadata.draftNumber))
                            .replace(DRAFT_MONTH, monthOf(documentParticles.metadata.draftDate))
                            .replace(DRAFT_YEAR, yearOf(documentParticles.metadata.draftDate))
                    )
                )

                content[1].content.clear()
                content[1].content.add(
                    createWordRun(
                        HEADER_LINE_2_FORMAT
                            .replace(TRIAL_USE, trialUseText(documentParticles.metadata.trialUse))
                            .replace(STANDARD_TYPE, decode(documentParticles.metadata.standardType.typeName))
                            .replace(PAR_TITLE, decode(documentParticles.metadata.parTitle))
                    )
                )
            }
        }
    }

    private fun assignDocVars() {
        setDocVariable("varDesignation", decode(documentParticles.metadata.designation))
        setDocVariable("varDraftNumber", decode(documentParticles.metadata.draftNumber))
        setDocVariable("txtGorRPorSTD", decode(documentParticles.metadata.standardType.typeName))
        setDocVariable("txtTrialUse", trialUseText(documentParticles.metadata.trialUse))
        setDocVariable("varTitlePAR", decode(documentParticles.metadata.parTitle))
        setDocVariable("varCommittee", decode(documentParticles.metadata.committeeName))
        setDocVariable("varDraftYear", yearOf(documentParticles.metadata.draftDate))
        setDocVariable("varDraftMonth", monthOf(documentParticles.metadata.draftDate))
        setDocVariable("varSociety", decode(documentParticles.metadata.societyName))
        setDocVariable("varWkGrpChair", decode(documentParticles.metadata.chairName))
        setDocVariable("varWkGrpViceChair", decode(documentParticles.metadata.subgroupChairName))
        setDocVariable("varWorkingGroup", decode(documentParticles.metadata.workingGroupName))
    }

    private fun decode(text: String) = StringEscapeUtils.unescapeHtml4(text)

    private fun trialUseText(trialUse: Boolean) = if (trialUse) " Trial-Use " else " "

    private fun monthOf(date: Date) = monthFormat.format(date)

    private fun yearOf(date: Date) = yearFormat.format(date)

    private fun amdCorOf(documentMetadata: DocumentMetadata): String {
        return when (val docType = documentMetadata.documentType()) {
            DocumentType.REVISION -> error("Expected amendment or corrigenda, found revision")
            else -> "${docType.shortName} ${documentMetadata.amendmentCorrigendaNumber}"
        }
    }

    private fun designationOf(documentMetadata: DocumentMetadata): String {
        return decode(
            if (documentMetadata.isAmendmentCorrigenda()) {
                "${documentMetadata.designation}-${documentMetadata.amendmentCorrigendaYear}"
            } else {
                documentMetadata.designation
            }
        )
    }

    private fun setDocVariable(variableName: String, newValue: String) {
        mainDocument.documentSettingsPart.contents.docVars.docVar.firstOrNull { it.name == variableName }?.also {
            it.`val` = newValue
        }
    }

    private companion object {
        const val DESIGNATION = "{designation}"
        const val AMD_COR_NUMBER = "{amd_cor_number}"
        const val DRAFT_NUMBER = "{draft_number}"
        const val DRAFT_MONTH = "{draft_month}"
        const val DRAFT_YEAR = "{draft_year}"
        const val TRIAL_USE = "{trial_use}"
        const val STANDARD_TYPE = "{standard_type}"
        const val PAR_TITLE = "{par_title}"

        const val HEADER_LINE_AMD_COR = "IEEE P$DESIGNATION/$AMD_COR_NUMBER/D$DRAFT_NUMBER, $DRAFT_MONTH $DRAFT_YEAR"
        const val HEADER_LINE_1_FORMAT = "IEEE P$DESIGNATION/D$DRAFT_NUMBER, $DRAFT_MONTH $DRAFT_YEAR"
        const val HEADER_LINE_2_FORMAT = "Draft$TRIAL_USE$STANDARD_TYPE for $PAR_TITLE"

        val yearFormat = SimpleDateFormat("yyyy", Locale.US)
        val monthFormat = SimpleDateFormat("MMM", Locale.US)
    }
}