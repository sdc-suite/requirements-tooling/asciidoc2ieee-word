package org.somda.asciidoc.ieee.word.hook

import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping

/**
 * Replaces paragraph at [BookmarkLabel.NORMATIVE_REFERENCE] bookmark by list of normative bibliographic items.
 */
class NormativeReferenceHook(
    document: DocumentParticles,
    mapper: ModelToWordMapping
) : BibliographyHook(document.normativeReferences, mapper) {
    override fun label() = BookmarkLabel.NORMATIVE_REFERENCE
}