package org.somda.asciidoc.ieee.word.hook

import org.docx4j.model.structure.DocumentModel
import org.docx4j.model.structure.HeaderFooterPolicy
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart

/**
 * Called once before the bookmarks are detected.
 */
interface DocumentHook {
    fun callback(
        mainDocument: MainDocumentPart,
        documentModel: DocumentModel
    )
}