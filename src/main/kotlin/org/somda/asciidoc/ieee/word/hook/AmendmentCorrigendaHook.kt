package org.somda.asciidoc.ieee.word.hook

import jakarta.xml.bind.JAXBElement
import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.createWordRun

/**
 * Replaces Amendment/Corrigenda placeholder.
 *
 * Since there are two placeholder in an amendment/corrigenda, this class is abstract and need implementations
 * that refer to a suitable label.
 */
abstract class AmendmentCorrigendaHook(
    private val document: DocumentParticles
) : BookmarkHook {

    override fun callback(bookmark: CTBookmark) {
        bookmark.parent.also { bookmarkP ->
            check(bookmarkP is P) {
                "Bookmark for amendment/corrigenda is required to be a Paragraph but was: ${bookmark.parent.javaClass}"
            }

            val bookmarkStartAndEnd = bookmarkP.content.asSequence()
                .mapIndexed { index, element -> Pair(index, element) }
                .filter { it.second.javaClass.isAssignableFrom(JAXBElement::class.java) }
                .map { Pair(it.first, JAXBElement::class.java.cast(it.second)) }
                .filter { it.second.value.javaClass.isAssignableFrom(CTBookmark::class.java) }
                .toList()

            require(bookmarkStartAndEnd.size == 2) {
                "The title in an amendment/corrigenda requires exactly two bookmark elements."
            }

            val theBookmark = bookmarkStartAndEnd.first().second.value
            require(theBookmark is CTBookmark) {
                "No bookmark found"
            }
            require(theBookmark.name == label().label) {
                "The title does not contain a bookmark named ${label().label}"
            }

            repeat(bookmarkStartAndEnd[1].first - bookmarkStartAndEnd[0].first - 1) {
                bookmarkP.content.removeAt(bookmarkStartAndEnd.first().first + 1)
            }

            bookmarkP.content.add(
                bookmarkStartAndEnd.first().first + 1, createWordRun(
                    "${document.metadata.amendmentCorrigenda} ${document.metadata.amendmentCorrigendaNumber}"
                )
            )
        }
    }
}