package org.somda.asciidoc.ieee.word.hook

import org.somda.asciidoc.ieee.model.DocumentParticles

/**
 * Replaces Amendment/Corrigenda placeholder at the first occurrence.
 */
class AmendmentCorrigendaHook1(document: DocumentParticles) : AmendmentCorrigendaHook(document) {
    override fun label() = BookmarkLabel.AMENDMENT_CORRIGENDA_1
}