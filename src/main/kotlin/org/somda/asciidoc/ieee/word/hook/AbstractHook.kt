package org.somda.asciidoc.ieee.word.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.docx4j.wml.R
import org.docx4j.wml.RPr
import org.docx4j.wml.RStyle
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.WordGlobals
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import org.somda.asciidoc.ieee.word.mapping.createPagebreakParagraph
import org.somda.asciidoc.ieee.word.mapping.createPagebreakRun
import org.somda.asciidoc.ieee.word.mapping.createWordRun
import org.somda.asciidoc.ieee.word.mapping.insertParagraphAfter

/**
 * Appends paragraph at [BookmarkLabel.ABSTRACT] bookmark with abstract text.
 */
class AbstractHook(
    private val document: DocumentParticles,
    private val mapper: ModelToWordMapping
) : BookmarkHook {
    override fun label() = BookmarkLabel.ABSTRACT

    override fun callback(bookmark: CTBookmark) {
        var currentParagraph = bookmark.parent as P

        val abstractRunStyleBold = RPr().apply {
            rStyle = RStyle().apply { `val` = WordGlobals.STYLE_ABSTRACT_HEADER }
        }

        // first entry replaces the bookmarked paragraph
        currentParagraph.content.also { content ->
            content.clear()
            content.add(
                R().apply {
                    content.add(createPagebreakRun())
                }
            )
            content.add(createWordRun("Abstract: ", abstractRunStyleBold))
            content.addAll(mapper.toRuns(document.abstract.first()))
        }

        // remaining elements are added after the bookmarked paragraph
        document.abstract.drop(1).map {
            mapper.toRuns(it)
        }.forEach { wordRuns ->
            currentParagraph.insertParagraphAfter {
                it.content.addAll(wordRuns)
            }.also {
                currentParagraph = it
            }
        }

        currentParagraph.insertParagraphAfter { }
    }
}