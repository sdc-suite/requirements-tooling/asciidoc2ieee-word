package org.somda.asciidoc.ieee.word.mapping

import org.docx4j.wml.JcEnumeration
import org.docx4j.wml.P
import org.docx4j.wml.PPr
import org.somda.asciidoc.ieee.word.WordGlobals

/**
 * Function alias that accepts a Word paragraph style.
 *
 * The style passed can be modified by this function.
 */
typealias PPrAppender = (style: PPr) -> Unit

/**
 * Infrastructure to manage paragraph styles.
 */
sealed class PPrModifier {

    /**
     * Modifier that assigns the given style.
     *
     * @property style the style to assign. Overrides existing styles on a paragraph.
     * @property appenders optional list of appender callbacks that are executed after style assignment.
     */
    data class Assign(val style: PPr, val appenders: List<PPrAppender> = mutableListOf()) : PPrModifier() {

        /**
         * Appends another style appender function to this modifier.
         */
        fun append(appender: PPrAppender) = copy(
            appenders = appenders.toMutableList().apply { add(appender) }
        )

        /**
         * Appends style appender functions of a [Append] modifier to this modifier.
         */
        fun append(appender: Append) = copy(
            appenders = appenders.toMutableList().apply { addAll(appender.appenders) }
        )

        /**
         * Applies the modifier on a paragraph.
         */
        override fun applyOn(paragraph: P) {
            paragraph.pPr = style
            appenders.forEach {
                it(paragraph.pPr)
            }
        }
    }

    /**
     * Modifier that calls the given appender functions.
     *
     * @property appenders callbacks to modify an existing style.
     */
    data class Append(val appenders: List<PPrAppender>) : PPrModifier() {

        /**
         * Constructor that accepts a single appender function.
         */
        constructor(appender: PPrAppender) : this(listOf(appender))

        /**
         * Appends another style appender function to this modifier.
         */
        fun append(appender: PPrAppender) = copy(
            appenders = appenders.toMutableList().apply { add(appender) }
        )

        /**
         * Appends style appender functions of a [Append] modifier to this modifier.
         */
        fun append(appender: Append) = copy(
            appenders = appenders.toMutableList().apply { addAll(appender.appenders) }
        )

        override fun applyOn(paragraph: P) {
            paragraph.pPr ?: Default.applyOn(paragraph)
            appenders.forEach {
                it(paragraph.pPr)
            }
        }
    }

    /**
     * Applies the default style with left alignment on a paragraph.
     */
    object Default : PPrModifier() {
        override fun applyOn(paragraph: P) {
            paragraph.pPr = createPPr(WordGlobals.STYLE_PARAGRAPH, JcEnumeration.LEFT)
        }
    }

    /**
     * Performs no modification.
     */
    object Void : PPrModifier() {
        override fun applyOn(paragraph: P) = Unit
    }

    /**
     * Applies the modifier on a paragraph.
     */
    abstract fun applyOn(paragraph: P)
}

