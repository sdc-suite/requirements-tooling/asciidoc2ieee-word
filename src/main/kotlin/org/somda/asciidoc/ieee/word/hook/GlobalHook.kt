package org.somda.asciidoc.ieee.word.hook

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.CTBookmark

/**
 * Called before and after the bookmarks are detected.
 */
interface GlobalHook {
    fun callbackBeforeBookmarkHooks(wordPackage: WordprocessingMLPackage, bookmarks: List<CTBookmark>) = Unit
    fun callbackAfterBookmarkHooks(wordPackage: WordprocessingMLPackage, bookmarks: List<CTBookmark>) = Unit
}