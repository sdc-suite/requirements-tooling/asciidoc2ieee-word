package org.somda.asciidoc.ieee.word.hook

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.toc.Toc
import org.docx4j.toc.TocGenerator
import org.docx4j.wml.CTBookmark

class TocHook(private val maxTocDepth: Int = 2) : GlobalHook {
    override fun callbackAfterBookmarkHooks(wordPackage: WordprocessingMLPackage, bookmarks: List<CTBookmark>) {
        bookmarks.map {
            if (it.name == CONTENTS_BOOKMARK_NAME) {
                wordPackage.mainDocumentPart.content.indexOf(it.parent)
            } else {
                -1
            }
        }.first { it != -1 }.also {
            val tocPosition = it
            val tocGenerator = TocGenerator(wordPackage)
            Toc.setTocHeadingText("")
            tocGenerator.generateToc(tocPosition, " TOC \\o \"1-$maxTocDepth\" \\h \\z \\u ", true)
            wordPackage.mainDocumentPart.content.removeAt(tocPosition + 1)
        }
    }

    private companion object {
        const val CONTENTS_BOOKMARK_NAME = "Contents"
    }
}