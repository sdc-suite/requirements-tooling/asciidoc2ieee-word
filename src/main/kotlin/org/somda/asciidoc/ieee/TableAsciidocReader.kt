package org.somda.asciidoc.ieee

import org.asciidoctor.ast.Document
import org.asciidoctor.ast.StructuralNode
import org.somda.asciidoc.ieee.model.Block

/**
 * Helper class to convert asciidoc blocks in tables.
 */
class TableAsciidocReader(private val mapping: AsciidocToModelMapping) {

    /**
     * Accepts a table cell document and resolves all blocks from it
     *
     * @param document the table cell document.
     * @return all blocks contained in `document` of the following asciidoc types:
     * - paragraph
     * - ordered list
     * - unordered list
     * - admonition
     */
    fun resolveBlocks(document: Document) = mutableListOf<Block>().apply {
        document.blocks.forEach {
            processBlock(it, this)
        }
    }

    private fun processBlock(structuralNode: StructuralNode, blocks: MutableList<Block>) {
        structuralNode.toSealed().let { node ->
            when (node) {
                is AsciidocNode.Document -> {
                    structuralNode.blocks.forEach {
                        processBlock(it, blocks)
                    }
                }

                is AsciidocNode.Paragraph, is AsciidocNode.OrderedList, is AsciidocNode.UnorderedList ->
                    blocks.add(mapping.toParagraph(node))

                is AsciidocNode.Admonition ->
                    blocks.add(mapping.toAdmonition(node))

                else -> Unit
            }
        }
    }
}