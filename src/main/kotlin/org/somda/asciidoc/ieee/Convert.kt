package org.somda.asciidoc.ieee

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.file
import org.apache.logging.log4j.kotlin.Logging
import org.somda.asciidoc.ieee.server.RunServer
import java.io.File
import kotlin.system.exitProcess

/**
 * Application entry point that either starts up a server or runs as a standalone application for single file conversion.
 */
fun main(args: Array<String>) = Convert().main(args)

/**
 * Command line handling that dispatches between server and single file conversion.
 */
class Convert : CliktCommand("asciidoc2ieee-word") {
    private companion object : Logging

    private val runServer by option("--server", help = "runs a server to accept conversion requests")
        .flag(default = false)

    private val adocInputFile by option("--input-file", help = "path to asciidoc input file for single file conversion")
        .file()
        .validate {
            require(it.exists()) { "Input file '$it' does not exist." }
        }

    override fun run() {
        if (runServer || File("server.run").exists()) {
            RunServer().run()
        } else {
            adocInputFile?.also {
                RunSingleConversion().run(it)
            } ?: logger.error { "Asciidoc input file is missing (option --input-file)" }
            exitProcess(0)
        }
    }
}