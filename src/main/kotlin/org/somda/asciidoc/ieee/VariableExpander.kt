package org.somda.asciidoc.ieee

import org.asciidoctor.ast.Document
import org.asciidoctor.extension.Preprocessor
import org.asciidoctor.extension.PreprocessorReader

class VariableExpander : Preprocessor() {
    private val variableExpanderLogic = VariableExpanderLogic()

    override fun process(document: Document, reader: PreprocessorReader) {
        val lines = reader.readLines()
        val newLines: MutableList<String> = ArrayList()

        for (line in lines.withIndex()) {
            // Document title is parsed automatically, no preprocessing necessary
            if (line.index == 0) {
                newLines.add(line.value)
                continue
            }

            if (variableExpanderLogic.handleVariableDefinitions(line.value)) {
                newLines.add(line.value)
                continue
            }

            newLines.add(variableExpanderLogic.replaceVariablesIn(line.value))
        }

        reader.restoreLines(newLines)
    }
}

internal class VariableExpanderLogic {
    private val ignores = setOf("nbsp")
    private val variables = mutableMapOf<String, String>()

    internal fun replaceVariablesIn(text: String): String {
        var currentText = text
        var resultingText = ""
        while (true) {
            val token = findNextToken(currentText)
            if (token == null) {
                resultingText += currentText
                break
            }
            val textToReplace = replaceVariablesIn(variables[token.identifier]!!)
            resultingText += currentText.substring(0, token.startIndex) +
                    textToReplace
            currentText = currentText.substring(token.endIndex + 1)
        }

        return resultingText
    }

    private fun findNextToken(text: String): VariableMatch? {
        var startIndex = 0
        while (true) {
            val index = text.indexOf("{", startIndex)

            // nothing found at all, exit
            if (index == -1) {
                return null
            }

            // skip symbol, next iteration
            if (index > 0 && text[index - 1] == '\\') {
                startIndex += 1
                continue
            }

            val toIgnore = ignores
                .filter { text.length > index + 1 }
                .count { text.substring(index + 1, index + it.length + 1) == it }
            if (toIgnore > 0) {
                startIndex += 1
                continue
            }

            // symbol found
            val endIndex = text.indexOf("}", startIndex)
            if (endIndex == -1) {
                error("Missing closing variable parenthesis in '$text'")
            }

            val variableName = text.substring(index + 1, endIndex)
            if (!variables.containsKey(variableName)) {
                return null
            }

            return VariableMatch(variableName, index, endIndex)
        }
    }

    internal fun handleVariableDefinitions(line: String): Boolean {
        undefinePattern.find(line)?.also { match ->
            variables.remove(match.groupValues[1])
            return true
        }

        definePattern.find(line)?.also { match ->
            variables[match.groupValues[1]] = match.groupValues[2].trim()
            return true
        }

        return false
    }

    private data class VariableMatch(
        val identifier: String,
        val startIndex: Int,
        val endIndex: Int
    )

    private val definePattern = """^:(.+?):(.*)$""".toRegex()
    private val undefinePattern = """^:!(.+?):\s*$""".toRegex()
}