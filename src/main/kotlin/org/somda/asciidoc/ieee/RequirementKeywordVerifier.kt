package org.somda.asciidoc.ieee

import org.apache.logging.log4j.kotlin.Logging
import org.asciidoctor.ast.Block
import org.asciidoctor.ast.Document
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.Treeprocessor
import org.somda.asciidoc.ieee.model.RequirementLevel
import org.somda.asciidoc.ieee.model.resolveRequirementLevel

/**
 * Checks expected requirement levels in IEEE requirements.
 *
 * Loops all paragraphs of ieee_requirement blocks and checks if there is exactly one requirement level keyword that
 * equals the value of the attribute key ieee_keyword.
 *
 * Exits with an error if
 *
 * - there are multiple different keywords
 * - if no keyword is found
 * - if more than one occurrence of the keyword is found
 */
class RequirementKeywordVerifier : Treeprocessor() {
    override fun process(document: Document): Document {
        processBlock(document as StructuralNode)
        return document
    }

    private fun processBlock(block: StructuralNode) {
        block.toSealed().let { node ->
            when (node) {
                is AsciidocNode.Requirement -> {
                    processRequirement(node.wrapped)
                }

                else -> logger.trace { "Ignore block of type '${block.context}'" }
            }
        }

        block.blocks.forEach { processBlock(it) }
    }

    private fun processRequirement(block: Block) {
        val attributes = Attributes(block.attributes)
        val levelRaw = attributes[Attribute.IEEE_REQUIREMENT_KEYWORD]
        checkNotNull(levelRaw) {
            "Missing requirement level keyword for IEEE requirement with id ${attributes[Attribute.ID]}"
        }
        val level = resolveRequirementLevel(levelRaw)
        checkNotNull(level) {
            "Invalid requirement level for IEEE requirement with id ${attributes[Attribute.ID]}: $level"
        }

        val msgPrefix = "Check requirement level keyword for #${attributes[Attribute.ID]} (${level.keyword}):"
        var levelCount = 0
        block.blocks.forEach { reqBlock ->
            reqBlock.toSealed().let { childNode ->
                when (childNode) {
                    is AsciidocNode.Paragraph -> {
                        levelCount += childNode.wrapped.source
                            .split(" ")
                            .map { it.trim() }
                            .count { it == level.keyword.uppercase() }

                        RequirementLevel.values().filter { it != level }.forEach { notLevel ->
                            val notLevelCount = childNode.wrapped.source
                                .split(" ")
                                .map { it.trim() }
                                .count { it == notLevel.keyword }
                            check(notLevelCount == 0) {
                                "$msgPrefix Requirement level keyword '${notLevel.keyword}' found"
                            }
                        }
                    }

                    else -> Unit
                }
            }
        }

        check(levelCount > 0) {
            "$msgPrefix Requirement level keyword is missing or spelled wrong. " +
                    "Verify that the keyword is written in uppercase."
        }

        check(levelCount == 1) {
            "$msgPrefix Only one requirement level keyword allowed per requirement"
        }

        logger.info { "$msgPrefix Done" }
    }

    private companion object : Logging
}