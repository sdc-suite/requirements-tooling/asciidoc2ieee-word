package org.somda.asciidoc.ieee

import org.apache.logging.log4j.kotlin.Logging
import org.asciidoctor.Options
import org.asciidoctor.ast.ContentModel
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.BlockProcessor
import org.asciidoctor.extension.Contexts
import org.asciidoctor.extension.Name
import org.asciidoctor.extension.Reader
import org.somda.asciidoc.*
import org.somda.asciidoc.ieee.model.RequirementMetadata
import org.somda.asciidoc.ieee.model.RequirementRole
import org.somda.asciidoc.ieee.model.resolveRequirementLevel

/**
 * Plugin that searches for [BLOCK_NAME_IEEE_REQUIREMENT] blocks.
 *
 * - Checks for requirement number duplicates
 * - Stores all requirements in [RequirementsCollector.detectedRequirements] for further processing
 *
 * @param expectedRequirementNumbers an optional list of numbers to verify if all expected requirements exist in the document.
 */
@Name(BLOCK_NAME_IEEE_REQUIREMENT)
@Contexts(Contexts.SIDEBAR)
@ContentModel(ContentModel.COMPOUND)
class RequirementsCollector(private val expectedRequirementNumbers: Set<Long>? = null) :
    BlockProcessor(BLOCK_NAME_IEEE_REQUIREMENT) {
    companion object : Logging {
        private val REQUIREMENT_NUMBER_FORMAT = "^r(\\d+)$".toRegex()
        private val REQUIREMENT_TITLE_FORMAT = "^([A-Z])*?R(\\d+)$".toRegex()
        private const val REQUIREMENT_ROLE = "requirement"

        fun requirementNumberOfId(id: String): Int {
            val matchResults = REQUIREMENT_NUMBER_FORMAT.findAll(id)
            return matchResults.map { it.groupValues[1] }.toList().first().toInt()
        }
    }

    private val detectedRequirements = mutableMapOf<Int, RequirementMetadata>()

    /**
     * Returns all requirements that were collected by this block processor.
     *
     * Make sure to only call this function once the conversion ended as otherwise this map will be empty.
     */
    fun detectedRequirements(): Map<Int, RequirementMetadata> = detectedRequirements

    override fun process(
        parent: StructuralNode, reader: Reader,
        attributes: MutableMap<String, Any>
    ): Any = retrieveRequirement(reader, Attributes(attributes)).let { requirement ->
        logger.info { "Found IEEE requirement #${requirement.number}: $requirement" }
        requirement.asciiDocAttributes[Attribute.ROLE] = REQUIREMENT_ROLE
        storeRequirement(requirement)
        createBlock(
            parent, plainContext(Contexts.SIDEBAR), mapOf(
                Options.ATTRIBUTES to attributes, // copy attributes for further processing
                ContentModel.KEY to ContentModel.COMPOUND // signify construction of a compound object
            )
        ).also {
            // make sure to separately parse contents since reader was requested by retrieveRequirement()
            // and is EOF now
            parseContent(it, requirement.asciiDocLines)
        }
    }

    private fun retrieveRequirement(reader: Reader, attributes: Attributes): RequirementMetadata {
        val number = kotlin.runCatching {
            requirementNumberOfId(attributes.id() ?: throw Exception("No Requirement id found"))
        }.onFailure {
            throw Exception(
                "Detected requirement block without id. " +
                        "Verify that each $BLOCK_NAME_IEEE_REQUIREMENT block has an Asciidoc id."
            )
        }.getOrThrow()

        val lines = reader.readLines()
        val level = checkNotNull(resolveRequirementLevel(attributes[Attribute.IEEE_REQUIREMENT_KEYWORD] ?: "")) {
            "Missing requirement level for IEEE requirement #$number"
        }

        try {
            return RequirementMetadata(
                number,
                level,
                RequirementRole.PARTICIPANT,
                setOf(),
                attributes,
                lines
            )
        } catch (e: Exception) {
            logger.error { "Error while processing requirement #$number: ${e.message}" }
            throw e
        }
    }

    private fun storeRequirement(requirement: RequirementMetadata) {
        validateRequirement(requirement)
        detectedRequirements[requirement.number] = requirement
    }

    private fun validateRequirement(requirement: RequirementMetadata) {
        val reqNumberFromTitle = REQUIREMENT_TITLE_FORMAT.findAll(requirement.blockTitle)
            .map { it.groupValues[2] }.toList().first().toInt()

        check(reqNumberFromTitle == requirement.number) {
            "IEEE requirement title format is wrong or number differs from ID: " +
                    "title=${requirement.blockTitle}, id=${requirement.blockId}"
        }

        checkNotNull(requirement.asciiDocAttributes[Attribute.ROLE]) {
            "IEEE requirement #'${requirement.number}' has no role; expected '$REQUIREMENT_ROLE'"
        }

        check(!detectedRequirements.containsKey(requirement.number)) {
            "IEEE requirement #'${requirement.number}' already exists"
        }

        check(expectedRequirementNumbers?.contains(requirement.number.toLong()) ?: true) {
            "Requirement number ${requirement.number} could not be found in requirement number set"
        }
    }
}