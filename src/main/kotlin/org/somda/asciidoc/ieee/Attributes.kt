package org.somda.asciidoc.ieee

/**
 * Known asciidoc and IEEE attributes.
 *
 * IEEE specific attributes start with `IEEE`.
 */
enum class Attribute(val key: String) {
    ID("id"),
    IMAGE_FOLDER("imagesdir"),
    TITLE("title"),
    REF_TEXT("reftext"),
    ROLE("role"),
    TARGET("target"),
    ALTERNATIVE_TEXT("alt"),
    HORIZONTAL_ALIGN("halign"),
    VERTICAL_ALIGN("valign"),
    STYLE("style"),
    COL_COUNT("colcount"),
    ROW_COUNT("rowcount"),
    TABLE_WIDTH_PERCENTAGE("tablepcwidth"),
    COLS("cols"),
    IEEE_REQUIREMENT_KEYWORD("ieee_keyword"),
    IEEE_SKIP("ieee_skip"),
    IEEE_DEFINITIONS( "ieee_definitions"),
    IEEE_ABBREVIATIONS( "ieee_abbreviations"),
    IEEE_BIBLIOGRAPHY_GROUP("ieee_bib_group"),
    IEEE_OVERVIEW( "ieee_overview"),
    IEEE_NORMATIVITY("ieee_normativity"),
    IEEE_METADATA_DESIGNATION("ieee_metadata_designation"),
    IEEE_METADATA_DRAFT_NUMBER("ieee_metadata_draft_number"),
    IEEE_METADATA_TRIAL_USE("ieee_metadata_trial_use"),
    IEEE_METADATA_GDE_REC_PRAC_STD("ieee_metadata_gde_rec_prac_std"),
    IEEE_METADATA_PAR_TITLE("ieee_metadata_par_title"),
    IEEE_METADATA_COMMITTEE_NAME("ieee_metadata_committee_name"),
    IEEE_METADATA_SOCIETY_NAME("ieee_metadata_society_name"),
    IEEE_METADATA_DRAFT_DATE("ieee_metadata_draft_date"),
    IEEE_METADATA_WORKING_GROUP_NAME("ieee_metadata_working_group_name"),
    IEEE_METADATA_CHAIR_NAME("ieee_metadata_chair_name"),
    IEEE_METADATA_SUBGROUP_CHAIR_NAME("ieee_metadata_subgroup_chair_name"),
    IEEE_ORIENTATION("ieee_orientation"),
    IEEE_AMENDMENT_CORRIGENDA("ieee_amendment_corrigenda"),
    IEEE_AMENDMENT_CORRIGENDA_NUMBER("ieee_amendment_corrigenda_number"),
    IEEE_AMENDMENT_CORRIGENDA_YEAR("ieee_amendment_corrigenda_year"),
    IEEE_CHANGE_TYPE("ieee_change_type"),
    IEEE_CHANGE_WHERE("ieee_change_where"),
    IEEE_ROW_NUMBER("ieee_row_number"),
    IEEE_TABLE_NUMBER("ieee_table_number"),
    IEEE_FIGURE_NUMBER("ieee_figure_number")
}

/**
 * Provides convenient access to all known attributes.
 */
class Attributes(private val attributes: MutableMap<String, Any>) {
    operator fun set(key: Attribute, value: Any) {
        attributes[key.key] = value
    }

    operator fun get(key: Attribute): String? = attributes[key.key]?.toString()

    fun has(key: Attribute) = attributes[key.key] != null

    override fun toString() =
        "{ " + attributes.entries.joinToString(", ") { "${it.key} => '${it.value}'" } + " }"
}