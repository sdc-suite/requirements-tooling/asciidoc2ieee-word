package org.somda.asciidoc.ieee

import org.asciidoctor.Asciidoctor
import org.asciidoctor.Options
import org.asciidoctor.SafeMode
import org.somda.asciidoc.ieee.model.DocumentParticles
import java.io.File

/**
 * Performs the Asciidoc conversion.
 *
 * @param inputType asciidoc input as file or string.
 * @param baseDir base dir passed to the asciidoctor engine.
 * @param outputFile the file to which the asciidoctor engine writes.
 * @param backend the backend, either html or pdf.
 * @param documentParticles the document particles struct to be populated during conversion.
 * @param requirementNumbers optional set of requirement numbers to verify existence for during conversion.
 *                           If the list does not exist, no verification is performed.
 */
class AsciidocConverter(
    private val inputType: Input,
    private val baseDir: File,
    private val outputFile: File,
    private val backend: String,
    private val documentParticles: DocumentParticles,
    private val requirementNumbers: List<Long>?
) : Runnable {
    override fun run() {
        val options = Options.builder()
            .baseDir(baseDir)
            .safe(SafeMode.UNSAFE)
            .backend(backend)
            .sourcemap(true)
            .toFile(outputFile).build()

        val asciidoctor = Asciidoctor.Factory.create()
        try {
            val variableExpander = VariableExpander()
            asciidoctor.javaExtensionRegistry().preprocessor(variableExpander)

            val requirementsCollector = RequirementsCollector(requirementNumbers?.toSet())
            asciidoctor.javaExtensionRegistry().block(requirementsCollector)

            val amendmentCorrigendaRequirementsCollector =
                AmendmentCorrigendaRequirementsCollector(requirementNumbers?.toSet())
            asciidoctor.javaExtensionRegistry().block(amendmentCorrigendaRequirementsCollector)

            val numberedReferencesCollector = NumberedReferencesCollector()
            asciidoctor.javaExtensionRegistry().treeprocessor(numberedReferencesCollector)

            val particlesReader = ParticlesReader(
                documentParticles,
                baseDir,
                requirementsCollector,
                numberedReferencesCollector
            )
            asciidoctor.javaExtensionRegistry().treeprocessor(particlesReader)
            asciidoctor.javaExtensionRegistry().treeprocessor(RequirementKeywordVerifier())

            asciidoctor.requireLibrary("asciidoctor-diagram") // enables plantuml
            when (inputType) {
                is Input.FileInput -> asciidoctor.convertFile(inputType.file, options)
                is Input.StringInput -> asciidoctor.convert(inputType.string, options)
            }

            if (requirementNumbers != null) {
                val foundIds = if (!documentParticles.metadata.isAmendmentCorrigenda()) {
                    requirementsCollector.detectedRequirements().map {
                        it.value.number.toLong()
                    }.toSet()
                } else {
                    amendmentCorrigendaRequirementsCollector.detectedIds
                }

                validateRequirementNumbers(
                    requirementNumbers,
                    foundIds
                )
            }
        } finally {
            asciidoctor.shutdown()
        }
    }

    private fun validateRequirementNumbers(
        expectedRequirementNumbers: List<Long>,
        actualRequirementNumbers: Set<Long>
    ) {
        val expectedAsSet = expectedRequirementNumbers.toSet()
        if (expectedAsSet.size < expectedRequirementNumbers.size) {
            throw Exception("Config file contains requirement number duplicates: " +
                    expectedRequirementNumbers.groupingBy { it }.eachCount().filter { it.value > 1 }.toList()
                        .joinToString(", ") {
                            "${it.second} times #${it.first}"
                        }
            )
        }

        if (expectedAsSet != actualRequirementNumbers) {
            val sub = expectedAsSet.subtract(actualRequirementNumbers)
            throw Exception("The following requirement numbers from the config were not found in the document: " +
                    sub.joinToString(", ") { it.toString() }
            )
        }
    }

    sealed interface Input {
        data class FileInput(val file: File) : Input
        data class StringInput(val string: String) : Input
    }
}