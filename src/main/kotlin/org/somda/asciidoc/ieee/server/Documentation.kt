package org.somda.asciidoc.ieee.server

object Documentation {
    val html = """
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Asciidoc2IEEE-Word Converter</title>
    <style>
        body {
            font-family: sans-serif;
            font-optical-sizing: auto;
            
            /* Use the specified font size */
            font-size-adjust: none;

            /* Use a font size that makes lowercase
               letters half the specified font size */
            font-size-adjust: 0.5;

            /* Two values - added in the Level 5 spec */
            font-size-adjust: ex-height 0.5;

            /* Global values */
            font-size-adjust: inherit;
            font-size-adjust: initial;
            font-size-adjust: revert;
            font-size-adjust: revert-layer;
            font-size-adjust: unset;
        }
    </style>
  </head>
  <body>
    <h1>Asciidoc2IEEE-Word Converter - Service is running</h1>
    <h2>API</h2>
    <p>Send a <code>POST</code> request with multipart form data containing</p>
    <ul>
        <li>a <code>ZIP</code> file that contains an <code>ADOC</code> file (on root level), source, and config <code>JSON</code></li>
        <li>Individually, an <code>ADOC</code> file and optionally a config <code>JSON</code></li> 
    </ul>
    <p>If you have multiple <code>ADOC</code> files on root level, you can specify the main file name by the query parameter <code>mainfile</code></p>
    <p>Depending on your desired output format, send the request to</p>
    <ul>
        <li><code>/pdf</code> to generate a non-IEEE-formatted PDF document</li>
        <li><code>/docx</code> to generate a IEEE-formatted DOCX document</li>
    </ul>
    <p><em>Attention: the conversion process may take some time!</em></p>
  </body>
  <h2>Examples</h2>
  <ul>
    <li><code>/pdf?mainfile=rootfile.adoc</code><br/>
        The request contains a main <code>ADOC</code> file named <em>rootfile.adoc</em>.<br/>
        The response is a PDF file.
    </li>
    <li><code>/docx</code><br/>
        The request contains any <code>ADOC</code> file that is considered the main file.<br/>
        The response is a DOCX file.
    </li>
  </ul>
</html>
    """.trimIndent()
}