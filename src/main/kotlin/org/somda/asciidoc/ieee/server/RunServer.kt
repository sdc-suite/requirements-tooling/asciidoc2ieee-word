package org.somda.asciidoc.ieee.server

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.apache.logging.log4j.kotlin.Logging
import org.somda.asciidoc.AsciidocErrorChecker
import org.somda.asciidoc.ieee.*
import org.somda.asciidoc.ieee.model.DocumentParticles
import org.somda.asciidoc.ieee.word.mapping.ModelToWordMapping
import java.io.*
import java.nio.file.Files
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream


class RunServer {
    fun run() {
        embeddedServer(Netty, 8080) {
            routing {
                get("/") {
                    call.respondText(ContentType.Text.Html) { Documentation.html }
                }
                get("/{backend}") {
                    if (listOf("pdf", "docx").contains(call.parameters["backend"])) {
                        call.respondText(
                            text = "Conversion can only be triggered by sending POST requests.",
                            status = HttpStatusCode.NotImplemented
                        )
                    } else {
                        call.respondText(
                            text = "The requested page does not exist.",
                            status = HttpStatusCode.NotFound
                        )
                    }
                }
                post("/{backend}") {
                    val workingDirectory = createTempDirectory()
                    val adocOutputFile = createTempFile()
                    try {
                        val backend = backend(call)
                        val mainFile = mainFile(call)

                        copyFilesFromMultipartForm(call, workingDirectory)

                        val adocInputFile = findAsciidocMainFile(workingDirectory, mainFile)
                        val configFile = findConfigFile(workingDirectory)

                        requireNotNull(adocInputFile) { "Missing Asciidoc input" }

                        val config = createConfig(configFile)

                        // data that is shared between the asciidoc ast processor and the word template
                        val documentParticles = DocumentParticles()

                        AsciidocErrorChecker().use {
                            // takes shared data and populates them
                            AsciidocConverter(
                                AsciidocConverter.Input.FileInput(adocInputFile),
                                workingDirectory,
                                adocOutputFile,
                                "pdf",
                                documentParticles,
                                config.requirements?.numbers
                            ).run()
                        }

                        val amendmentCorrigenda = documentParticles.metadata.isAmendmentCorrigenda()
                        // takes shared data and converts them to word output
                        val wordTemplate = createWordTemplate(amendmentCorrigenda)
                        val modelToWordMapper = ModelToWordMapping(wordTemplate.wordPackage)

                        call.response.header(
                            HttpHeaders.ContentDisposition,
                            ContentDisposition.Attachment.withParameter(
                                ContentDisposition.Parameters.FileName,
                                adocInputFile.nameWithoutExtension + ".$backend"
                            ).toString()
                        )

                        if (backend == "pdf") {
                            call.respondBytes(ContentType.Application.Pdf) { adocOutputFile.readBytes() }
                        } else {
                            val wordBytes = ByteArrayOutputStream()
                            wordTemplate.transform(
                                wordBytes,
                                bookmarkHooks(documentParticles, modelToWordMapper),
                                documentHooks(documentParticles),
                                globalHooks()
                            )
                            call.respondBytes(ContentType.Application.Docx) { wordBytes.toByteArray() }
                        }
                    } catch (e: Exception) {
                        when (e) {
                            is IllegalArgumentException,
                            is BadRequestException -> call.respondText(
                                text = sanitizeText(
                                    assembleCauses(e),
                                    listOf(adocOutputFile.parentFile, workingDirectory)
                                ),
                                status = HttpStatusCode.BadRequest
                            )

                            else -> {
                                logger.warn(e) { "Unexpected error: ${e.message}" }
                                call.respondText(
                                    text = "An internal server error ensued. Please contact the administrator of this service",
                                    status = HttpStatusCode.InternalServerError
                                )
                            }
                        }
                    } finally {
                        workingDirectory.deleteRecursively()
                        adocOutputFile.delete()
                    }
                }
            }
        }.start(wait = true)
    }

    private companion object : Logging {
        fun assembleCauses(throwable: Throwable?): String = throwable?.let {
            assembleCauses(it.cause) +
                    it.message +
                    "\n"
        } ?: ""

        fun unzipToDirectory(zipFile: InputStream, directory: File) {
            ZipInputStream(zipFile).also { zis ->
                try {
                    do {
                        val zipEntry = zis.nextEntry ?: break
                        val newFile = newFile(directory, zipEntry)
                        if (zipEntry.isDirectory) {
                            if (!newFile.isDirectory && !newFile.mkdirs()) {
                                throw IOException("Failed to create directory $newFile")
                            }
                        } else {
                            // fix for Windows-created archives
                            val parent = newFile.parentFile
                            if (!parent.isDirectory && !parent.mkdirs()) {
                                throw IOException("Failed to create directory $parent")
                            }

                            // write file content
                            FileOutputStream(newFile).use {
                                it.write(zis.readBytes())
                            }
                        }
                    } while (true)
                } finally {
                    zis.closeEntry()
                    zis.close()
                }
            }
        }

        fun newFile(destinationDir: File, zipEntry: ZipEntry): File {
            val destFile = File(destinationDir, zipEntry.name)
            val destDirPath = destinationDir.canonicalPath
            val destFilePath = destFile.canonicalPath
            if (!destFilePath.startsWith(destDirPath + File.separator)) {
                throw IOException("Entry is outside of the target dir: " + zipEntry.name)
            }
            return destFile
        }

        suspend fun createTempDirectory(): File = withContext(Dispatchers.IO) {
            try {
                Files.createTempDirectory(
                    "asciidoc2ieee-word_"
                ).toFile().apply {
                    setReadable(true)
                    setWritable(true)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Could not create temporary directory: ${e.message}" }
                throw e
            }
        }.absoluteFile

        suspend fun createTempFile(): File = withContext(Dispatchers.IO) {
            try {
                Files.createTempFile("asciidoc2ieee-word_", null).toFile().apply {
                    setReadable(true)
                    setWritable(true)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Could not create temporary file: ${e.message}" }
                throw e
            }
        }.absoluteFile

        fun findAsciidocMainFile(workingDirectory: File, mainFile: String?) = when (mainFile) {
            null -> workingDirectory.listFiles(FileFilter { it.name.endsWith("adoc", true) })
                ?.firstOrNull()

            else -> workingDirectory.listFiles(FileFilter { it.name == mainFile })?.firstOrNull()
        }

        fun findConfigFile(workingDirectory: File) =
            workingDirectory.listFiles(FileFilter { it.name.endsWith("json", true) })?.firstOrNull()

        // baseDirs: longest path at lowest index
        fun sanitizeText(text: String, baseDirs: List<File>) = baseDirs.fold("") { _, acc ->
            text.replace(acc.absoluteFile.absolutePath, "")
                .replace(acc.absoluteFile.absolutePath.replace("\\", "/"), "")
        }

        fun backend(call: ApplicationCall) = when (call.parameters["backend"]) {
            "pdf" -> "pdf"
            else -> "docx"
        }

        fun mainFile(call: ApplicationCall) = call.request.queryParameters["mainfile"]

        suspend fun copyFilesFromMultipartForm(call: ApplicationCall, destination: File) {
            val multipartData = runCatching {
                call.receiveMultipart()
            }.getOrElse {
                throw BadRequestException("Please provide files by using multipart form data\n${it.message}")
            }

            multipartData.forEachPart { part ->
                if (part is PartData.FileItem) {
                    val fileName = part.originalFileName as String
                    val fileBytes = part.streamProvider().readBytes()
                    when (fileName.endsWith("zip")) {
                        true -> {
                            withContext(Dispatchers.IO) {
                                unzipToDirectory(ByteArrayInputStream(fileBytes), destination)
                            }
                        }

                        false -> {
                            withContext(Dispatchers.IO) {
                                FileOutputStream(File(destination, fileName)).use {
                                    it.write(fileBytes)
                                }
                            }
                        }
                    }
                }

                part.dispose()
            }
        }
    }
}