package org.somda.asciidoc

/**
 * Allows for parsing terms that consist of asciidoc attributes and a text.
 *
 * Some items are not entirely parsed by AsciidoctorJ. This class can parse attribute groups of bibliographic items
 * and descriptions. Caution: there is only support for reference and label pairs, split by commas.
 *
 * @param term the term to parse.
 * @param termStyle term style, i.e. description or bib ref.
 */
class AttributedTerm(term: String, termStyle: TermStyle) {
    private val parsedTerm: Info

    init {
        val termRegex = termStyle.regex
        val (attributes, text) = termRegex.find(term)?.destructured
            ?: throw Exception("Expect term to match /$termRegex/, but miss for '$term'")
        val splitAttr = attributes.split(",").map { it.trim() }
        parsedTerm = Info(splitAttr.firstOrNull(), splitAttr.drop(1).firstOrNull(), text.trim())
    }

    /**
     * Retrieves the term.
     */
    fun get() = parsedTerm

    /**
     * Recognized term styles.
     *
     * Currently, there is support for description and bib ref items only.
     *
     * - Description attributes are enclosed by double brackets: `[[<reference>, <label>]]<text>`
     * - Bib ref attributes are enclosed by triple brackets: `[[[<reference>, <label>]]]<text>`
     */
    enum class TermStyle(val regex: Regex) {
        DESCRIPTION("""^\s*\[\[(.*)\]\](.*)$""".toRegex()),
        BIB_REF("""^\s*\[\[\[(.*)\]\]\](.*)$""".toRegex())
    }

    /**
     * Info of a parsed term.
     *
     * Layout: `[<reference>, <label>]<text>`
     */
    data class Info(val reference: String?, val label: String?, val text: String)
}