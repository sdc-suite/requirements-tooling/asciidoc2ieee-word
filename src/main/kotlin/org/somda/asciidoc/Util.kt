package org.somda.asciidoc

import org.asciidoctor.ast.StructuralNode
import org.somda.asciidoc.ieee.AsciidocNode
import org.somda.asciidoc.ieee.Attribute
import org.somda.asciidoc.ieee.Attributes
import org.somda.asciidoc.ieee.toSealed


/**
 * Resolves the block id attribute from an attributes map or null if the block id is missing.
 */
fun Attributes.id() = this[Attribute.ID]

/**
 * Resolves the block title attribute from an attributes map or null if the block title is missing.
 */
fun Attributes.title() = this[Attribute.TITLE]

/**
 * Resolves the block reference text attribute from an attributes map or null if the block reference text is missing.
 */
fun Attributes.refText() = this[Attribute.REF_TEXT]

/**
 * Creates a context name without a leading colon.
 *
 * `BlockProcessor.createBlock()` requires a context name without a leading colon.
 * This function checks the format and removes the leading colon.
 */
fun plainContext(context: String) = "^:([a-z]+)$".toRegex()
    .findAll(context)
    .map { it.groupValues[1] }
    .toList()
    .first()

/**
 * Checks if a node is an appendix block.
 *
 * @return true if yes, false otherwise.
 */
fun StructuralNode.isAppendix() = when (val section = this.toSealed()) {
    is AsciidocNode.Section -> section.wrapped.sectionName == "appendix"
    else -> false
}