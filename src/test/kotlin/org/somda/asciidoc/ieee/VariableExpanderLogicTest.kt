package org.somda.asciidoc.ieee

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class VariableExpanderLogicTest {
    @Test
    fun `test simple input with nested variables`() {
        val lines = """
            :var1: Test1
            :var2: Test2{var1}
            
            Var1: {var1}
            Var2: {var2}
        """.trimIndent()


        val expectedLines = """
            :var1: Test1
            :var2: Test2{var1}
            
            Var1: Test1
            Var2: Test2Test1
        """.trimIndent()

        val actualLines = runExpander(lines)
        assertEquals(expectedLines.lines(), actualLines)
    }

    @Test
    fun `test use of undefined variables throw`() {
        run {
            val undefinedUse = """
                Some undefined: {var1}
            """.trimIndent()

            val expectedLines = """
                Some undefined: {var1}
            """.trimIndent()

            val actualLines = runExpander(undefinedUse)
            assertEquals(expectedLines.lines(), actualLines)
        }

        run {
            val undefinedUse = """
                :var1: Test
                
                Here is some expansion: {var1}
                
                Here is some expansion: {var1}
            """.trimIndent()

            val expectedLines = """
                :var1: Test
                
                Here is some expansion: Test
                
                Here is some expansion: Test
            """.trimIndent()

            val actualLines = runExpander(undefinedUse)
            assertEquals(expectedLines.lines(), actualLines)
        }

        run {
            val undefinedUse = """
                :var1: Test
                
                Here is some expansion: {var1}
                
                :!var1:
                
                Here is some expansion: {var1}
            """.trimIndent()

            val expectedLines = """
                :var1: Test
                
                Here is some expansion: Test
                
                :!var1:
                
                Here is some expansion: {var1}
            """.trimIndent()

            val actualLines = runExpander(undefinedUse)
            assertEquals(expectedLines.lines(), actualLines)
        }
    }

    @Test
    fun `test multiple variables in one line`() {
        val input = """
            :var1: Test1
            :var2: Test2
            
            Here is multiple use in one line: {var1}, {var2}, {var1}, suffix
        """.trimIndent()

        val expectedLines = """
            :var1: Test1
            :var2: Test2
            
            Here is multiple use in one line: Test1, Test2, Test1, suffix
        """.trimIndent()

        val actualLines = runExpander(input)
        assertEquals(expectedLines.lines(), actualLines)
    }

    private fun runExpander(text: String): List<String> {
        val variableExpanderLogic = VariableExpanderLogic()
        val resultingLines = mutableListOf<String>()
        for (line in text.lines()) {
            if (variableExpanderLogic.handleVariableDefinitions(line)) {
                resultingLines.add(line)
            } else {
                resultingLines.add(variableExpanderLogic.replaceVariablesIn(line))
            }
        }

        return resultingLines
    }
}