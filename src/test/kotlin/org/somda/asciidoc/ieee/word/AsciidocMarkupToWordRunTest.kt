package org.somda.asciidoc.ieee.word

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.somda.asciidoc.Diff
import org.somda.asciidoc.DiffChunk.Change
import org.somda.asciidoc.DiffChunk.Delete
import org.somda.asciidoc.DiffChunk.Insert
import org.somda.asciidoc.DiffChunk.Plain
import org.somda.asciidoc.DiffSequence

class AsciidocMarkupToWordRunTest {
    @Test
    fun diff1() {
        val original =
            """The numerical distance between two values in the range of the given upper and lower bound.""".trimMargin()
        val revised =
            """The numerical two values in the range of the given lower and lower bound. as a non-negative decimal""".trimMargin()

        val patch = Diff.from(original, revised)

        val diff = DiffSequence(
            listOf(
                Plain(text = "The numerical"),
                Delete(text = "distance between"),
                Plain(text = "two values in the range of the given"),
                Change(deletedText = "upper", insertedText = "lower"),
                Plain(text = "and lower bound."),
                Insert(text = "as a non-negative decimal")
            )
        )

        assertEquals(diff, patch)
    }

    @Test
    fun diff2() {
        val original =
            """The numerical distance between two values in.""".trimMargin()
        val revised =
            """The numerical distance between two values as a non-negative decimal in.""".trimMargin()

        val patch = Diff.from(original, revised)

        val diff = DiffSequence(
            listOf(
                Plain(text = "The numerical distance between two values"),
                Insert(text = "as a non-negative decimal"),
                Plain(text = "in.")
            )
        )

        assertEquals(diff, patch)
    }

    @Test
    fun diff3() {
        val original =
            """The maximum number of technical alarm conditions that can be present at a point of time. If no value is given, an unlimited number SHALL be assumed.""".trimMargin()
        val revised =
            """The maximum number of technical alarm conditions that can be present simultaneously. If a SERVICE PROVIDER does not provide a value, this indicates there is no upper limit.""".trimMargin()

        val patch = Diff.from(original, revised)

        val diff = DiffSequence(
            listOf(
                Plain(text = "The maximum number of technical alarm conditions that can be present"),
                Change(deletedText = "at a point of time.", insertedText = "simultaneously."),
                Plain(text = "If"),
                Change(deletedText = "no value", insertedText = "a SERVICE PROVIDER does not provide a value, this indicates there"),
                Plain(text = "is"),
                Change(deletedText = "given, an unlimited number SHALL be assumed.", insertedText = "no upper limit.")
            )
        )

        patch.chunks.forEach {
            println(it)
        }

        assertEquals(diff, patch)
    }
}